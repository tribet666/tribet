# tribet.com

## Wymanagania

* PHP 7.0+
* MySQL 5.6+ 

## Przygotowania

### GIT - instalacja

Instrukcje na stronie:

> https://git-scm.com/book/en/v2/Getting-Started-Installing-Git


### Composer - instalacja

Instrukcje na stronie:

> https://getcomposer.org/download/


## Baza danych (cz.1)

Poniewaz jak pisales masz zainstalowany pakiet xampp, rowniez phpmyadmin powinien funkcjonowac u Ciebie bezproblemowo, zatem odpal w przegladarce adres:

> http://localhost/phpmyadmin

Zakladam ze masz phpadmina w polskiej wersji jezykowej. W przeciwnym przypadku w razie problemow daj znac.
Po zalogowaniu utworz nowa baze danych o nazwie tribet:

1. Kliknij w "Bazy danych":

[![tribet1.png](https://i.postimg.cc/HL14zhGD/tribet1.png)](https://postimg.cc/tsDxCDC2)

2. W polu nazwy wpisz oczywiscie tribet:

[![tribet2.png](https://i.postimg.cc/FKPbvsdm/tribet2.png)](https://postimg.cc/rdrRCTNY)

3) Przejdz do uprawnien:

[![tribet3.png](https://i.postimg.cc/FHSsmPYK/tribet3.png)](https://postimg.cc/pm2tJJYN)

4) Kliknij:

[![tribet4.png](https://i.postimg.cc/RV5vd7s5/tribet4.png)](https://postimg.cc/s1mFDhCw)

5) Jako nazwe uzytkownika wpisz "toor", jako haslo wpisz rowniez "toor"

[![tribet5.png](https://i.postimg.cc/YSV597TK/tribet5.png)](https://postimg.cc/yJmQQMVv)

6) Na tej samej stronie przejdz nizej aby okreslic uprawnienia:

[![tribet6.png](https://i.postimg.cc/T2Qcq0Yh/tribet6.png)](https://postimg.cc/CdBDwj8V)


## Instalacja projektu

1. Utworz lokalnie (Twoj komputer) w dowolnym, wygodnym dla siebie miejscu katalog o nazwie "TRIBET PROJECT".
2. Przejdz do tak utworzonego folderu i otworz konsole CMD (shift + right click, "Open Command Window" albo cos w tym stylu).
3. Skopiuj i wklej polecenie: (klonowanie repozytorium znajdujacego sie w chmurze Bitbucket.com)
```sh
git clone https://tribet666@bitbucket.org/tribet666/tribet.git
``` 
5. Otworz projekt w swoim edytorze - zakladam ze masz zainstalowany vsCode jesli nie to polecam bo wg mnie jest to w tej chwili najlepsza bezplatna opcja.
6. Usun z glownego katalogu projektu plik o nazwie "composer.lock"
7. Z menu glownym edytora otworz sobie konsole:
> Terminal->New Terminal
4. W terminalu (upewnij sie ze jestes w katalogu "tribet") sciagnij zaleznosci dla projektu (biblioteki, ktore wykorzystuje projekt) poleceniem:
```sh
composer install
``` 
## Baza danych (cz.2)
1. Ponownie odpal phpmyadmin w przegladarce:

> http://localhost/phpmyadmin

2. Wybierz utworzona wczesniej baze danych "tribet"

[![tribet7.png](https://i.postimg.cc/GtTddCr9/tribet7.png)](https://postimg.cc/H8mf9FrC)

3. Kliknij w "SQL"  (oczywiscie na tym etapie Twoje okno bedzie chwilowo roznilo sie od tego na zdjeciu)

[![tribet8.png](https://i.postimg.cc/bY42bYZC/tribet8.png)](https://postimg.cc/yWXdCHXc)

4. Znajdz i otworz plik "tribet.sql" znajdujacy sie w katalogu "tribet/sql". Skopiuj jego zawartosc i wklej do okna zapytania. Kliknij wykonaj.

[![tribet9.png](https://i.postimg.cc/c44w0k8Z/tribet9.png)](https://postimg.cc/9zKrBppN)

5. Pliki, ktore przeslalem Ci poczta (temp.txt-klucz kodowania oraz temp2.txt-klucz recaptcha4), zawieraja poufne kody. Zwyczajowo takich informacji nie przechowuje sie w repozytorium.
Nalezy umiescic je w katalogu glownym projektu tzn w "tribet".

## Serwer HTTP

Jak zauwazyles xampp przydawal nam sie do tej pory jedynie w odniesieniu do phpmyadmin (obsluga bazy danych).
Jesli chodzi o serwer to proponuje Ci wykorzystac developerskie narzedzie dostarczane wraz z php. Jest to opcja bardziej przytepna i elastyczna w porownaniu do samego xampp'a, ktorego wykorzystanie wymaga dodatkowej konfiguracji.
Zatem przejdz ponownie do terminalu (upewnij sie, ze jestes w katalogu glownym projektu "tribet") i wypisz polecenie:
```sh
php -S localhost:8000 -t public
``` 
Od tej chwili projekt jest osiagalny z poziomu przegldarki jako:

> ~~http://localhost:8000/~~ (niestety ustawienia cookies (zapamietywanie wersji jezykowej) daja mozliwosc ustawienia tylko jednej domeny, wiec wybralem te ponizej.

~~lub~~ 

> http://127.0.0.1:8000/
> http://127.0.0.1:8000/dashboard

## Uwagi koncowe

Ta pozycja pewnie z czasem bedzie stawala sie coraz wieksza. W tej chwili zacznijmy mysql.

1. Pamietaj, ze projekt do pracy wymaga aktywnego serwera mysql, zatem jesli jutro lub innym czasie odpalisz jedynie serwer HTTP to wywali Ci blad. Musisz zatem uruchomic serwer mysql albo z poziomu CMD (terminal systemowy), albo w terminalu Twojego edytora kodu, albo dla swietego spokoju po prostu odpal xampp'a :).
