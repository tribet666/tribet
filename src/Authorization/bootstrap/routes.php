<?php

declare(strict_types=1);

// Package routes delivered routes:
require_once ROOT_FOLDER . 'vendor/paneric/authentication/routes.php';
require_once ROOT_FOLDER . 'vendor/paneric/authorization/routes.php';
