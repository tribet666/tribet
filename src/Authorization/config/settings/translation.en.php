<?php

$translationModule = [
    'translation_module' => [
        'message_title' => 'Tribet message'
    ],
];

$translationPackage = include ROOT_FOLDER . 'vendor/paneric/authorization/translation.en.php';

$translationModule['translation_module'] = array_merge($translationModule['translation_module'], $translationPackage);

return $translationModule;
