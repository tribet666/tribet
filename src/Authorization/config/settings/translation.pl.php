<?php

$translationModule = [
    'translation_module' => [
        'message_title' => 'Tribet - wiadomość'
    ],
];

$translationPackage = include ROOT_FOLDER . 'vendor/paneric/authorization/translation.pl.php';

$translationModule['translation_module'] = array_merge($translationModule['translation_module'], $translationPackage);

return $translationModule;
