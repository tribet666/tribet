<?php

declare(strict_types=1);

$validationModule = [
    'validation' => [
    ],
];

$validationAuthe = include ROOT_FOLDER . 'vendor/paneric/authentication/validation.php';
$validationAutheCommon = include ROOT_FOLDER . 'vendor/paneric/authentication/validation-common.php';

$validation['validation'] = array_merge(
    $validationModule['validation'],
    $validationAuthe['validation'],
    $validationAutheCommon['validation'],
);

return $validation;
