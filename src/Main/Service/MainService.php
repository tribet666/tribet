<?php

declare(strict_types=1);

namespace App\Main\Service;

use Psr\Http\Message\ServerRequestInterface as Request;

class MainService
{
    public function index(Request $request): void {}
}
