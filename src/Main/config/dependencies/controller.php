<?php

declare(strict_types=1);

use App\Main\Controller\MainController;
use App\Main\Service\MainService;
use DI\Container;
use Paneric\Interfaces\Session\SessionInterface;
use Twig\Environment as Twig;

return [
    MainController::class => static function (Container $container): MainController
    {
        return new MainController(
            $container->get(Twig::class),
            $container->get(SessionInterface::class),
            $container->get(MainService::class)
        );
    },
];
