<?php

declare(strict_types=1);

namespace App\Main\Controller;

use App\Main\Service\MainService;
use Paneric\Controller\AppController;
use Paneric\Interfaces\Session\SessionInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Twig\Environment as Twig;

class MainController extends AppController
{
//    private $mainService;
//
//    public function __construct(Twig $twig, SessionInterface $session, MainService $mainService)
//    {
//        parent:: __construct($twig, $session);
//
//        $this->mainService = $mainService;
//    }

    public function index(Request $request, Response $response): Response
    {
        return $this->redirect(
            $response,
            '/produkty',
            200
        );
    }

    public function showArticlesPanel(Response $response, string $name): Response
    {
        return $this->render(
            $response,
            "@module/$name.html.twig"
        );
    }
}
