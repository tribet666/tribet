<?php

declare(strict_types=1);

namespace Paneric\ECommerce\ECommerce\Service;

use Paneric\CSRTriad\Service;
use Paneric\ECommerce\ECommerce\DTO\InvoiceDTO;
use Psr\Http\Message\ServerRequestInterface as Request;
use Paneric\Interfaces\Session\SessionInterface;

class InvoiceService extends Service
{
    protected $invoiceRepository;

    public function __construct(
        RepositoryInterface $invoiceRepository,
        SessionInterface $session
    ) {
        parent::__construct($session);

        $this->invoiceRepository = $invoiceRepository;
    }


    public function getAll(): array
    {
        $this->session->setFlash(['page_title' => 'content_invoices_show_title'], 'value');

        return [
            'invoices' => $this->jsonSerializeObjects($this->invoiceRepository->findAll())
        ];
    }

    public function get(int $invoiceId): ?object
    {
        return $this->invoiceRepository->findOneById($invoiceId);
    }

    public function create(Request $request): ?array
    {
        $attributes = [];
        $validation = [];

        $invoiceDTO = new InvoiceDTO();

        if ($request->getMethod() === 'POST') {

            $validation = $request->getAttribute('validation')[InvoiceDTO::class];

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

//                $uniqueAttributes = [
//                    'attribute_1' => $attributes['attribute_1'],
//                    'attribute_2' => $attributes['attribute_2']
//                ];
//
//                if ($this->invoiceRepository->findByEnhanced($uniqueAttributes, 'OR') === null) {
                    return $this->invoiceRepository->create($invoiceDTO->hydrate($attributes));
//                }
//
//                $this->session->setFlash(['db_invoice_add_error'], 'error');
            }
        }

        return [
            'invoice' => $invoiceDTO->hydrate($attributes),
            'validation' => $validation
        ];
    }

    public function update(int $invoiceId, Request $request): ?array
    {
        $validation = [];

        $invoiceDTO = new InvoiceDTO();

        $method = $request->getMethod();

        if ($method === 'POST') {

            $validation = $request->getAttribute('validation')[InvoiceDTO::class];

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

//                $uniqueAttributes = [
//                    'attribute_1' => $attributes['attribute_1'],
//                    'attribute_2' => $attributes['attribute_2']
//                ];

//                if ($this->invoiceRepository->findByEnhanced($uniqueAttributes, 'OR', $invoiceId) === null) {
                    return $this->invoiceRepository->update(
                        $invoiceId,
                        $invoiceDTO->hydrate($attributes)
                    );
//                }
//
//                $this->session->setFlash(['db_invoice_edit_error'], 'error');
            }
        }

        return [
            'invoice' => $this->invoiceRepository->findOneById($invoiceId),
            'validation' => $validation
        ];
    }

    public function delete(Request $request, int $invoiceId): ?array
    {
        if ($request->getMethod() === 'POST') {

            return $this->invoiceRepository->remove($invoiceId);
        }

        $this->session->setFlash(['invoice_delete_warning'], 'warning');

        return [];
    }
}
