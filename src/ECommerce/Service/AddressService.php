<?php

declare(strict_types=1);

namespace Paneric/ECommerce\ECommerce\Service;

use Paneric\CSRTriad\Service;
use Paneric/ECommerce\ECommerce\DTO\AddressDTO;
use Psr\Http\Message\ServerRequestInterface as Request;
use Paneric\Interfaces\Session\SessionInterface;

class AddressService extends Service
{
    protected $addressRepository;

    public function __construct(
        RepositoryInterface $addressRepository,
        SessionInterface $session
    ) {
        parent::__construct($session);

        $this->addressRepository = $addressRepository;
    }


    public function getAll(): array
    {
        $this->session->setFlash(['page_title' => 'content_addresses_show_title'], 'value');

        return [
            'addresses' => $this->jsonSerializeObjects($this->addressRepository->findAll())
        ];
    }

    public function get(int $addressId): ?object
    {
        return $this->addressRepository->findOneById($addressId);
    }

    public function create(Request $request): ?array
    {
        $attributes = [];
        $validation = [];

        $addressDTO = new AddressDTO();

        if ($request->getMethod() === 'POST') {

            $validation = $request->getAttribute('validation')[AddressDTO::class];

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

//                $uniqueAttributes = [
//                    'attribute_1' => $attributes['attribute_1'],
//                    'attribute_2' => $attributes['attribute_2']
//                ];
//
//                if ($this->addressRepository->findByEnhanced($uniqueAttributes, 'OR') === null) {
                    return $this->addressRepository->create($addressDTO->hydrate($attributes));
//                }
//
//                $this->session->setFlash(['db_address_add_error'], 'error');
            }
        }

        return [
            'address' => $addressDTO->hydrate($attributes),
            'validation' => $validation
        ];
    }

    public function update(int $addressId, Request $request): ?array
    {
        $validation = [];

        $addressDTO = new AddressDTO();

        $method = $request->getMethod();

        if ($method === 'POST') {

            $validation = $request->getAttribute('validation')[AddressDTO::class];

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

//                $uniqueAttributes = [
//                    'attribute_1' => $attributes['attribute_1'],
//                    'attribute_2' => $attributes['attribute_2']
//                ];

//                if ($this->addressRepository->findByEnhanced($uniqueAttributes, 'OR', $addressId) === null) {
                    return $this->addressRepository->update(
                        $addressId,
                        $addressDTO->hydrate($attributes)
                    );
//                }
//
//                $this->session->setFlash(['db_address_edit_error'], 'error');
            }
        }

        return [
            'address' => $this->addressRepository->findOneById($addressId),
            'validation' => $validation
        ];
    }

    public function delete(Request $request, int $addressId): ?array
    {
        if ($request->getMethod() === 'POST') {

            return $this->addressRepository->remove($addressId);
        }

        $this->session->setFlash(['address_delete_warning'], 'warning');

        return [];
    }
}
