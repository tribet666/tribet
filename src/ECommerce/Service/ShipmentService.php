<?php

declare(strict_types=1);

namespace Paneric\ECommerce\ECommerce\Service;

use Paneric\CSRTriad\Service;
use Paneric\ECommerce\ECommerce\DTO\ShipmentDTO;
use Psr\Http\Message\ServerRequestInterface as Request;
use Paneric\Interfaces\Session\SessionInterface;

class ShipmentService extends Service
{
    protected $shipmentRepository;

    public function __construct(
        RepositoryInterface $shipmentRepository,
        SessionInterface $session
    ) {
        parent::__construct($session);

        $this->shipmentRepository = $shipmentRepository;
    }


    public function getAll(): array
    {
        $this->session->setFlash(['page_title' => 'content_shipments_show_title'], 'value');

        return [
            'shipments' => $this->jsonSerializeObjects($this->shipmentRepository->findAll())
        ];
    }

    public function get(int $shipmentId): ?object
    {
        return $this->shipmentRepository->findOneById($shipmentId);
    }

    public function create(Request $request): ?array
    {
        $attributes = [];
        $validation = [];

        $shipmentDTO = new ShipmentDTO();

        if ($request->getMethod() === 'POST') {

            $validation = $request->getAttribute('validation')[ShipmentDTO::class];

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

//                $uniqueAttributes = [
//                    'attribute_1' => $attributes['attribute_1'],
//                    'attribute_2' => $attributes['attribute_2']
//                ];
//
//                if ($this->shipmentRepository->findByEnhanced($uniqueAttributes, 'OR') === null) {
                    return $this->shipmentRepository->create($shipmentDTO->hydrate($attributes));
//                }
//
//                $this->session->setFlash(['db_shipment_add_error'], 'error');
            }
        }

        return [
            'shipment' => $shipmentDTO->hydrate($attributes),
            'validation' => $validation
        ];
    }

    public function update(int $shipmentId, Request $request): ?array
    {
        $validation = [];

        $shipmentDTO = new ShipmentDTO();

        $method = $request->getMethod();

        if ($method === 'POST') {

            $validation = $request->getAttribute('validation')[ShipmentDTO::class];

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

//                $uniqueAttributes = [
//                    'attribute_1' => $attributes['attribute_1'],
//                    'attribute_2' => $attributes['attribute_2']
//                ];

//                if ($this->shipmentRepository->findByEnhanced($uniqueAttributes, 'OR', $shipmentId) === null) {
                    return $this->shipmentRepository->update(
                        $shipmentId,
                        $shipmentDTO->hydrate($attributes)
                    );
//                }
//
//                $this->session->setFlash(['db_shipment_edit_error'], 'error');
            }
        }

        return [
            'shipment' => $this->shipmentRepository->findOneById($shipmentId),
            'validation' => $validation
        ];
    }

    public function delete(Request $request, int $shipmentId): ?array
    {
        if ($request->getMethod() === 'POST') {

            return $this->shipmentRepository->remove($shipmentId);
        }

        $this->session->setFlash(['shipment_delete_warning'], 'warning');

        return [];
    }
}
