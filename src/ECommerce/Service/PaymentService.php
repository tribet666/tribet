<?php

declare(strict_types=1);

namespace Paneric\ECommerce\ECommerce\Service;

use Paneric\CSRTriad\Service;
use Paneric\ECommerce\ECommerce\DTO\PaymentDTO;
use Psr\Http\Message\ServerRequestInterface as Request;
use Paneric\Interfaces\Session\SessionInterface;

class PaymentService extends Service
{
    protected $paymentRepository;

    public function __construct(
        RepositoryInterface $paymentRepository,
        SessionInterface $session
    ) {
        parent::__construct($session);

        $this->paymentRepository = $paymentRepository;
    }


    public function getAll(): array
    {
        $this->session->setFlash(['page_title' => 'content_payments_show_title'], 'value');

        return [
            'payments' => $this->jsonSerializeObjects($this->paymentRepository->findAll())
        ];
    }

    public function get(int $paymentId): ?object
    {
        return $this->paymentRepository->findOneById($paymentId);
    }

    public function create(Request $request): ?array
    {
        $attributes = [];
        $validation = [];

        $paymentDTO = new PaymentDTO();

        if ($request->getMethod() === 'POST') {

            $validation = $request->getAttribute('validation')[PaymentDTO::class];

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

//                $uniqueAttributes = [
//                    'attribute_1' => $attributes['attribute_1'],
//                    'attribute_2' => $attributes['attribute_2']
//                ];
//
//                if ($this->paymentRepository->findByEnhanced($uniqueAttributes, 'OR') === null) {
                    return $this->paymentRepository->create($paymentDTO->hydrate($attributes));
//                }
//
//                $this->session->setFlash(['db_payment_add_error'], 'error');
            }
        }

        return [
            'payment' => $paymentDTO->hydrate($attributes),
            'validation' => $validation
        ];
    }

    public function update(int $paymentId, Request $request): ?array
    {
        $validation = [];

        $paymentDTO = new PaymentDTO();

        $method = $request->getMethod();

        if ($method === 'POST') {

            $validation = $request->getAttribute('validation')[PaymentDTO::class];

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

//                $uniqueAttributes = [
//                    'attribute_1' => $attributes['attribute_1'],
//                    'attribute_2' => $attributes['attribute_2']
//                ];

//                if ($this->paymentRepository->findByEnhanced($uniqueAttributes, 'OR', $paymentId) === null) {
                    return $this->paymentRepository->update(
                        $paymentId,
                        $paymentDTO->hydrate($attributes)
                    );
//                }
//
//                $this->session->setFlash(['db_payment_edit_error'], 'error');
            }
        }

        return [
            'payment' => $this->paymentRepository->findOneById($paymentId),
            'validation' => $validation
        ];
    }

    public function delete(Request $request, int $paymentId): ?array
    {
        if ($request->getMethod() === 'POST') {

            return $this->paymentRepository->remove($paymentId);
        }

        $this->session->setFlash(['payment_delete_warning'], 'warning');

        return [];
    }
}
