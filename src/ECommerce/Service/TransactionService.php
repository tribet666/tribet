<?php

declare(strict_types=1);

namespace Paneric\ECommerce\ECommerce\Service;

use Paneric\CSRTriad\Service;
use Paneric\ECommerce\ECommerce\DTO\TransactionDTO;
use Psr\Http\Message\ServerRequestInterface as Request;
use Paneric\Interfaces\Session\SessionInterface;

class TransactionService extends Service
{
    protected $transactionRepository;

    public function __construct(
        RepositoryInterface $transactionRepository,
        SessionInterface $session
    ) {
        parent::__construct($session);

        $this->transactionRepository = $transactionRepository;
    }


    public function getAll(): array
    {
        $this->session->setFlash(['page_title' => 'content_transactions_show_title'], 'value');

        return [
            'transactions' => $this->jsonSerializeObjects($this->transactionRepository->findAll())
        ];
    }

    public function get(int $transactionId): ?object
    {
        return $this->transactionRepository->findOneById($transactionId);
    }

    public function create(Request $request): ?array
    {
        $attributes = [];
        $validation = [];

        $transactionDTO = new TransactionDTO();

        if ($request->getMethod() === 'POST') {

            $validation = $request->getAttribute('validation')[TransactionDTO::class];

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

//                $uniqueAttributes = [
//                    'attribute_1' => $attributes['attribute_1'],
//                    'attribute_2' => $attributes['attribute_2']
//                ];
//
//                if ($this->transactionRepository->findByEnhanced($uniqueAttributes, 'OR') === null) {
                    return $this->transactionRepository->create($transactionDTO->hydrate($attributes));
//                }
//
//                $this->session->setFlash(['db_transaction_add_error'], 'error');
            }
        }

        return [
            'transaction' => $transactionDTO->hydrate($attributes),
            'validation' => $validation
        ];
    }

    public function update(int $transactionId, Request $request): ?array
    {
        $validation = [];

        $transactionDTO = new TransactionDTO();

        $method = $request->getMethod();

        if ($method === 'POST') {

            $validation = $request->getAttribute('validation')[TransactionDTO::class];

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

//                $uniqueAttributes = [
//                    'attribute_1' => $attributes['attribute_1'],
//                    'attribute_2' => $attributes['attribute_2']
//                ];

//                if ($this->transactionRepository->findByEnhanced($uniqueAttributes, 'OR', $transactionId) === null) {
                    return $this->transactionRepository->update(
                        $transactionId,
                        $transactionDTO->hydrate($attributes)
                    );
//                }
//
//                $this->session->setFlash(['db_transaction_edit_error'], 'error');
            }
        }

        return [
            'transaction' => $this->transactionRepository->findOneById($transactionId),
            'validation' => $validation
        ];
    }

    public function delete(Request $request, int $transactionId): ?array
    {
        if ($request->getMethod() === 'POST') {

            return $this->transactionRepository->remove($transactionId);
        }

        $this->session->setFlash(['transaction_delete_warning'], 'warning');

        return [];
    }
}
