<?php

declare(strict_types=1);

namespace Paneric\ECommerce\ECommerce\Service;

use Paneric\CSRTriad\Service;
use Paneric\ECommerce\ECommerce\DTO\ContactDTO;
use Psr\Http\Message\ServerRequestInterface as Request;
use Paneric\Interfaces\Session\SessionInterface;

class ContactService extends Service
{
    protected $contactRepository;

    public function __construct(
        RepositoryInterface $contactRepository,
        SessionInterface $session
    ) {
        parent::__construct($session);

        $this->contactRepository = $contactRepository;
    }


    public function getAll(): array
    {
        $this->session->setFlash(['page_title' => 'content_contacts_show_title'], 'value');

        return [
            'contacts' => $this->jsonSerializeObjects($this->contactRepository->findAll())
        ];
    }

    public function get(int $contactId): ?object
    {
        return $this->contactRepository->findOneById($contactId);
    }

    public function create(Request $request): ?array
    {
        $attributes = [];
        $validation = [];

        $contactDTO = new ContactDTO();

        if ($request->getMethod() === 'POST') {

            $validation = $request->getAttribute('validation')[ContactDTO::class];

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

//                $uniqueAttributes = [
//                    'attribute_1' => $attributes['attribute_1'],
//                    'attribute_2' => $attributes['attribute_2']
//                ];
//
//                if ($this->contactRepository->findByEnhanced($uniqueAttributes, 'OR') === null) {
                    return $this->contactRepository->create($contactDTO->hydrate($attributes));
//                }
//
//                $this->session->setFlash(['db_contact_add_error'], 'error');
            }
        }

        return [
            'contact' => $contactDTO->hydrate($attributes),
            'validation' => $validation
        ];
    }

    public function update(int $contactId, Request $request): ?array
    {
        $validation = [];

        $contactDTO = new ContactDTO();

        $method = $request->getMethod();

        if ($method === 'POST') {

            $validation = $request->getAttribute('validation')[ContactDTO::class];

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

//                $uniqueAttributes = [
//                    'attribute_1' => $attributes['attribute_1'],
//                    'attribute_2' => $attributes['attribute_2']
//                ];

//                if ($this->contactRepository->findByEnhanced($uniqueAttributes, 'OR', $contactId) === null) {
                    return $this->contactRepository->update(
                        $contactId,
                        $contactDTO->hydrate($attributes)
                    );
//                }
//
//                $this->session->setFlash(['db_contact_edit_error'], 'error');
            }
        }

        return [
            'contact' => $this->contactRepository->findOneById($contactId),
            'validation' => $validation
        ];
    }

    public function delete(Request $request, int $contactId): ?array
    {
        if ($request->getMethod() === 'POST') {

            return $this->contactRepository->remove($contactId);
        }

        $this->session->setFlash(['contact_delete_warning'], 'warning');

        return [];
    }
}
