<?php

declare(strict_types=1);

namespace Paneric\ECommerce\ECommerce\Service;

use Paneric\CSRTriad\Service;
use Paneric\ECommerce\ECommerce\DTO\CartDTO;
use Psr\Http\Message\ServerRequestInterface as Request;
use Paneric\Interfaces\Session\SessionInterface;

class CartService extends Service
{
    protected $cartRepository;

    public function __construct(
        RepositoryInterface $cartRepository,
        SessionInterface $session
    ) {
        parent::__construct($session);

        $this->cartRepository = $cartRepository;
    }


    public function getAll(): array
    {
        $this->session->setFlash(['page_title' => 'content_carts_show_title'], 'value');

        return [
            'carts' => $this->jsonSerializeObjects($this->cartRepository->findAll())
        ];
    }

    public function get(int $cartId): ?object
    {
        return $this->cartRepository->findOneById($cartId);
    }

    public function create(Request $request): ?array
    {
        $attributes = [];
        $validation = [];

        $cartDTO = new CartDTO();

        if ($request->getMethod() === 'POST') {

            $validation = $request->getAttribute('validation')[CartDTO::class];

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

//                $uniqueAttributes = [
//                    'attribute_1' => $attributes['attribute_1'],
//                    'attribute_2' => $attributes['attribute_2']
//                ];
//
//                if ($this->cartRepository->findByEnhanced($uniqueAttributes, 'OR') === null) {
                    return $this->cartRepository->create($cartDTO->hydrate($attributes));
//                }
//
//                $this->session->setFlash(['db_cart_add_error'], 'error');
            }
        }

        return [
            'cart' => $cartDTO->hydrate($attributes),
            'validation' => $validation
        ];
    }

    public function update(int $cartId, Request $request): ?array
    {
        $validation = [];

        $cartDTO = new CartDTO();

        $method = $request->getMethod();

        if ($method === 'POST') {

            $validation = $request->getAttribute('validation')[CartDTO::class];

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

//                $uniqueAttributes = [
//                    'attribute_1' => $attributes['attribute_1'],
//                    'attribute_2' => $attributes['attribute_2']
//                ];

//                if ($this->cartRepository->findByEnhanced($uniqueAttributes, 'OR', $cartId) === null) {
                    return $this->cartRepository->update(
                        $cartId,
                        $cartDTO->hydrate($attributes)
                    );
//                }
//
//                $this->session->setFlash(['db_cart_edit_error'], 'error');
            }
        }

        return [
            'cart' => $this->cartRepository->findOneById($cartId),
            'validation' => $validation
        ];
    }

    public function delete(Request $request, int $cartId): ?array
    {
        if ($request->getMethod() === 'POST') {

            return $this->cartRepository->remove($cartId);
        }

        $this->session->setFlash(['cart_delete_warning'], 'warning');

        return [];
    }
}
