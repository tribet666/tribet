<?php

declare(strict_types=1);

namespace Paneric\ECommerce\ECommerce\Service;

use Paneric\CSRTriad\Service;
use Paneric\ECommerce\ECommerce\DTO\OrderDTO;
use Psr\Http\Message\ServerRequestInterface as Request;
use Paneric\Interfaces\Session\SessionInterface;

class OrderService extends Service
{
    protected $orderRepository;

    public function __construct(
        RepositoryInterface $orderRepository,
        SessionInterface $session
    ) {
        parent::__construct($session);

        $this->orderRepository = $orderRepository;
    }


    public function getAll(): array
    {
        $this->session->setFlash(['page_title' => 'content_orders_show_title'], 'value');

        return [
            'orders' => $this->jsonSerializeObjects($this->orderRepository->findAll())
        ];
    }

    public function get(int $orderId): ?object
    {
        return $this->orderRepository->findOneById($orderId);
    }

    public function create(Request $request): ?array
    {
        $attributes = [];
        $validation = [];

        $orderDTO = new OrderDTO();

        if ($request->getMethod() === 'POST') {

            $validation = $request->getAttribute('validation')[OrderDTO::class];

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

//                $uniqueAttributes = [
//                    'attribute_1' => $attributes['attribute_1'],
//                    'attribute_2' => $attributes['attribute_2']
//                ];
//
//                if ($this->orderRepository->findByEnhanced($uniqueAttributes, 'OR') === null) {
                    return $this->orderRepository->create($orderDTO->hydrate($attributes));
//                }
//
//                $this->session->setFlash(['db_order_add_error'], 'error');
            }
        }

        return [
            'order' => $orderDTO->hydrate($attributes),
            'validation' => $validation
        ];
    }

    public function update(int $orderId, Request $request): ?array
    {
        $validation = [];

        $orderDTO = new OrderDTO();

        $method = $request->getMethod();

        if ($method === 'POST') {

            $validation = $request->getAttribute('validation')[OrderDTO::class];

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

//                $uniqueAttributes = [
//                    'attribute_1' => $attributes['attribute_1'],
//                    'attribute_2' => $attributes['attribute_2']
//                ];

//                if ($this->orderRepository->findByEnhanced($uniqueAttributes, 'OR', $orderId) === null) {
                    return $this->orderRepository->update(
                        $orderId,
                        $orderDTO->hydrate($attributes)
                    );
//                }
//
//                $this->session->setFlash(['db_order_edit_error'], 'error');
            }
        }

        return [
            'order' => $this->orderRepository->findOneById($orderId),
            'validation' => $validation
        ];
    }

    public function delete(Request $request, int $orderId): ?array
    {
        if ($request->getMethod() === 'POST') {

            return $this->orderRepository->remove($orderId);
        }

        $this->session->setFlash(['order_delete_warning'], 'warning');

        return [];
    }
}
