<?php

declare(strict_types=1);

namespace Paneric\ECommerce\ECommerce\Service;

use Paneric\CSRTriad\Service;
use Paneric\ECommerce\ECommerce\DTO\CompanyDTO;
use Psr\Http\Message\ServerRequestInterface as Request;
use Paneric\Interfaces\Session\SessionInterface;

class CompanyService extends Service
{
    protected $companyRepository;

    public function __construct(
        RepositoryInterface $companyRepository,
        SessionInterface $session
    ) {
        parent::__construct($session);

        $this->companyRepository = $companyRepository;
    }


    public function getAll(): array
    {
        $this->session->setFlash(['page_title' => 'content_companys_show_title'], 'value');

        return [
            'companys' => $this->jsonSerializeObjects($this->companyRepository->findAll())
        ];
    }

    public function get(int $companyId): ?object
    {
        return $this->companyRepository->findOneById($companyId);
    }

    public function create(Request $request): ?array
    {
        $attributes = [];
        $validation = [];

        $companyDTO = new CompanyDTO();

        if ($request->getMethod() === 'POST') {

            $validation = $request->getAttribute('validation')[CompanyDTO::class];

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

//                $uniqueAttributes = [
//                    'attribute_1' => $attributes['attribute_1'],
//                    'attribute_2' => $attributes['attribute_2']
//                ];
//
//                if ($this->companyRepository->findByEnhanced($uniqueAttributes, 'OR') === null) {
                    return $this->companyRepository->create($companyDTO->hydrate($attributes));
//                }
//
//                $this->session->setFlash(['db_company_add_error'], 'error');
            }
        }

        return [
            'company' => $companyDTO->hydrate($attributes),
            'validation' => $validation
        ];
    }

    public function update(int $companyId, Request $request): ?array
    {
        $validation = [];

        $companyDTO = new CompanyDTO();

        $method = $request->getMethod();

        if ($method === 'POST') {

            $validation = $request->getAttribute('validation')[CompanyDTO::class];

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

//                $uniqueAttributes = [
//                    'attribute_1' => $attributes['attribute_1'],
//                    'attribute_2' => $attributes['attribute_2']
//                ];

//                if ($this->companyRepository->findByEnhanced($uniqueAttributes, 'OR', $companyId) === null) {
                    return $this->companyRepository->update(
                        $companyId,
                        $companyDTO->hydrate($attributes)
                    );
//                }
//
//                $this->session->setFlash(['db_company_edit_error'], 'error');
            }
        }

        return [
            'company' => $this->companyRepository->findOneById($companyId),
            'validation' => $validation
        ];
    }

    public function delete(Request $request, int $companyId): ?array
    {
        if ($request->getMethod() === 'POST') {

            return $this->companyRepository->remove($companyId);
        }

        $this->session->setFlash(['company_delete_warning'], 'warning');

        return [];
    }
}
