<?php

declare(strict_types=1);

namespace Paneric\ECommerce\ECommerce\Controller;

use Paneric\Controller\AppController;
use Paneric\ECommerce\ECommerce\Service\ContactService;
use Paneric\Interfaces\Session\SessionInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Twig\Environment as Twig;

class ContactController extends AppController
{
    protected $contactService;

    public function __construct(Twig $twig, SessionInterface $session, ContactService $contactService)
    {
        parent:: __construct($twig, $session);

        $this->contactService = $contactService;
    }


    public function showAll(Response $response): Response
    {
        return $this->render(
            $response,
            '@eCommerce/contact/show_all.html.twig',
            $this->contactService->getAll()
        );
    }

    public function show(Response $response, int $id): Response
    {
        return $this->render(
            $response,
            '@eCommerce/contact/show.html.twig',
            ['contact' => $this->contactService->getOneById($id)]
        );
    }

    public function add(Request $request, Response $response): Response
    {
        $result = $this->contactService->create($request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/eCommerce/contacts/show',
                200
            );
        }

        return $this->render(
            $response,
            '@eCommerce/contact/add.html.twig',
            $result
        );
    }

    public function edit(Request $request, Response $response, int $id): Response
    {
        $result = $this->contactService->update($id, $request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/eCommerce/contacts/show',
                200
            );
        }

        return $this->render(
            $response,
            '@eCommerce/contact/edit.html.twig',
            $result
        );
    }

    public function delete(Request $request, Response $response, int $id): Response
    {
        $result = $this->contactService->delete($request, $id);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/eCommerce/contacts/show',
                200
            );
        }

        return $this->render(
            $response,
            '@eCommerce/contact/delete.html.twig',
            ['id' => $id]
        );
    }
}
