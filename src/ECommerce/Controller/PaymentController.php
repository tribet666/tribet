<?php

declare(strict_types=1);

namespace Paneric\ECommerce\ECommerce\Controller;

use Paneric\Controller\AppController;
use Paneric\ECommerce\ECommerce\Service\PaymentService;
use Paneric\Interfaces\Session\SessionInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Twig\Environment as Twig;

class PaymentController extends AppController
{
    protected $paymentService;

    public function __construct(Twig $twig, SessionInterface $session, PaymentService $paymentService)
    {
        parent:: __construct($twig, $session);

        $this->paymentService = $paymentService;
    }


    public function showAll(Response $response): Response
    {
        return $this->render(
            $response,
            '@eCommerce/payment/show_all.html.twig',
            $this->paymentService->getAll()
        );
    }

    public function show(Response $response, int $id): Response
    {
        return $this->render(
            $response,
            '@eCommerce/payment/show.html.twig',
            ['payment' => $this->paymentService->getOneById($id)]
        );
    }

    public function add(Request $request, Response $response): Response
    {
        $result = $this->paymentService->create($request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/eCommerce/payments/show',
                200
            );
        }

        return $this->render(
            $response,
            '@eCommerce/payment/add.html.twig',
            $result
        );
    }

    public function edit(Request $request, Response $response, int $id): Response
    {
        $result = $this->paymentService->update($id, $request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/eCommerce/payments/show',
                200
            );
        }

        return $this->render(
            $response,
            '@eCommerce/payment/edit.html.twig',
            $result
        );
    }

    public function delete(Request $request, Response $response, int $id): Response
    {
        $result = $this->paymentService->delete($request, $id);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/eCommerce/payments/show',
                200
            );
        }

        return $this->render(
            $response,
            '@eCommerce/payment/delete.html.twig',
            ['id' => $id]
        );
    }
}
