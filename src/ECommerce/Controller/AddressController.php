<?php

declare(strict_types=1);

namespace Paneric\ECommerce\ECommerce\Controller;

use Paneric\Controller\AppController;
use Paneric\ECommerce\ECommerce\Service\AddressService;
use Paneric\Interfaces\Session\SessionInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Twig\Environment as Twig;

class AddressController extends AppController
{
    protected $addressService;

    public function __construct(Twig $twig, SessionInterface $session, AddressService $addressService)
    {
        parent:: __construct($twig, $session);

        $this->addressService = $addressService;
    }


    public function showAll(Response $response): Response
    {
        return $this->render(
            $response,
            '@eCommerce/address/show_all.html.twig',
            $this->addressService->getAll()
        );
    }

    public function show(Response $response, int $id): Response
    {
        return $this->render(
            $response,
            '@eCommerce/address/show.html.twig',
            ['address' => $this->addressService->getOneById($id)]
        );
    }

    public function add(Request $request, Response $response): Response
    {
        $result = $this->addressService->create($request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/eCommerce/addresses/show',
                200
            );
        }

        return $this->render(
            $response,
            '@eCommerce/address/add.html.twig',
            $result
        );
    }

    public function edit(Request $request, Response $response, int $id): Response
    {
        $result = $this->addressService->update($id, $request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/eCommerce/addresses/show',
                200
            );
        }

        return $this->render(
            $response,
            '@eCommerce/address/edit.html.twig',
            $result
        );
    }

    public function delete(Request $request, Response $response, int $id): Response
    {
        $result = $this->addressService->delete($request, $id);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/eCommerce/addresses/show',
                200
            );
        }

        return $this->render(
            $response,
            '@eCommerce/address/delete.html.twig',
            ['id' => $id]
        );
    }
}
