<?php

declare(strict_types=1);

namespace Paneric\ECommerce\ECommerce\Controller;

use Paneric\Controller\AppController;
use Paneric\ECommerce\ECommerce\Service\CompanyService;
use Paneric\Interfaces\Session\SessionInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Twig\Environment as Twig;

class CompanyController extends AppController
{
    protected $companyService;

    public function __construct(Twig $twig, SessionInterface $session, CompanyService $companyService)
    {
        parent:: __construct($twig, $session);

        $this->companyService = $companyService;
    }


    public function showAll(Response $response): Response
    {
        return $this->render(
            $response,
            '@eCommerce/company/show_all.html.twig',
            $this->companyService->getAll()
        );
    }

    public function show(Response $response, int $id): Response
    {
        return $this->render(
            $response,
            '@eCommerce/company/show.html.twig',
            ['company' => $this->companyService->getOneById($id)]
        );
    }

    public function add(Request $request, Response $response): Response
    {
        $result = $this->companyService->create($request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/eCommerce/companys/show',
                200
            );
        }

        return $this->render(
            $response,
            '@eCommerce/company/add.html.twig',
            $result
        );
    }

    public function edit(Request $request, Response $response, int $id): Response
    {
        $result = $this->companyService->update($id, $request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/eCommerce/companys/show',
                200
            );
        }

        return $this->render(
            $response,
            '@eCommerce/company/edit.html.twig',
            $result
        );
    }

    public function delete(Request $request, Response $response, int $id): Response
    {
        $result = $this->companyService->delete($request, $id);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/eCommerce/companys/show',
                200
            );
        }

        return $this->render(
            $response,
            '@eCommerce/company/delete.html.twig',
            ['id' => $id]
        );
    }
}
