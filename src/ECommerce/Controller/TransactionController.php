<?php

declare(strict_types=1);

namespace Paneric\ECommerce\ECommerce\Controller;

use Paneric\Controller\AppController;
use Paneric\ECommerce\ECommerce\Service\TransactionService;
use Paneric\Interfaces\Session\SessionInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Twig\Environment as Twig;

class TransactionController extends AppController
{
    protected $transactionService;

    public function __construct(Twig $twig, SessionInterface $session, TransactionService $transactionService)
    {
        parent:: __construct($twig, $session);

        $this->transactionService = $transactionService;
    }


    public function showAll(Response $response): Response
    {
        return $this->render(
            $response,
            '@eCommerce/transaction/show_all.html.twig',
            $this->transactionService->getAll()
        );
    }

    public function show(Response $response, int $id): Response
    {
        return $this->render(
            $response,
            '@eCommerce/transaction/show.html.twig',
            ['transaction' => $this->transactionService->getOneById($id)]
        );
    }

    public function add(Request $request, Response $response): Response
    {
        $result = $this->transactionService->create($request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/eCommerce/transactions/show',
                200
            );
        }

        return $this->render(
            $response,
            '@eCommerce/transaction/add.html.twig',
            $result
        );
    }

    public function edit(Request $request, Response $response, int $id): Response
    {
        $result = $this->transactionService->update($id, $request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/eCommerce/transactions/show',
                200
            );
        }

        return $this->render(
            $response,
            '@eCommerce/transaction/edit.html.twig',
            $result
        );
    }

    public function delete(Request $request, Response $response, int $id): Response
    {
        $result = $this->transactionService->delete($request, $id);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/eCommerce/transactions/show',
                200
            );
        }

        return $this->render(
            $response,
            '@eCommerce/transaction/delete.html.twig',
            ['id' => $id]
        );
    }
}
