<?php

declare(strict_types=1);

namespace Paneric\ECommerce\ECommerce\Controller;

use Paneric\Controller\AppController;
use Paneric\ECommerce\ECommerce\Service\OrderService;
use Paneric\Interfaces\Session\SessionInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Twig\Environment as Twig;

class OrderController extends AppController
{
    protected $orderService;

    public function __construct(Twig $twig, SessionInterface $session, OrderService $orderService)
    {
        parent:: __construct($twig, $session);

        $this->orderService = $orderService;
    }


    public function showAll(Response $response): Response
    {
        return $this->render(
            $response,
            '@eCommerce/order/show_all.html.twig',
            $this->orderService->getAll()
        );
    }

    public function show(Response $response, int $id): Response
    {
        return $this->render(
            $response,
            '@eCommerce/order/show.html.twig',
            ['order' => $this->orderService->getOneById($id)]
        );
    }

    public function add(Request $request, Response $response): Response
    {
        $result = $this->orderService->create($request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/eCommerce/orders/show',
                200
            );
        }

        return $this->render(
            $response,
            '@eCommerce/order/add.html.twig',
            $result
        );
    }

    public function edit(Request $request, Response $response, int $id): Response
    {
        $result = $this->orderService->update($id, $request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/eCommerce/orders/show',
                200
            );
        }

        return $this->render(
            $response,
            '@eCommerce/order/edit.html.twig',
            $result
        );
    }

    public function delete(Request $request, Response $response, int $id): Response
    {
        $result = $this->orderService->delete($request, $id);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/eCommerce/orders/show',
                200
            );
        }

        return $this->render(
            $response,
            '@eCommerce/order/delete.html.twig',
            ['id' => $id]
        );
    }
}
