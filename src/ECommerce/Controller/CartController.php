<?php

declare(strict_types=1);

namespace Paneric\ECommerce\ECommerce\Controller;

use Paneric\Controller\AppController;
use Paneric\ECommerce\ECommerce\Service\CartService;
use Paneric\Interfaces\Session\SessionInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Twig\Environment as Twig;

class CartController extends AppController
{
    protected $cartService;

    public function __construct(Twig $twig, SessionInterface $session, CartService $cartService)
    {
        parent:: __construct($twig, $session);

        $this->cartService = $cartService;
    }


    public function showAll(Response $response): Response
    {
        return $this->render(
            $response,
            '@eCommerce/cart/show_all.html.twig',
            $this->cartService->getAll()
        );
    }

    public function show(Response $response, int $id): Response
    {
        return $this->render(
            $response,
            '@eCommerce/cart/show.html.twig',
            ['cart' => $this->cartService->getOneById($id)]
        );
    }

    public function add(Request $request, Response $response): Response
    {
        $result = $this->cartService->create($request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/eCommerce/carts/show',
                200
            );
        }

        return $this->render(
            $response,
            '@eCommerce/cart/add.html.twig',
            $result
        );
    }

    public function edit(Request $request, Response $response, int $id): Response
    {
        $result = $this->cartService->update($id, $request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/eCommerce/carts/show',
                200
            );
        }

        return $this->render(
            $response,
            '@eCommerce/cart/edit.html.twig',
            $result
        );
    }

    public function delete(Request $request, Response $response, int $id): Response
    {
        $result = $this->cartService->delete($request, $id);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/eCommerce/carts/show',
                200
            );
        }

        return $this->render(
            $response,
            '@eCommerce/cart/delete.html.twig',
            ['id' => $id]
        );
    }
}
