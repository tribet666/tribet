<?php

declare(strict_types=1);

namespace Paneric\ECommerce\ECommerce\Controller;

use Paneric\Controller\AppController;
use Paneric\ECommerce\ECommerce\Service\ShipmentService;
use Paneric\Interfaces\Session\SessionInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Twig\Environment as Twig;

class ShipmentController extends AppController
{
    protected $shipmentService;

    public function __construct(Twig $twig, SessionInterface $session, ShipmentService $shipmentService)
    {
        parent:: __construct($twig, $session);

        $this->shipmentService = $shipmentService;
    }


    public function showAll(Response $response): Response
    {
        return $this->render(
            $response,
            '@eCommerce/shipment/show_all.html.twig',
            $this->shipmentService->getAll()
        );
    }

    public function show(Response $response, int $id): Response
    {
        return $this->render(
            $response,
            '@eCommerce/shipment/show.html.twig',
            ['shipment' => $this->shipmentService->getOneById($id)]
        );
    }

    public function add(Request $request, Response $response): Response
    {
        $result = $this->shipmentService->create($request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/eCommerce/shipments/show',
                200
            );
        }

        return $this->render(
            $response,
            '@eCommerce/shipment/add.html.twig',
            $result
        );
    }

    public function edit(Request $request, Response $response, int $id): Response
    {
        $result = $this->shipmentService->update($id, $request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/eCommerce/shipments/show',
                200
            );
        }

        return $this->render(
            $response,
            '@eCommerce/shipment/edit.html.twig',
            $result
        );
    }

    public function delete(Request $request, Response $response, int $id): Response
    {
        $result = $this->shipmentService->delete($request, $id);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/eCommerce/shipments/show',
                200
            );
        }

        return $this->render(
            $response,
            '@eCommerce/shipment/delete.html.twig',
            ['id' => $id]
        );
    }
}
