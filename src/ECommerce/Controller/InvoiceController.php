<?php

declare(strict_types=1);

namespace Paneric\ECommerce\ECommerce\Controller;

use Paneric\Controller\AppController;
use Paneric\ECommerce\ECommerce\Service\InvoiceService;
use Paneric\Interfaces\Session\SessionInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Twig\Environment as Twig;

class InvoiceController extends AppController
{
    protected $invoiceService;

    public function __construct(Twig $twig, SessionInterface $session, InvoiceService $invoiceService)
    {
        parent:: __construct($twig, $session);

        $this->invoiceService = $invoiceService;
    }


    public function showAll(Response $response): Response
    {
        return $this->render(
            $response,
            '@eCommerce/invoice/show_all.html.twig',
            $this->invoiceService->getAll()
        );
    }

    public function show(Response $response, int $id): Response
    {
        return $this->render(
            $response,
            '@eCommerce/invoice/show.html.twig',
            ['invoice' => $this->invoiceService->getOneById($id)]
        );
    }

    public function add(Request $request, Response $response): Response
    {
        $result = $this->invoiceService->create($request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/eCommerce/invoices/show',
                200
            );
        }

        return $this->render(
            $response,
            '@eCommerce/invoice/add.html.twig',
            $result
        );
    }

    public function edit(Request $request, Response $response, int $id): Response
    {
        $result = $this->invoiceService->update($id, $request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/eCommerce/invoices/show',
                200
            );
        }

        return $this->render(
            $response,
            '@eCommerce/invoice/edit.html.twig',
            $result
        );
    }

    public function delete(Request $request, Response $response, int $id): Response
    {
        $result = $this->invoiceService->delete($request, $id);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/eCommerce/invoices/show',
                200
            );
        }

        return $this->render(
            $response,
            '@eCommerce/invoice/delete.html.twig',
            ['id' => $id]
        );
    }
}
