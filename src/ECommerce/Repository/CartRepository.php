<?php

declare(strict_types=1);

namespace Paneric\ECommerce\ECommerce\Repository;

use Paneric\ECommerce\ECommerce\Repository\Interfaces\CartRepositoryInterface;
use Paneric\ECommerce\ECommerce\DTO\CartDTO;
use Paneric\PdoWrapper\Manager;
use PDO;

class CartRepository extends PDORepository implements CartRepositoryInterface
{
    public function __construct(Manager $manager)
    {
        parent::__construct($manager);

        $this->table = 'carts';
        $this->dtoClass = CartDTO::class;
        $this->fetchMode = PDO::FETCH_CLASS;
    }
}
