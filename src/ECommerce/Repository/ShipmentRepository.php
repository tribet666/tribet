<?php

declare(strict_types=1);

namespace Paneric\ECommerce\ECommerce\Repository;

use Paneric\ECommerce\ECommerce\Repository\Interfaces\ShipmentRepositoryInterface;
use Paneric\ECommerce\ECommerce\DTO\ShipmentDTO;
use Paneric\PdoWrapper\Manager;
use PDO;

class ShipmentRepository extends PDORepository implements ShipmentRepositoryInterface
{
    public function __construct(Manager $manager)
    {
        parent::__construct($manager);

        $this->table = 'shipments';
        $this->dtoClass = ShipmentDTO::class;
        $this->fetchMode = PDO::FETCH_CLASS;
    }
}
