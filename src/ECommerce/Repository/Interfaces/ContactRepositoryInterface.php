<?php

declare(strict_types=1);

namespace Paneric\ECommerce\ECommerce\Repository\Interfaces;

interface ContactRepositoryInterface
{
    public function findOneById(int $id): ?object;

    public function findAll();

    public function findByEnhanced(array $criteria, string $operator = 'AND', int $id = null): ?array;

    public function create(HydratorInterface $hydrator): ?array;

    public function update(int $id, HydratorInterface $hydrator): ?array;

    public function remove(int $id): ?array;
}
