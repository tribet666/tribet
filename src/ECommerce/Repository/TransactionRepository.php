<?php

declare(strict_types=1);

namespace Paneric\ECommerce\ECommerce\Repository;

use Paneric\ECommerce\ECommerce\Repository\Interfaces\TransactionRepositoryInterface;
use Paneric\ECommerce\ECommerce\DTO\TransactionDTO;
use Paneric\PdoWrapper\Manager;
use PDO;

class TransactionRepository extends PDORepository implements TransactionRepositoryInterface
{
    public function __construct(Manager $manager)
    {
        parent::__construct($manager);

        $this->table = 'transactions';
        $this->dtoClass = TransactionDTO::class;
        $this->fetchMode = PDO::FETCH_CLASS;
    }
}
