<?php

declare(strict_types=1);

namespace Paneric\ECommerce\ECommerce\Repository;

use Paneric\ECommerce\ECommerce\Repository\Interfaces\ContactRepositoryInterface;
use Paneric\ECommerce\ECommerce\DTO\ContactDTO;
use Paneric\PdoWrapper\Manager;
use PDO;

class ContactRepository extends PDORepository implements ContactRepositoryInterface
{
    public function __construct(Manager $manager)
    {
        parent::__construct($manager);

        $this->table = 'contacts';
        $this->dtoClass = ContactDTO::class;
        $this->fetchMode = PDO::FETCH_CLASS;
    }
}
