<?php

declare(strict_types=1);

namespace Paneric\ECommerce\ECommerce\Repository;

use Paneric\ECommerce\ECommerce\Repository\Interfaces\InvoiceRepositoryInterface;
use Paneric\ECommerce\ECommerce\DTO\InvoiceDTO;
use Paneric\PdoWrapper\Manager;
use PDO;

class InvoiceRepository extends PDORepository implements InvoiceRepositoryInterface
{
    public function __construct(Manager $manager)
    {
        parent::__construct($manager);

        $this->table = 'invoices';
        $this->dtoClass = InvoiceDTO::class;
        $this->fetchMode = PDO::FETCH_CLASS;
    }
}
