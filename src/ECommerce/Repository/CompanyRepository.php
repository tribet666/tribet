<?php

declare(strict_types=1);

namespace Paneric\ECommerce\ECommerce\Repository;

use Paneric\ECommerce\ECommerce\Repository\Interfaces\CompanyRepositoryInterface;
use Paneric\ECommerce\ECommerce\DTO\CompanyDTO;
use Paneric\PdoWrapper\Manager;
use PDO;

class CompanyRepository extends PDORepository implements CompanyRepositoryInterface
{
    public function __construct(Manager $manager)
    {
        parent::__construct($manager);

        $this->table = 'companys';
        $this->dtoClass = CompanyDTO::class;
        $this->fetchMode = PDO::FETCH_CLASS;
    }
}
