<?php

declare(strict_types=1);

namespace Paneric\ECommerce\ECommerce\Repository;

use Paneric\ECommerce\ECommerce\Repository\Interfaces\PaymentRepositoryInterface;
use Paneric\ECommerce\ECommerce\DTO\PaymentDTO;
use Paneric\PdoWrapper\Manager;
use PDO;

class PaymentRepository extends PDORepository implements PaymentRepositoryInterface
{
    public function __construct(Manager $manager)
    {
        parent::__construct($manager);

        $this->table = 'payments';
        $this->dtoClass = PaymentDTO::class;
        $this->fetchMode = PDO::FETCH_CLASS;
    }
}
