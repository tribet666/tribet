<?php

declare(strict_types=1);

namespace Paneric/ECommerce\ECommerce\Repository;

use Paneric/ECommerce\ECommerce\Repository\Interfaces\AddressRepositoryInterface;
use Paneric/ECommerce\ECommerce\DTO\AddressDTO;
use Paneric\PdoWrapper\Manager;
use PDO;

class AddressRepository extends PDORepository implements AddressRepositoryInterface
{
    public function __construct(Manager $manager)
    {
        parent::__construct($manager);

        $this->table = 'addresses';
        $this->dtoClass = AddressDTO::class;
        $this->fetchMode = PDO::FETCH_CLASS;
    }
}
