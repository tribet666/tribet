<?php

declare(strict_types=1);

namespace Paneric\ECommerce\ECommerce\Repository;

use Paneric\ECommerce\ECommerce\Repository\Interfaces\OrderRepositoryInterface;
use Paneric\ECommerce\ECommerce\DTO\OrderDTO;
use Paneric\PdoWrapper\Manager;
use PDO;

class OrderRepository extends PDORepository implements OrderRepositoryInterface
{
    public function __construct(Manager $manager)
    {
        parent::__construct($manager);

        $this->table = 'orders';
        $this->dtoClass = OrderDTO::class;
        $this->fetchMode = PDO::FETCH_CLASS;
    }
}
