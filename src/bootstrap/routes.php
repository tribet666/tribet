<?php

declare(strict_types=1);

use App\Article\Controller\CategoryController;
use App\Dashboard\Controller\AdminController;
use App\Dashboard\Controller\CMSController;
use App\Dashboard\Controller\DashboardController;
use App\ECommerce\Controller\ECommerceController;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use App\Main\Controller\MainController;

$app->get('/', function(Request $request, Response $response) {
    return $this->get(MainController::class)->index($request, $response);
})->setName('main.index');

$app->get('/main[/{local}]', function(Request $request, Response $response) {
    return $this->get(MainController::class)->index($request, $response);
})->setName('main.index.local');

$app->get('/produkty', function(Request $request, Response $response) {
    return $this->get(CategoryController::class)->showAsTiles($response);
})->setName('main.index.local');



$app->get('/dashboard[/{local}]', function(Request $request, Response $response) {
    return $this->get(DashboardController::class)->index($request, $response);
})->setName('dashboard.index');

$app->get('/cms[/{local}]', function(Request $request, Response $response) {
    return $this->get(CMSController::class)->index($request, $response);
})->setName('cms.index');

$app->get('/admin[/{local}]', function(Request $request, Response $response) {
    return $this->get(AdminController::class)->index($request, $response);
})->setName('admin.index');

$app->get('/e-commerce[/{local}]', function(Request $request, Response $response) {
    return $this->get(ECommerceController::class)->index($request, $response);
})->setName('e-commerce.index');



include ROOT_FOLDER . 'vendor/paneric/authentication/routes-cross.php';
