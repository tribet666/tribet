<?php

declare(strict_types=1);

// Define Custom Error Handler
//use App\Error\Controller\ErrorController;
//use Psr\Http\Message\ServerRequestInterface;
use Slim\Error\Renderers\HtmlErrorRenderer;
use Zeuxisoo\Whoops\Slim\WhoopsMiddleware;
// TODO: PROD/DEV set Error Handler
//$customErrorHandler = function (
//    ServerRequestInterface $request,
//    Throwable $exception,
//    bool $displayErrorDetails,
//    bool $logErrors,
//    bool $logErrorDetails
//) use ($app) {
//    $payload = [
//        'message' => $exception->getMessage(),
//        'code' => $exception->getCode()
//    ];
//
//    $response = $app->getResponseFactory()->createResponse();
//
//    return $app->getContainer()->get(ErrorController::class)->index($response, $payload);
//};
//
//// always last one
//$errorMiddleware = $app->addErrorMiddleware(
//    true,
//    true,
//    true
//);
//$errorMiddleware->setDefaultErrorHandler($customErrorHandler);

// Whoops:
//if (ENV === 'dev') {
//    $app->add(new WhoopsMiddleware(['enable' => true]));
//} else {
//    $errorMiddleware = $app->addErrorMiddleware(false, true, true);
//    $errorHandler    = $errorMiddleware->getDefaultErrorHandler();
//    $errorHandler->registerErrorRenderer('text/html', HtmlErrorRenderer::class);
//}

$errorMiddleware = $app->addErrorMiddleware(true, true, true);