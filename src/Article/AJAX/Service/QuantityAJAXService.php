<?php

declare(strict_types=1);

namespace App\Article\AJAX\Service;

use App\Article\DTO\OptionsSearchCriteriaDTO;
use App\Article\Repository\Interfaces\CategoryRepositoryInterface;
use App\Article\Repository\Interfaces\FeatureRepositoryInterface;
use App\Article\Repository\Interfaces\OptionRepositoryInterface;
use App\Article\Repository\Interfaces\QuantityRepositoryInterface;
use App\Article\Repository\Interfaces\SubcategoryRepositoryInterface;
use InvalidArgumentException;
use Paneric\CSRTriad\Service;
use Paneric\Interfaces\Session\SessionInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class QuantityAJAXService extends Service
{
    private $categoryRepository;

    private $subcategoryRepository;

    private $featureRepository;

    private $optionRepository;

    private $quantityRepository;

    private $searchCriteria;

    public function __construct(
        SessionInterface $session,
        FeatureRepositoryInterface $featureRepository,
        SubcategoryRepositoryInterface $subcategoryRepository,
        CategoryRepositoryInterface $categoryRepository,
        OptionRepositoryInterface $optionRepository,
        QuantityRepositoryInterface $quantityRepository
    ) {
        parent::__construct($session);

        $this->categoryRepository = $categoryRepository;

        $this->subcategoryRepository = $subcategoryRepository;

        $this->featureRepository = $featureRepository;

        $this->optionRepository = $optionRepository;

        $this->quantityRepository = $quantityRepository;
    }

    public function search(Request $request, string $mode): array
    {
        if (!$request->getMethod() === 'POST') {
            throw new InvalidArgumentException(sprintf('Invalid request method - POST required'));
        }

        $validation = $request->getAttribute('validation')[OptionsSearchCriteriaDTO::class];

        if (!empty($validation)) {
            var_dump($request->getParsedBody());
            var_dump($validation);
            throw new InvalidArgumentException('Invalid criteria');
        }

        $attributes = $request->getParsedBody();

        $this->searchCriteria = new OptionsSearchCriteriaDTO();
        $this->searchCriteria->hydrate($attributes);

        $selectName = ucfirst($this->searchCriteria->getSelectRef());

        if ($mode === 'add' && $selectName !== 'Entrys') {

            $criteria = $this->{'setBy' . $selectName}(
                [
                    'categorys' => [],
                    'subcategorys' => [],
                    'features1' => [],
                    'features2' => [],
                    'feature1_options' => [],
                    'feature2_options' => [],
                    'entrys' => [],
                ]
            );

            $criteria['criteria'] = $this->searchCriteria->convert();

            return $criteria;
        }

        $criteria = $this->setByEntrys(
            [
                'categorys' => [],
                'subcategorys' => [],
                'features1' => [],
                'features2' => [],
                'entrys' => [],
            ],
            $mode
        );

        $criteria['criteria'] = $this->searchCriteria->convert();

        return $criteria;
    }

    private function setByCategorys(array $criteria): array
    {
        $criteria['categorys'] = $this->jsonSerializeObjects($this->categoryRepository->findBy([], ['ref' => 'ASC']));

        $criteria['subcategorys'] = $this->jsonSerializeObjects($this->subcategoryRepository->findBy([
            'category_id' => $this->searchCriteria->getCategoryId()
        ]));

        if ($this->searchCriteria->getSelectRef() === 'categorys') {
            $this->searchCriteria
                ->setSubcategoryId(0)
                ->setFeature1Id(0)
                ->setFeature2Id(0)
                ->setEntrysNumber(0);
        }

        return $criteria;
    }

    private function setBySubcategorys(array $criteria): array
    {
        $criteria = $this->setByCategorys($criteria);

        $criteria['features1'] = $this->jsonSerializeObjects($this->featureRepository->findBy([
            'subcategory_id' => $this->searchCriteria->getSubcategoryId()
        ]));

        if ($this->searchCriteria->getSelectRef() === 'subcategorys') {
            $this->searchCriteria
                ->setFeature1Id(0)
                ->setFeature2Id(0)
                ->setEntrysNumber(0);
        }

        return $criteria;
    }

    private function setByFeatures1(array $criteria): array
    {
        $criteria = $this->setBySubCategorys($criteria);

        $criteria['features2'] = $this->jsonSerializeObjects($this->featureRepository->findBy([
            'subcategory_id' => $this->searchCriteria->getSubcategoryId()
        ]));

        if ($this->searchCriteria->getSelectRef() === 'features1') {
            $this->searchCriteria
                ->setFeature2Id(0)
                ->setEntrysNumber(0);
        }

        return $criteria;
    }

    private function setByFeatures2(array $criteria): array
    {
        $criteria = $this->setByFeatures1($criteria);

        $criteria['entrys'] = [1 => 1, 2 => 2, 5 => 5, 10 => 10, 20 => 20, 30 => 30];

        if ($this->searchCriteria->getSelectRef() === 'features2') {
            $this->searchCriteria
                ->setEntrysNumber(0);
        }

        return $criteria;
    }

    private function setByEntrys(array $criteria, string $mode): array
    {
        $feature1Id = $this->searchCriteria->getFeature1Id();
        $feature2Id = $this->searchCriteria->getFeature2Id();

        $criteria = $this->setByFeatures2($criteria);

        if ($mode === 'add') {
            $criteria['feature1_options'] = $this->jsonSerializeObjects(
                $this->optionRepository->findBy(['feature_id' => $feature1Id])
            );
            $criteria['feature2_options'] = $this->jsonSerializeObjects(
                $this->optionRepository->findBy(['feature_id' => $feature2Id])
            );
        }

        if ($mode === 'edit') {
            $criteria['quantitys'] = $this->jsonSerializeObjects(
                $this->quantityRepository->findBy(['feature1_id' => $feature1Id, 'feature2_id' => $feature2Id])
            );
        }

        return $criteria;
    }
}
