<?php

declare(strict_types=1);

namespace App\Article\AJAX\Service;

use App\Article\DTO\OptionsSearchCriteriaDTO;
use App\Article\Repository\Interfaces\CategoryRepositoryInterface;
use App\Article\Repository\Interfaces\FeatureRepositoryInterface;
use App\Article\Repository\Interfaces\OptionRepositoryInterface;
use App\Article\Repository\Interfaces\SubcategoryRepositoryInterface;
use InvalidArgumentException;
use Paneric\CSRTriad\Service;
use Paneric\Interfaces\Session\SessionInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class OptionAJAXService extends Service
{
    private $categoryRepository;

    private $subcategoryRepository;

    private $featureRepository;

    private $optionRepository;

    private $searchCriteria;

    public function __construct(
        SessionInterface $session,
        FeatureRepositoryInterface $featureRepository,
        SubcategoryRepositoryInterface $subcategoryRepository,
        CategoryRepositoryInterface $categoryRepository,
        OptionRepositoryInterface $optionRepository
    ) {
        parent::__construct($session);

        $this->categoryRepository = $categoryRepository;

        $this->subcategoryRepository = $subcategoryRepository;

        $this->featureRepository = $featureRepository;

        $this->optionRepository = $optionRepository;
    }

    public function search(Request $request, string $mode): array
    {
        if (!$request->getMethod() === 'POST') {
            throw new InvalidArgumentException(sprintf('Invalid request method - POST required'));
        }

        $validation = $request->getAttribute('validation')[OptionsSearchCriteriaDTO::class];

        if (!empty($validation)) {
            throw new InvalidArgumentException('Invalid criteria');
        }

        $attributes = $request->getParsedBody();

        $this->searchCriteria = new OptionsSearchCriteriaDTO();
        $this->searchCriteria->hydrate($attributes);

        $selectName = ucfirst($this->searchCriteria->getSelectRef());

        if ($selectName !== 'Features') {

            $criteria = $this->{'setBy' . $selectName}(
                [
                    'categorys' => [],
                    'subcategorys' => [],
                    'features' => [],
                ]
            );

            $criteria['criteria'] = $this->searchCriteria->convert();

            return $criteria;
        }

        $criteria = $this->setByFeatures(
            [
                'categorys' => [],
                'subcategorys' => [],
                'features' => [],
            ],
            $mode
        );

        $criteria['criteria'] = $this->searchCriteria->convert();

        return $criteria;
    }

    private function setByCategorys(array $criteria): array
    {
        $criteria['categorys'] = $this->jsonSerializeObjects($this->categoryRepository->findBy([], ['ref' => 'ASC']));

        $criteria['subcategorys'] = $this->jsonSerializeObjects($this->subcategoryRepository->findBy([
            'category_id' => $this->searchCriteria->getCategoryId()
        ]));

        if ($this->searchCriteria->getSelectRef() === 'categorys') {
            $this->searchCriteria
                ->setSubcategoryId(0)
                ->setFeatureId(0);
        }

        return $criteria;
    }

    private function setBySubcategorys(array $criteria): array
    {
        $criteria = $this->setByCategorys($criteria);

        $criteria['features'] = $this->jsonSerializeObjects($this->featureRepository->findBy([
            'subcategory_id' => $this->searchCriteria->getSubcategoryId()
        ]));

        if ($this->searchCriteria->getSelectRef() === 'categorys') {
            $this->searchCriteria
                ->setFeatureId(0);
        }

        return $criteria;
    }

    private function setByFeatures(array $criteria, string $mode): array
    {
        $featureId = $this->searchCriteria->getFeatureId();

        $criteria = $this->setBySubcategorys($criteria);

        if ($mode === 'edit') {
            $criteria['options'] = $this->jsonSerializeObjects(
                $this->optionRepository->findBy(['feature_id' => $featureId])
            );
        }

        return $criteria;
    }
}