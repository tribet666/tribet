<?php

declare(strict_types=1);

namespace App\Article\AJAX\Controller;

use App\Article\AJAX\Service\OptionAJAXService;
use InvalidArgumentException;
use Paneric\CSRTriad\Controller\AppController;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Twig\Environment as Twig;

class OptionAJAXController extends AppController
{
    protected $optionAJAXService;

    protected $twig;

    public function __construct(Twig $twig, OptionAJAXService $optionAJAXService)
    {
        parent:: __construct($twig);

        $this->optionAJAXService = $optionAJAXService;
    }

    public function search(Request $request, Response $response, string $mode): Response
    {
        if (!in_array('application/x-www-form-urlencoded', $request->getHeader('Content-type'))) {
            throw new InvalidArgumentException('This is not a XmlHttpRequest');
        }

        return $this->render(
            $response,
            '@module/option/ajax/' . $mode . '_multiple_ajax.html.twig',
            $this->optionAJAXService->search($request, $mode)
        );
    }
}
