<?php

declare(strict_types=1);

namespace App\Article\AJAX\Controller;

use App\Article\AJAX\Service\QuantityAJAXService;
use InvalidArgumentException;
use Paneric\CSRTriad\Controller\AppController;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Twig\Environment as Twig;

class QuantityAJAXController extends AppController
{
    protected $quantityAJAXService;

    protected $twig;

    public function __construct(Twig $twig, QuantityAJAXService $quantityAJAXService)
    {
        parent:: __construct($twig);

        $this->quantityAJAXService = $quantityAJAXService;
    }

    public function search(Request $request, Response $response, string $mode): Response
    {
        if (!in_array('application/x-www-form-urlencoded', $request->getHeader('Content-type'))) {
            throw new InvalidArgumentException('This is not a XmlHttpRequest');
        }

        return $this->render(
            $response,
            '@module/quantity/ajax/' . $mode . '_multiple_ajax.html.twig',
            $this->quantityAJAXService->search($request, $mode)
        );
    }
}
