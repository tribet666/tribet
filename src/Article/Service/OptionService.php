<?php

declare(strict_types=1);

namespace App\Article\Service;

use App\Article\DTO\OptionsSearchCriteriaDTO;
use App\Article\Query\OptionQuery;
use App\Article\Repository\Interfaces\CategoryRepositoryInterface;
use App\Article\Repository\Interfaces\OptionRepositoryInterface;
use App\Article\Repository\Interfaces\FeatureRepositoryInterface;
use App\Article\Repository\Interfaces\SubcategoryRepositoryInterface;
use Paneric\CSRTriad\Service;
use App\Article\DTO\OptionDTO;
use Psr\Http\Message\ServerRequestInterface as Request;
use Paneric\Interfaces\Session\SessionInterface;

class OptionService extends Service
{
    private $optionQuery;

    private $optionRepository;

    private $featureRepository;

    private $subcategoryRepository;

    private $categoryRepository;

    public function __construct(
        OptionQuery $optionQuery,
        OptionRepositoryInterface $optionRepository,
        FeatureRepositoryInterface $featureRepository,
        SubcategoryRepositoryInterface $subcategoryRepository,
        CategoryRepositoryInterface $categoryRepository,
        SessionInterface $session
    ) {
        parent::__construct($session);

        $this->optionQuery = $optionQuery;

        $this->optionRepository = $optionRepository;

        $this->featureRepository = $featureRepository;

        $this->subcategoryRepository = $subcategoryRepository;

        $this->categoryRepository = $categoryRepository;
    }


    public function getAllPaginated(): array
    {
        $this->session->setFlash(['page_title' => 'content_options_show_title'], 'value');

        $pagination = $this->session->getData('pagination');
        $options = $this->optionQuery->findAllPaginated(
            $pagination['limit'],
            $pagination['offset']
        );

        return [
            'options' => $this->jsonSerializeObjects($options)
        ];
    }

    public function getAllWhereOrdered(array $where = [], array $order): array
    {
        return $this->jsonSerializeObjects(
            $this->optionRepository->findBy($where, $order)
        );
    }

    public function get(int $optionId): ?object
    {
        return $this->optionQuery->findOneById($optionId);
    }

    public function create(Request $request): ?array
    {
        $attributes = [];
        $validation = [];

        $optionDTO = new OptionDTO();

        if ($request->getMethod() === 'POST') {

            $validation = $request->getAttribute('validation')[OptionDTO::class];

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

                $uniqueAttributes = [
                    'ref' => $attributes['ref']
                ];

                if ($this->optionRepository->findByEnhanced($uniqueAttributes, 'OR') === null) {
                    return $this->optionRepository->create($optionDTO->hydrate($attributes));
                }

                $this->session->setFlash(['db_option_unique_error'], 'error');
            }
        }

        return [
            'option' => $attributes,
            'features' => $this->featureRepository->findAll(),
            'validation' => $validation
        ];
    }

    public function update(int $optionId, Request $request): ?array
    {
        $validation = [];

        $optionDTO = new OptionDTO();

        $method = $request->getMethod();

        if ($method === 'POST') {

            $validation = $request->getAttribute('validation')[OptionDTO::class];

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

                $uniqueAttributes = [
                    'ref' => $attributes['ref'],
                ];

                if ($this->optionRepository->findByEnhanced($uniqueAttributes, 'OR', $optionId) === null) {
                    return $this->optionRepository->update(
                        $optionId,
                        $optionDTO->hydrate($attributes)
                    );
                }

                $this->session->setFlash(['db_option_unique_error'], 'error');
            }
        }

        return [
            'option' => $this->optionRepository->findOneById($optionId)->convert(),
            'features' => $this->featureRepository->findAll(),
            'validation' => $validation
        ];
    }

    public function updateMultiple(Request $request): ?array
    {
        $optionDTO = new OptionDTO();

        $method = $request->getMethod();

        if ($method === 'POST') {

            $validation = array_merge(
                $request->getAttribute('validation')[OptionDTO::class],
                $request->getAttribute('validation')[OptionsSearchCriteriaDTO::class]
            );

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

                $options = $this->jsonSerializeArrays($attributes, new OptionDTO());

                if ((int) $this->optionRepository->updateMultiple($options) > 0) {
                    return null;
                }

                $this->session->setFlash(['db_options_update_multiple_error'], 'error');
            }

            return [
                'categorys' => $this->jsonSerializeObjects($this->categoryRepository->findBy([], ['ref' => 'ASC'])),
                'subcategorys' => $this->jsonSerializeObjects($this->subcategoryRepository->findBy([
                    'category_id' => $attributes['category_id']
                ])),
                'features' => $this->jsonSerializeObjects($this->featureRepository->findBy([
                    'subcategory_id' => $attributes['subcategory_id']
                ])),
                'criteria' => [
                    'category_id' => $attributes['category_id'],
                    'subcategory_id' => $attributes['subcategory_id'],
                    'feature_id' => $attributes['feature_id'],
                ],
                'options' => $this->jsonSerializeObjects(
                    $this->optionRepository->findBy(['feature_id' => $attributes['feature_id']])
                ),
                'validation' => $validation
            ];
        }

        return [
            'categorys' => $this->jsonSerializeObjects($this->categoryRepository->findBy([], ['ref' => 'ASC'])),
            'subcategorys' => [],
            'features' => [],
            'criteria' => [],
            'options' => [],
            'validation' => []
        ];
    }


    public function delete(Request $request, int $optionId): ?array
    {
        if ($request->getMethod() === 'POST') {

            return $this->optionRepository->remove($optionId);
        }

        $this->session->setFlash(['option_delete_warning'], 'warning');

        return [];
    }
}
