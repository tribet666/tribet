<?php

declare(strict_types=1);

namespace App\Article\Service;

use App\Article\Query\SubcategoryQuery;
use App\Article\Repository\Interfaces\SubcategoryRepositoryInterface;
use App\Article\Repository\Interfaces\CategoryRepositoryInterface;
use App\Article\Repository\Interfaces\FeatureRepositoryInterface;
use Paneric\CSRTriad\Service;
use App\Article\DTO\SubcategoryDTO;
use Paneric\Interfaces\Hydrator\HydratorInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Paneric\Interfaces\Session\SessionInterface;

class SubcategoryService extends Service
{
    private $subcategoryQuery;

    private $subcategoryRepository;

    private $categoryRepository;

    private $featureRepository;

    public function __construct(
        SubcategoryQuery $subcategoryQuery,
        SubcategoryRepositoryInterface $subcategoryRepository,
        CategoryRepositoryInterface $categoryRepository,
        FeatureRepositoryInterface $featureRepository,
        SessionInterface $session
    ) {
        parent::__construct($session);

        $this->subcategoryQuery = $subcategoryQuery;

        $this->subcategoryRepository = $subcategoryRepository;

        $this->categoryRepository = $categoryRepository;

        $this->featureRepository = $featureRepository;
    }

    public function getRouteAttributesByRef(string $subcategoryRef): ?HydratorInterface
    {
        return $this->subcategoryRepository->findRouteAttributesByRef($subcategoryRef);
    }

    public function getAllPaginated(): array
    {
        $this->session->setFlash(['page_title' => 'content_subcategorys_show_title'], 'value');

        $pagination = $this->session->getData('pagination');
        $subcategorys = $this->subcategoryQuery->findAllPaginated(
            $pagination['limit'],
            $pagination['offset']
        );

        return [
            'subcategorys' => $this->jsonSerializeObjects($subcategorys)
        ];
    }

    public function getAllWhereOrdered(array $where = [], array $order): array
    {
        return $this->jsonSerializeObjects(
            $this->subcategoryRepository->findBy($where, $order)
        );
    }

    public function get(int $subcategoryId): ?object
    {
        return $this->subcategoryQuery->findOneById($subcategoryId);
    }

    public function create(Request $request): ?array
    {
        $attributes = [];
        $validation = [];

        $subcategoryDTO = new SubcategoryDTO();

        if ($request->getMethod() === 'POST') {

            $validation = $request->getAttribute('validation')[SubcategoryDTO::class];

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

                $uniqueAttributes = [
                    'ref' => $attributes['ref'],
                    'number' => $attributes['number'],
                ];

                if ($this->subcategoryRepository->findByEnhanced($uniqueAttributes, 'AND') === null) {
                    return $this->subcategoryRepository->create($subcategoryDTO->hydrate($attributes));
                }

                $this->session->setFlash(['db_subcategory_unique_error'], 'error');
            }
        }

        return [
            'subcategory' => $attributes,
            'categorys' => $this->categoryRepository->findAll(),
            'validation' => $validation
        ];
    }

    public function update(int $subcategoryId, Request $request): ?array
    {
        $validation = [];

        $subcategoryDTO = new SubcategoryDTO();

        $method = $request->getMethod();

        if ($method === 'POST') {

            $validation = $request->getAttribute('validation')[SubcategoryDTO::class];

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

                $uniqueAttributes = [
                    'ref' => $attributes['ref'],
                ];

                if ($this->subcategoryRepository->findByEnhanced($uniqueAttributes, 'OR', $subcategoryId) === null) {
                    return $this->subcategoryRepository->update(
                        $subcategoryId,
                        $subcategoryDTO->hydrate($attributes)
                    );
                }

                $this->session->setFlash(['db_subcategory_unique_error'], 'error');
            }
        }

        return [
            'subcategory' => $this->subcategoryRepository->findOneById($subcategoryId)->convert(),
            'categorys' => $this->categoryRepository->findAll(),
            'validation' => $validation
        ];
    }

    public function delete(Request $request, int $subcategoryId): ?array
    {
        if ($request->getMethod() === 'POST') {
            return $this->subcategoryRepository->remove($subcategoryId);
        }

        $this->session->setFlash(['subcategory_delete_warning'], 'warning');

        return [];
    }


    public function updateFeatures(int $subcategoryId, Request $request) // OK
    {
        $validation = [];

        if ($request->getMethod() === 'POST') {

            $validation = $request->getAttribute('validation')[FeatureDTO::class];

            $arguments = $request->getParsedBody()['id'];

            if ($arguments === null) {
                return 'message';
            }

            if (empty($validation)) {
                return $this->subcategoryQuery->updateFeatures(
                    $subcategoryId,
                    $arguments
                );
            }
        }

        return [
            'subcategory' => $this->subcategoryRepository->findOneById($subcategoryId),
            'features' => $this->featureRepository->findAll(),
            'subcategory_features' => $this->jsonSerializeObjectsById(
                $this->subcategoryQuery->findFeatures($subcategoryId)
            ),
            'validation' => $validation
        ];
    }

    public function deleteFeatures(Request $request, int $roleId): ?array
    {
        if ($request->getMethod() === 'POST') {

            return $this->subcategoryQuery->removeFeatures($roleId);
        }

        $this->session->setFlash(['subcategory_features_delete_warning'], 'warning');

        return [];
    }
}
