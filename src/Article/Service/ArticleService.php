<?php

declare(strict_types=1);

namespace App\Article\Service;

use App\Article\DTO\ArticleDTO;
use App\Article\Query\OptionQuery;
use Paneric\CSRTriad\Service;
use Psr\Http\Message\ServerRequestInterface as Request;
use Paneric\Interfaces\Session\SessionInterface;

class ArticleService extends Service
{
    protected $optionQuery;

    public function __construct(
        OptionQuery $optionQuery,
        SessionInterface $session
    ) {
        parent::__construct($session);

        $this->optionQuery = $optionQuery;
    }


//    public function getAll(): array
//    {
//        $this->session->setFlash(['page_title' => 'content_articles_show_title'], 'value');
//
//        $pagination = $this->session->getData('pagination');
//        $articles = $this->articleRepository->findBy(
//            [],
//            ['ref' => 'ASC'],
//            $pagination['limit'],
//            $pagination['offset']
//        );
//
//        return [
//            'articles' => $this->jsonSerializeObjects($articles)
//        ];
//    }
//
//    public function get(int $articleId): ?object
//    {
//        return $this->articleRepository->findOneById($articleId);
//    }

    public function configure(Request $request, int $subcategoryId): ?array
    {
        $attributes = [];
        $validation = [];

        $articleADTO = new ArticleDTO();

        if ($request->getMethod() === 'POST') {

            $validation = $request->getAttribute('validation')[ArticleDTO::class];

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

                $cart = $this->session->getData('cart');
                $cart = $cart ?? [];
                $cart[] = $attributes;

                $this->session->setFlash(['session_article_add_cart'], 'info');

                return null;
            }
        }

        $options = $this->optionQuery->findAllBySubcategoryId($subcategoryId);

        return [
            'options' => $this->jsonSerializeObjects($options),
            'article' => $articleADTO->hydrate($attributes),
            'validation' => $validation
        ];
    }

//    public function update(int $articleId, Request $request): ?array
//    {
//        $validation = [];
//
//        $articleDTO = new ArticleDTO();
//
//        $method = $request->getMethod();
//
//        if ($method === 'POST') {
//
//            $validation = $request->getAttribute('validation')[ArticleDTO::class];
//
//            $attributes = $request->getParsedBody();
//
//            if (empty($validation)) {
//
//                $uniqueAttributes = [
//                    'ref' => $attributes['ref'],
//                ];
//
//                if ($this->articleRepository->findByEnhanced($uniqueAttributes, 'OR', $articleId) === null) {
//                    return $this->articleRepository->update(
//                        $articleId,
//                        $articleDTO->hydrate($attributes)
//                    );
//                }
//
//                $this->session->setFlash(['db_article_unique_error'], 'error');
//            }
//        }
//
//        return [
//            'article' => $this->articleRepository->findOneById($articleId),
//            'validation' => $validation
//        ];
//    }
//
//    public function delete(Request $request, int $articleId): ?array
//    {
//        if ($request->getMethod() === 'POST') {
//
//            return $this->articleRepository->remove($articleId);
//        }
//
//        $this->session->setFlash(['article_delete_warning'], 'warning');
//
//        return [];
//    }
}
