<?php

declare(strict_types=1);

namespace App\Article\Service;

use App\Article\DTO\FeatureDTO;
use Psr\Http\Message\ServerRequestInterface as Request;

trait FeatureServiceTrait
{
    public function getAllPaginated(): array
    {
        $this->session->setFlash(['page_title' => 'content_' . $this->feature . 's_show_title'], 'value');

        $pagination = $this->session->getData('pagination');
        $features = $this->repository->findBy(
            [],
            ['number' => 'ASC'],
            $pagination['limit'],
            $pagination['offset']
        );

        return [
            'label' => $this->feature,
            'features' => $this->jsonSerializeObjects($features)
        ];
    }

    public function getAllWhereOrdered(array $where = [], array $order): array
    {
        return $this->jsonSerializeObjects(
            $this->repository->findBy($where, $order)
        );
    }


    public function get(int $featureId): ?object
    {
        return $this->repository->findOneById($featureId);
    }

    public function create(Request $request): ?array
    {
        $attributes = [];
        $validation = [];

        $featureDTO = new FeatureDTO();

        if ($request->getMethod() === 'POST') {

            $validation = $request->getAttribute('validation')[FeatureDTO::class];

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

                $uniqueAttributes = [
                    'ref' => $attributes['ref'],
                ];

                if ($this->repository->findByEnhanced($uniqueAttributes, 'OR') === null) {
                    return $this->repository->create($featureDTO->hydrate($attributes));
                }

                $this->session->setFlash(['db_' . $this->feature . '_unique_error'], 'error');
            }
        }

        return [
            'label' => $this->feature,
            'feature' => $featureDTO->hydrate($attributes),
            'validation' => $validation
        ];
    }

    public function update(int $featureId, Request $request): ?array
    {
        $validation = [];

        $featureDTO = new FeatureDTO();

        $method = $request->getMethod();

        if ($method === 'POST') {

            $validation = $request->getAttribute('validation')[FeatureDTO::class];

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

                $uniqueAttributes = [
                    'ref' => $attributes['ref'],
                ];

                if ($this->repository->findByEnhanced($uniqueAttributes, 'OR', $featureId) === null) {
                    return $this->repository->update(
                        $featureId,
                        $featureDTO->hydrate($attributes)
                    );
                }

                $this->session->setFlash(['db_' . $this->feature . '_unique_error'], 'error');
            }
        }

        return [
            'label' => $this->feature,
            'feature' => $this->repository->findOneById($featureId),
            'validation' => $validation
        ];
    }

    public function delete(Request $request, int $featureId): ?array
    {
        if ($request->getMethod() === 'POST') {

            return $this->repository->remove($featureId);
        }

        $this->session->setFlash([$this->feature . '_delete_warning'], 'warning');

        return [];
    }
}
