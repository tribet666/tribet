<?php

declare(strict_types=1);

namespace App\Article\Service;

use App\Article\DTO\OptionsSearchCriteriaDTO;
use App\Article\Query\QuantityQuery;
use App\Article\Repository\Interfaces\CategoryRepositoryInterface;
use App\Article\Repository\Interfaces\OptionRepositoryInterface;
use App\Article\Repository\Interfaces\QuantityRepositoryInterface;
use App\Article\Repository\Interfaces\FeatureRepositoryInterface;
use App\Article\Repository\Interfaces\SubcategoryRepositoryInterface;
use Paneric\CSRTriad\Service;
use App\Article\DTO\QuantityDTO;
use Psr\Http\Message\ServerRequestInterface as Request;
use Paneric\Interfaces\Session\SessionInterface;

class QuantityService extends Service
{
    private $quantityQuery;

    private $quantityRepository;

    private $featureRepository;

    private $optionRepository;

    private $subcategoryRepository;

    private $categoryRepository;

    public function __construct(
        QuantityQuery $quantityQuery,
        QuantityRepositoryInterface $quantityRepository,
        FeatureRepositoryInterface $featureRepository,
        OptionRepositoryInterface $optionRepository,
        SubcategoryRepositoryInterface $subcategoryRepository,
        CategoryRepositoryInterface $categoryRepository,
        SessionInterface $session
    ) {
        parent::__construct($session);

        $this->quantityQuery = $quantityQuery;

        $this->quantityRepository = $quantityRepository;

        $this->featureRepository = $featureRepository;

        $this->optionRepository = $optionRepository;

        $this->subcategoryRepository = $subcategoryRepository;

        $this->categoryRepository = $categoryRepository;
    }


    public function getAllPaginated(): array
    {
        $this->session->setFlash(['page_title' => 'content_quantitys_show_title'], 'value');

        $pagination = $this->session->getData('pagination');
        $quantitys = $this->quantityQuery->findAllPaginated(
            $pagination['limit'],
            $pagination['offset']
        );

        return [
            'quantitys' => $this->jsonSerializeObjects($quantitys)
        ];
    }

    public function getAllWhereOrdered(array $where = [], array $order): array
    {
        return $this->jsonSerializeObjects(
            $this->quantityRepository->findBy($where, $order)
        );
    }

    public function get(int $quantityId): ?object
    {
        return $this->quantityQuery->findOneById($quantityId);
    }

    public function create(Request $request): ?array
    {
        $attributes = [];
        $validation = [];

        $quantityDTO = new QuantityDTO();

        if ($request->getMethod() === 'POST') {

            $validation = $request->getAttribute('validation')[QuantityDTO::class];

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

                $uniqueAttributes = [
                    'ref' => $attributes['ref']
                ];

                if ($this->quantityRepository->findByEnhanced($uniqueAttributes, 'OR') === null) {
                    return $this->quantityRepository->create($quantityDTO->hydrate($attributes));
                }

                $this->session->setFlash(['db_quantity_unique_error'], 'error');
            }
        }

        return [
            'quantity' => $attributes,
            'features' => $this->featureRepository->findAll(),
            'validation' => $validation
        ];
    }

    public function createMultiple(Request $request): ?array
    {
        if ($request->getMethod() === 'POST') {

            $validation = array_merge(
                $request->getAttribute('validation')[QuantityDTO::class],
                $request->getAttribute('validation')[OptionsSearchCriteriaDTO::class]
            );

            $attributes = $request->getParsedBody();

            $quantitys = $this->jsonSerializeArrays($attributes, new QuantityDTO());

            if (empty($validation)) {

                if ((int) $this->quantityRepository->createMultiple($quantitys) > 0) {
                    return null;
                }

                $this->session->setFlash(['db_quantitys_create_multiple_error'], 'error');
            }

            return [
                'categorys' => $this->jsonSerializeObjects($this->categoryRepository->findBy([], ['ref' => 'ASC'])),
                'subcategorys' => $this->jsonSerializeObjects($this->subcategoryRepository->findBy([
                    'category_id' => $attributes['category_id']
                ])),
                'features1' => $this->jsonSerializeObjects($this->featureRepository->findBy([
                    'subcategory_id' => $attributes['subcategory_id']
                ])),
                'features2' => $this->jsonSerializeObjects($this->featureRepository->findBy([
                    'subcategory_id' => $attributes['subcategory_id']
                ])),
                'feature1_options' => $this->jsonSerializeObjects($this->optionRepository->findBy([
                    'feature_id' => $attributes['feature1_id']
                ])),
                'feature2_options' => $this->jsonSerializeObjects($this->optionRepository->findBy([
                    'feature_id' => $attributes['feature2_id']
                ])),
                'entrys' => [1 => 1, 2 => 2, 5 => 5, 10 => 10, 20 => 20, 30 => 30],
                'criteria' => [
                    'category_id' => $attributes['category_id'],
                    'subcategory_id' => $attributes['subcategory_id'],
                    'feature1_id' => $attributes['feature1_id'],
                    'feature2_id' => $attributes['feature2_id'],
                    'entrys_number' => $attributes['entrys_number'],
                ],
                'quantitys' => $quantitys,

                'validation' => $validation,
            ];
        }

        return [
            'categorys' => $this->jsonSerializeObjects($this->categoryRepository->findBy([], ['ref' => 'ASC'])),
            'subcategorys' => [],
            'features1' => [],
            'features2' => [],
            'feature1_options' => [],
            'feature2_options' => [],
            'entrys' => [],
            'criteria' => [],
            'quantitys' => [],
            'validation' => [],
        ];
    }

    public function update(int $quantityId, Request $request): ?array
    {
        $validation = [];

        $quantityDTO = new QuantityDTO();

        $method = $request->getMethod();

        if ($method === 'POST') {

            $validation = $request->getAttribute('validation')[QuantityDTO::class];

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

                $uniqueAttributes = [
                    'ref' => $attributes['ref'],
                ];

                if ($this->quantityRepository->findByEnhanced($uniqueAttributes, 'OR', $quantityId) === null) {
                    return $this->quantityRepository->update(
                        $quantityId,
                        $quantityDTO->hydrate($attributes)
                    );
                }

                $this->session->setFlash(['db_quantity_unique_error'], 'error');
            }
        }

        return [
            'quantity' => $this->quantityRepository->findOneById($quantityId)->convert(),
            'features' => $this->featureRepository->findAll(),
            'validation' => $validation
        ];
    }

    public function updateMultiple(Request $request): ?array
    {
        $quantityDTO = new QuantityDTO();

        $method = $request->getMethod();

        if ($method === 'POST') {

            $validation = array_merge(
                $request->getAttribute('validation')[QuantityDTO::class],
                $request->getAttribute('validation')[QuantitysSearchCriteriaDTO::class]
            );

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

                $quantitys = $this->jsonSerializeArrays($attributes, new QuantityDTO());

                if ((int) $this->quantityRepository->updateMultiple($quantitys) > 0) {
                    return null;
                }

                $this->session->setFlash(['db_quantitys_update_multiple_error'], 'error');
            }

            return [
                'categorys' => $this->jsonSerializeObjects($this->categoryRepository->findBy([], ['ref' => 'ASC'])),
                'subcategorys' => $this->jsonSerializeObjects($this->subcategoryRepository->findBy([
                    'category_id' => $attributes['category_id']
                ])),
                'features' => $this->jsonSerializeObjects($this->featureRepository->findBy([
                    'subcategory_id' => $attributes['subcategory_id']
                ])),
                'criteria' => [
                    'category_id' => $attributes['category_id'],
                    'subcategory_id' => $attributes['subcategory_id'],
                    'feature_id' => $attributes['feature_id'],
                ],
                'quantitys' => $this->jsonSerializeObjects(
                    $this->quantityRepository->findBy(['feature_id' => $attributes['feature_id']])
                ),
                'validation' => $validation
            ];
        }

        return [
            'categorys' => $this->jsonSerializeObjects($this->categoryRepository->findBy([], ['ref' => 'ASC'])),
            'subcategorys' => [],
            'features' => [],
            'criteria' => [],
            'quantitys' => [],
            'validation' => []
        ];
    }


    public function delete(Request $request, int $quantityId): ?array
    {
        if ($request->getMethod() === 'POST') {

            return $this->quantityRepository->remove($quantityId);
        }

        $this->session->setFlash(['quantity_delete_warning'], 'warning');

        return [];
    }
}
