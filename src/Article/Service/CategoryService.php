<?php

declare(strict_types=1);

namespace App\Article\Service;

use App\Article\DTO\CategoryDTO;
use App\Article\Query\SubcategoryQuery;
use App\Article\Repository\Interfaces\CategoryRepositoryInterface;
use Paneric\CSRTriad\Service;
use Paneric\Interfaces\Hydrator\HydratorInterface;
use Paneric\Interfaces\Session\SessionInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class CategoryService extends Service
{
    private $categoryRepository;

    private $subcategoryQuery;

    public function __construct(
        CategoryRepositoryInterface $categoryRepository,
        SubcategoryQuery $subcategoryQuery,
        SessionInterface $session
    ) {
        parent::__construct($session);

        $this->categoryRepository = $categoryRepository;

        $this->subcategoryQuery = $subcategoryQuery;
    }

    public function getRouteAttributesByRef(string $categoryRef): ?HydratorInterface
    {
        return $this->categoryRepository->findRouteAttributesByRef($categoryRef);
    }

    public function get(int $categoryId): ?object
    {
        return $this->categoryRepository->findOneById($categoryId);
    }

    public function getAllPaginated(): array
    {
        $this->session->setFlash(['page_title' => 'content_categorys_show_title'], 'value');

        $pagination = $this->session->getData('pagination');
        $categorys = $this->categoryRepository->findBy(
            [],
            ['number' => 'ASC'],
            $pagination['limit'],
            $pagination['offset']
        );

        return [
            'categorys' => $this->jsonSerializeObjects($categorys)
        ];
    }

    public function getAllWhereOrdered(array $where = [], array $order): array
    {
        return $this->jsonSerializeObjects(
            $this->categoryRepository->findBy($where, $order)
        );
    }

    public function getAllAsTiles(): array
    {
        $categorys = $this->categoryRepository->findAllAsTiles();

        return [
            'categorys' => $this->jsonSerializeObjects($categorys)
        ];
    }

    public function getRefferedAsTiles(int $categoryId): ?array
    {
        $subcategorys = $this->subcategoryQuery->findByCategoryIdAsTiles($categoryId);

        return [
            'subcategorys' => $this->jsonSerializeObjects($subcategorys)
        ];
    }

    public function create(Request $request): ?array
    {
        $attributes = [];
        $validation = [];

        $categoryDTO = new CategoryDTO();

        if ($request->getMethod() === 'POST') {

            $validation = $request->getAttribute('validation')[CategoryDTO::class];

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

                $uniqueAttributes = [
                    'ref' => $attributes['ref'],
                    'number' => $attributes['number'],
                ];

                if ($this->categoryRepository->findByEnhanced($uniqueAttributes, 'OR') === null) {
                    return $this->categoryRepository->create($categoryDTO->hydrate($attributes));
                }

                $this->session->setFlash(['db_category_unique_error'], 'error');
            }
        }

        return [
            'category' => $attributes,
            'validation' => $validation
        ];
    }

    public function update(int $categoryId, Request $request): ?array
    {
        $validation = [];

        $categoryDTO = new CategoryDTO();

        $method = $request->getMethod();

        if ($method === 'POST') {

            $validation = $request->getAttribute('validation')[CategoryDTO::class];

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

                $uniqueAttributes = [
                    'ref' => $attributes['ref'],
                    'number' => $attributes['number'],
                ];

                if ($this->categoryRepository->findByEnhanced($uniqueAttributes, 'OR', $categoryId) === null) {
                    return $this->categoryRepository->update(
                        $categoryId,
                        $categoryDTO->hydrate($attributes)
                    );
                }

                $this->session->setFlash(['db_category_unique_error'], 'error');
            }
        }

        return [
            'category' => $this->categoryRepository->findOneById($categoryId)->convert(),
            'validation' => $validation
        ];
    }

    public function delete(Request $request, int $categoryId): ?array
    {
        if ($request->getMethod() === 'POST') {

            return $this->categoryRepository->remove($categoryId);
        }

        $this->session->setFlash(['category_delete_warning'], 'warning');

        return [];
    }
}
