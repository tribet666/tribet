<?php

declare(strict_types=1);

namespace App\Article\Service;

use App\Article\Query\FeatureQuery;
use App\Article\Repository\Interfaces\FeatureRepositoryInterface;
use Paneric\CSRTriad\Service;
use Paneric\Interfaces\Session\SessionInterface;

class FeatureService extends Service
{
    use FeatureServiceTrait;

    private $repository;

    private $query;

    private $feature = 'feature';

    public function __construct(
        FeatureRepositoryInterface $repository,
        FeatureQuery $query,
        SessionInterface $session
    ) {
        parent::__construct($session);

        $this->repository = $repository;

        $this->query = $query;
    }

    public function getBySubcategoryRef(string $subcategoryRef): array
    {
        $features = $this->query->findBySubcategoryRef($subcategoryRef);

        return $this->jsonSerializeObjects($features);
    }
}
