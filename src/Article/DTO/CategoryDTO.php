<?php

declare(strict_types=1);

namespace App\Article\DTO;

use DateTimeImmutable;
use JsonSerializable;
use Paneric\Interfaces\Hydrator\HydratorInterface;

class CategoryDTO implements HydratorInterface, JsonSerializable
{
    private $id;



    private $ref;

    private $number;

    private $labelPl;

    private $labelEn;

    private $infoPl;

    private $infoEn;

    private $descriptionPl;

    private $descriptionEn;



    private $createdAt;

    private $updatedAt;



    public function getId(): ?int
    {
        return $this->id;
    }



    public function getRef(): ?string
    {
        return $this->ref;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function getLabelPl(): ?string
    {
        return $this->labelPl;
    }

    public function getLabelEn(): ?string
    {
        return $this->labelEn;
    }

    public function getInfoPl(): ?string
    {
        return $this->infoPl;
    }

    public function getInfoEn(): ?string
    {
        return $this->infoEn;
    }

    public function getDescriptionPl(): ?string
    {
        return $this->descriptionPl;
    }

    public function getDescriptionEn(): ?string
    {
        return $this->descriptionEn;
    }



    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }



    public function __set($name, $value)
    {
        $str ='';

        if (strpos($name, '_') !== false) {
            $str = str_replace(' ', '', ucwords(str_replace('_', ' ', $name)));
            $str[0] = lcfirst($str[0]);
        }

        if (
            $name === 'id' ||
            $name === 'number'
        ) {
            $this->$str = (int) $value;
        }

        if (
            $name === 'label_pl' ||
            $name === 'label_en' ||
            $name === 'info_pl' ||
            $name === 'info_en' ||
            $name === 'description_pl' ||
            $name === 'description_en'
        ) {
            $this->$str = $value;
        }

        if ($value !== null && ($name === 'created_at' || $name === 'updated_at')) {
            $this->$str = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $value);
        }
    }


    public function hydrate(array $attributes): self
    {
        if (isset($attributes['id'])) {
            $this->id = (int) $attributes['id'];
        }


        if (isset($attributes['ref'])) {
            $this->ref = $attributes['ref'];
        }

        if (isset($attributes['number'])) {
            $this->number = (int) $attributes['number'];
        }

        if (isset($attributes['label_pl'])) {
            $this->labelPl = $attributes['label_pl'];
        }

        if (isset($attributes['label_en'])) {
            $this->labelEn = $attributes['label_en'];
        }

        if (isset($attributes['info_pl'])) {
            $this->infoPl = $attributes['info_pl'];
        }

        if (isset($attributes['info_en'])) {
            $this->infoEn = $attributes['info_en'];
        }

        if (isset($attributes['description_pl'])) {
            $this->descriptionPl = $attributes['description_pl'];
        }

        if (isset($attributes['description_en'])) {
            $this->descriptionEn = $attributes['description_en'];
        }


        return $this;
    }

    public function convert(): array
    {
        $attributes = [];


        if ($this->id !== null) {
            $attributes['id'] = $this->id;
        }


        if ($this->ref !== null) {
            $attributes['ref'] = $this->ref;
        }

        if ($this->number !== null) {
            $attributes['number'] = $this->number;
        }

        if ($this->labelPl !== null) {
            $attributes['label_pl'] = $this->labelPl;
        }

        if ($this->labelEn !== null) {
            $attributes['label_en'] = $this->labelEn;
        }

        if ($this->infoPl !== null) {
            $attributes['info_pl'] = $this->infoPl;
        }

        if ($this->infoEn !== null) {
            $attributes['info_en'] = $this->infoEn;
        }

        if ($this->descriptionPl !== null) {
            $attributes['description_pl'] = $this->descriptionPl;
        }

        if ($this->descriptionEn !== null) {
            $attributes['description_en'] = $this->descriptionEn;
        }


        return $attributes;
    }

    public function jsonSerialize(): array
    {
        return get_object_vars($this);
    }
}
