<?php

declare(strict_types=1);

namespace App\Article\DTO;

use DateTimeImmutable;
use JsonException;
use JsonSerializable;
use Paneric\Interfaces\Hydrator\HydratorInterface;

class QuantityDTO implements HydratorInterface, JsonSerializable
{
    private $id;

    private $feature1OptionId;

    private $feature2OptionId;

    private $quantityPriceFactor;

    private $createdAt;

    private $updatedAt;


    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getFeature1OptionId(): ?int
    {
        return $this->feature1OptionId;
    }

    public function getFeature2OptionId(): ?int
    {
        return $this->feature2OptionId;
    }

    public function getQuantityPriceFactor(): ?string
    {
        return $this->quantityPriceFactor;
    }

    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function __set($name, $value)
    {
        $str ='';

        if (strpos($name, '_') !== false) {
            $str = str_replace(' ', '', ucwords(str_replace('_', ' ', $name)));
            $str[0] = lcfirst($str[0]);
        }

        if ($name === 'id' || $name === 'feature1_option_id' || $name === 'feature2_option_id') {
            $this->$str = (int) $value;
        }

        if ($name === 'quantity_price_factor') {
            $this->$str = $value;
        }

        if ($value !== null && ($name === 'created_at' || $name === 'updated_at')) {
            $this->$str = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $value);
        }
    }


    public function hydrate(array $attributes): self
    {
        if (isset($attributes['id'])) {
            $this->id = $attributes['id'];
        }

        if (isset($attributes['feature1_option_id'])) {
            $this->feature1OptionId = $attributes['feature1_option_id'];
        }

        if (isset($attributes['feature2_option_id'])) {
            $this->feature2OptionId = $attributes['feature2_option_id'];
        }

        if (isset($attributes['quantity_price_factor'])) {
            $this->quantityPriceFactor = $attributes['quantity_price_factor'];
        }

        return $this;
    }

    public function convert(): array
    {
        $attributes = [];

        if ($this->id !== null) {
            $attributes['id'] = $this->id;
        }

        if ($this->feature1OptionId !== null) {
            $attributes['feature1_option_id'] = $this->feature1OptionId;
        }

        if ($this->feature2OptionId !== null) {
            $attributes['feature2_option_id'] = $this->feature2OptionId;
        }

        if ($this->quantityPriceFactor !== null) {
            $attributes['quantity_price_factor'] = $this->quantityPriceFactor;
        }

        return $attributes;
    }

    public function jsonSerialize(): array
    {
        $attributes = [];

        if ($this->id !== null) {
            $attributes['id'] = $this->id;
        }

        if ($this->feature1OptionId !== null) {
            $attributes['feature1_option_id'] = $this->feature1OptionId;
        }

        if ($this->feature2OptionId !== null) {
            $attributes['feature2_option_id'] = $this->feature2OptionId;
        }

        if ($this->quantityPriceFactor !== null) {
            try {
                $attributes['quantity_price_factor'] = json_encode(
                    $this->quantityPriceFactor,
                    JSON_THROW_ON_ERROR,
                    512
                );
            } catch (JsonException $e) {
                echo $e->getMessage();
            }
        }

        return $attributes;
    }
}
