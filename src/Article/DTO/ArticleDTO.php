<?php

declare(strict_types=1);

namespace App\Article\DTO;

use DateTimeImmutable;
use JsonException;
use JsonSerializable;
use Paneric\Interfaces\Hydrator\HydratorInterface;

class ArticleDTO implements HydratorInterface, JsonSerializable
{
    private $id;
    
    private $features;
    

    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getFeatures(): ?array
    {
        return $this->features;
    }


    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function __set($name, $value)
    {
        $str ='';

        if (strpos($name, '_') !== false) {
            $str = str_replace(' ', '', ucwords(str_replace('_', ' ', $name)));
            $str[0] = lcfirst($str[0]);
        }

        if ($name === 'id') {
            $this->$str = (int) $value;
        }

        if ($name === 'features') {
            try {
                $this->$str = json_decode($value, true, 512, JSON_THROW_ON_ERROR);
            } catch (JsonException $e) {
                $e->getMessage();
            }
        }

        if ($value !== null && ($name === 'created_at' || $name === 'updated_at')) {
            $this->$str = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $value);
        }
    }


    public function hydrate(array $attributes): self
    {
        if (isset($attributes['id'])) {
            $this->id = $attributes['id'];
        }
        
        if (isset($attributes['features'])) {
            $this->features = $attributes['features'];
        }

        return $this;
    }

    public function convert(): array
    {
        $attributes = [];

        if ($this->id !== null) {
            $attributes['id'] = $this->id;
        }

        if ($this->features !== null) {
            $attributes['features'] = $this->features;
        }

        return $attributes;
    }

    public function jsonSerialize(): array
    {
        return get_object_vars($this);
    }
}
