<?php

declare(strict_types=1);

namespace App\Article\DTO;

use JsonSerializable;
use Paneric\Interfaces\Hydrator\HydratorInterface;

class OptionsSearchCriteriaDTO implements HydratorInterface, JsonSerializable
{
    private $selectRef;

    private $categoryId;

    private $subcategoryId;

    private $featureId;

    private $feature1Id;

    private $feature2Id;

    private $entrysNumber;


    public function getSelectRef(): ?string
    {
        return $this->selectRef;
    }

    public function getCategoryId(): ?int
    {
        return $this->categoryId;
    }

    public function getSubcategoryId(): ?int
    {
        return $this->subcategoryId;
    }

    public function getFeatureId(): ?int
    {
        return $this->featureId;
    }

    public function getFeature1Id(): ?int
    {
        return $this->feature1Id;
    }

    public function getFeature2Id(): ?int
    {
        return $this->feature2Id;
    }

    public function getEntrysNumber(): ?int
    {
        return $this->entrysNumber;
    }



    public function setSelectRef(string $selectRef): self
    {
        $this->selectRef = $selectRef;

        return $this;
    }

    public function setCategoryId(int $categoryId): self
    {
        $this->categoryId = $categoryId;

        return $this;
    }

    public function setSubcategoryId(int $subcategoryId): self
    {
        $this->subcategoryId = $subcategoryId;

        return $this;
    }

    public function setFeatureId(int $featureId): self
    {
        $this->featureId = $featureId;

        return $this;
    }

    public function setFeature1Id(int $feature1Id): self
    {
        $this->feature1Id = $feature1Id;

        return $this;
    }

    public function setFeature2Id(int $feature2Id): self
    {
        $this->feature2Id = $feature2Id;

        return $this;
    }

    public function setEntrysNumber(int $entrysNumber): self
    {
        $this->entrysNumber = $entrysNumber;

        return $this;
    }



    public function hydrate(array $attributes): self
    {
        if (isset($attributes['select_ref'])) {
            $this->selectRef = $attributes['select_ref'];
        }

        if (isset($attributes['category_id'])) {
            $this->categoryId = (int) $attributes['category_id'];
        }

        if (isset($attributes['subcategory_id'])) {
            $this->subcategoryId = (int) $attributes['subcategory_id'];
        }

        if (isset($attributes['feature_id'])) {
            $this->featureId = (int) $attributes['feature_id'];
        }

        if (isset($attributes['feature1_id'])) {
            $this->feature1Id = (int) $attributes['feature1_id'];
        }

        if (isset($attributes['feature2_id'])) {
            $this->feature2Id = (int) $attributes['feature2_id'];
        }

        if (isset($attributes['entrys_number'])) {
            $this->entrysNumber = (int) $attributes['entrys_number'];
        }

        return $this;
    }

    public function convert(): array
    {
        $attributes = [];

        if ($this->selectRef !== null) {
            $attributes['select_ref'] = $this->selectRef;
        }

        if ($this->categoryId !== null) {
            $attributes['category_id'] = $this->categoryId;
        }

        if ($this->subcategoryId !== null) {
            $attributes['subcategory_id'] = $this->subcategoryId;
        }

        if ($this->featureId !== null) {
            $attributes['feature_id'] = $this->featureId;
        }

        if ($this->feature1Id !== null) {
            $attributes['feature1_id'] = $this->feature1Id;
        }

        if ($this->feature2Id !== null) {
            $attributes['feature2_id'] = $this->feature2Id;
        }

        if ($this->entrysNumber !== null) {
            $attributes['entrys_number'] = $this->entrysNumber;
        }

        return $attributes;
    }

    public function jsonSerialize(): array
    {
        return get_object_vars($this);
    }
}