<?php

declare(strict_types=1);

namespace App\Article\DTO;

use DateTimeImmutable;
use JsonSerializable;
use Paneric\Interfaces\Hydrator\HydratorInterface;

class FeatureDTO implements HydratorInterface, JsonSerializable
{
    private $id;
    
    private $ref;

    private $number;

    private $pl;

    private $en;

    private $createdAt;

    private $updatedAt;


    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getRef(): ?string
    {
        return $this->ref;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function getPl(): ?string
    {
        return $this->pl;
    }

    public function getEn(): ?string
    {
        return $this->en;
    }
    

    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function __set($name, $value)
    {
        $str ='';

        if (strpos($name, '_') !== false) {
            $str = str_replace(' ', '', ucwords(str_replace('_', ' ', $name)));
            $str[0] = lcfirst($str[0]);
        }

        if (
            $name === 'id' ||
            $name === 'number'
        ) {
            $this->$str = (int) $value;
        }

        if ($value !== null && ($name === 'created_at' || $name === 'updated_at')) {
            $this->$str = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $value);
        }
    }


    public function hydrate(array $attributes): self
    {
        if (isset($attributes['id'])) {
            $this->id = (int) $attributes['id'];
        }
        
        if (isset($attributes['ref'])) {
            $this->ref = $attributes['ref'];
        }

        if (isset($attributes['number'])) {
            $this->number = (int) $attributes['number'];
        }

        if (isset($attributes['pl'])) {
            $this->pl = $attributes['pl'];
        }

        if (isset($attributes['en'])) {
            $this->en = $attributes['en'];
        }

        return $this;
    }

    public function convert(): array
    {
        $attributes = [];

        if ($this->id !== null) {
            $attributes['id'] = $this->id;
        }
        
        if ($this->ref !== null) {
            $attributes['ref'] = $this->ref;
        }

        if ($this->number !== null) {
            $attributes['number'] = $this->number;
        }

        if ($this->pl !== null) {
            $attributes['pl'] = $this->pl;
        }

        if ($this->en !== null) {
            $attributes['en'] = $this->en;
        }

        return $attributes;
    }

    public function jsonSerialize(): array
    {
        return get_object_vars($this);
    }
}
