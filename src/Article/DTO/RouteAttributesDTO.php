<?php

declare(strict_types=1);

namespace App\Article\DTO;

use DateTimeImmutable;
use JsonSerializable;
use Paneric\Interfaces\Hydrator\HydratorInterface;

class RouteAttributesDTO implements HydratorInterface, JsonSerializable
{
    private $id;

    private $path;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function __set($name, $value)
    {
        if ($name === 'ref') {
            $this->path = $value;
        }
    }


    public function hydrate(array $attributes): self
    {
        if (isset($attributes['id'])) {
            $this->id = (int) $attributes['id'];
        }

        if (isset($attributes['path'])) {
            $this->path = $attributes['path'];
        }

        return $this;
    }

    public function convert(): array
    {
        $attributes = [];

        if ($this->id !== null) {
            $attributes['id'] = $this->id;
        }

        if ($this->path !== null) {
            $attributes['path'] = $this->path;
        }

        return $attributes;
    }

    public function jsonSerialize(): array
    {
        return get_object_vars($this);
    }
}