<?php

declare(strict_types=1);

namespace App\Article\Controller;

use Paneric\Controller\AppController;
use App\Article\Service\SubcategoryService;
use Paneric\Interfaces\Session\SessionInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Twig\Environment as Twig;

class SubcategoryController extends AppController
{
    protected $subcategoryService;

    public function __construct(Twig $twig, SessionInterface $session, SubcategoryService $subcategoryService)
    {
        parent:: __construct($twig, $session);

        $this->subcategoryService = $subcategoryService;
    }


    public function showAll(Response $response): Response
    {
        return $this->render(
            $response,
            '@module/subcategory/show_all.html.twig',
            $this->subcategoryService->getAllPaginated()
        );
    }

    public function show(Response $response, int $id): Response
    {
        return $this->render(
            $response,
            '@module/subcategory/show.html.twig',
            ['subcategory' => $this->subcategoryService->get($id)]
        );
    }

    public function add(Request $request, Response $response): Response
    {
        $result = $this->subcategoryService->create($request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/article/subcategorys/show',
                200
            );
        }

        return $this->render(
            $response,
            '@module/subcategory/add.html.twig',
            $result
        );
    }

    public function edit(Request $request, Response $response, int $id): Response
    {
        $result = $this->subcategoryService->update($id, $request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/article/subcategorys/show',
                200
            );
        }

        return $this->render(
            $response,
            '@module/subcategory/edit.html.twig',
            $result
        );
    }

    public function delete(Request $request, Response $response, int $id): Response
    {
        $result = $this->subcategoryService->delete($request, $id);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/article/subcategorys/show',
                200
            );
        }

        return $this->render(
            $response,
            '@module/subcategory/delete.html.twig',
            ['id' => $id]
        );
    }

    public function editFeatures(Request $request, Response $response, int $id): Response
    {
        $result = $this->subcategoryService->updateFeatures($id, $request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/article/subcategorys/show',
                200
            );
        }

        if (is_array($result)) {
            return $this->render(
                $response,
                '@module/feature/edit_multiple.html.twig',
                $result
            );
        }

        return $this->redirect(
            $response,
            '/article/subcategory/' . $id . '/features/delete',
            200
        );
    }

    public function deleteFeatures(Request $request, Response $response, int $id): Response
    {
        $result = $this->subcategoryService->deleteFeatures($request, $id);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/article/subcategorys/show',
                200
            );
        }

        return $this->render(
            $response,
            '@module/subcategory/delete_relations.html.twig',
            ['id' => $id]
        );
    }
}
