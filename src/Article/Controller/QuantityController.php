<?php

declare(strict_types=1);

namespace App\Article\Controller;

use Paneric\Controller\AppController;
use App\Article\Service\QuantityService;
use Paneric\Interfaces\Session\SessionInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Twig\Environment as Twig;

class QuantityController extends AppController
{
    protected $quantityService;

    public function __construct(Twig $twig, SessionInterface $session, QuantityService $quantityService)
    {
        parent:: __construct($twig, $session);

        $this->quantityService = $quantityService;
    }


    public function showAll(Response $response): Response
    {
        return $this->render(
            $response,
            '@module/quantity/show_all.html.twig',
            $this->quantityService->getAllPaginated()
        );
    }

    public function show(Response $response, int $id): Response
    {
        return $this->render(
            $response,
            '@module/quantity/show.html.twig',
            ['quantity' => $this->quantityService->getOneById($id)]
        );
    }

    public function add(Request $request, Response $response): Response
    {
        $result = $this->quantityService->create($request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/article/quantitys/show',
                200
            );
        }

        return $this->render(
            $response,
            '@module/quantity/add.html.twig',
            $result
        );
    }

    public function addMultiple(Request $request, Response $response): Response
    {
        $result = $this->quantityService->createMultiple($request);

        if ($result === null) {
            return $this->redirect(
                $response,
                '/article/quantitys/add',
                200
            );
        }

        return $this->render(
            $response,
            '@module/quantity/add_multiple.html.twig',
            $result
        );
    }

    public function edit(Request $request, Response $response, int $id): Response
    {
        $result = $this->quantityService->update($id, $request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/article/quantitys/show',
                200
            );
        }

        return $this->render(
            $response,
            '@module/quantity/edit.html.twig',
            $result
        );
    }

    public function editMultiple(Request $request, Response $response): Response
    {
        $result = $this->quantityService->updateMultiple($request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/article/quantitys/edit',
                200
            );
        }

        return $this->render(
            $response,
            '@module/quantity/edit_multiple.html.twig',
            $result
        );
    }

    public function delete(Request $request, Response $response, int $id): Response
    {
        $result = $this->quantityService->delete($request, $id);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/article/quantitys/show',
                200
            );
        }

        return $this->render(
            $response,
            '@module/quantity/delete.html.twig',
            ['id' => $id]
        );
    }
}
