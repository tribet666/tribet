<?php

declare(strict_types=1);

namespace App\Article\Controller;

use Paneric\Controller\AppController;
use App\Article\Service\CategoryService;
use Paneric\Interfaces\Session\SessionInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Twig\Environment as Twig;

class CategoryController extends AppController
{
    private $categoryService;

    public function __construct(Twig $twig, SessionInterface $session, CategoryService $categoryService)
    {
        parent:: __construct($twig, $session);

        $this->categoryService = $categoryService;
    }

    public function showAll(Response $response): Response
    {
        return $this->render(
            $response,
            '@module/category/show_all.html.twig',
            $this->categoryService->getAllPaginated()
        );
    }

    public function show(Response $response, int $id): Response
    {
        return $this->render(
            $response,
            '@module/category/show.html.twig',
            ['category' => $this->categoryService->get($id)]
        );
    }

    public function showAsTiles(Response $response): Response
    {
        return $this->render(
            $response,
            '@module/category/show_as_tiles.html.twig',
            $this->categoryService->getAllAsTiles()
        );
    }

    public function showRefferedAsTiles(Response $response, int $id): Response
    {
        return $this->render(
            $response,
            '@module/category/show_reffered_as_tiles.html.twig',
            $this->categoryService->getRefferedAsTiles($id)
        );
    }

    public function add(Request $request, Response $response): Response
    {
        $result = $this->categoryService->create($request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/article/categorys/show',
                200
            );
        }

        return $this->render(
            $response,
            '@module/category/add.html.twig',
            $result
        );
    }

    public function edit(Request $request, Response $response, int $id): Response
    {
        $result = $this->categoryService->update($id, $request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/article/categorys/show',
                200
            );
        }

        return $this->render(
            $response,
            '@module/category/edit.html.twig',
            $result
        );
    }

    public function delete(Request $request, Response $response, int $id): Response
    {
        $result = $this->categoryService->delete($request, $id);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/article/categorys/show',
                200
            );
        }

        return $this->render(
            $response,
            '@module/category/delete.html.twig',
            [
                'id' => $id
            ]
        );
    }
}
