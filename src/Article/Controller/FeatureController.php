<?php

declare(strict_types=1);

namespace App\Article\Controller;

use Paneric\Controller\AppController;
use App\Article\Service\FeatureService;
use Paneric\Interfaces\Session\SessionInterface;
use Twig\Environment as Twig;

class FeatureController extends AppController
{
    use FeatureControllerTrait;

    private $service;

    private $feature = 'feature';

    public function __construct(Twig $twig, SessionInterface $session, FeatureService $service)
    {
        parent:: __construct($twig, $session);

        $this->service = $service;
    }
}
