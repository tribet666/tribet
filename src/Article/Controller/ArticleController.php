<?php

declare(strict_types=1);

namespace App\Article\Controller;

use Paneric\Controller\AppController;
use App\Article\Service\ArticleService;
use Paneric\Interfaces\Session\SessionInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Twig\Environment as Twig;

class ArticleController extends AppController
{
    protected $articleService;

    public function __construct(Twig $twig, SessionInterface $session, ArticleService $articleService)
    {
        parent:: __construct($twig, $session);

        $this->articleService = $articleService;
    }

    public function present(Request $request, Response $response): Response
    {
        return $this->render(
            $response,
            '@module/article/present.html.twig'
        );
    }

    public function configure(Request $request, Response $response, int $subcategoryId): Response
    {
        $result = $this->articleService->configure($request, $subcategoryId);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/produkty',
                200
            );
        }

        return $this->render(
            $response,
            '@module/article/configure.html.twig',
            $result
        );
    }
}
