<?php

declare(strict_types=1);

namespace App\Article\Controller;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

trait FeatureControllerTrait
{
    public function showAll(Response $response): Response
    {
        return $this->render(
            $response,
            '@module/feature/show_all.html.twig',
            $this->service->getAllPaginated()
        );
    }

    public function show(Response $response, int $id): Response
    {
        return $this->render(
            $response,
            '@module/feature/show.html.twig',
            [$this->feature => $this->service->get($id)]
        );
    }

    public function add(Request $request, Response $response): Response
    {
        $result = $this->service->create($request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/article/' . $this->feature . 's/show',
                200
            );
        }

        return $this->render(
            $response,
            '@module/feature/add.html.twig',
            $result
        );
    }

    public function edit(Request $request, Response $response, int $id): Response
    {
        $result = $this->service->update($id, $request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/article/' . $this->feature . 's/show',
                200
            );
        }

        return $this->render(
            $response,
            '@module/feature/edit.html.twig',
            $result
        );
    }

    public function delete(Request $request, Response $response, int $id): Response
    {
        $result = $this->service->delete($request, $id);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/article/' . $this->feature . 's/show',
                200
            );
        }

        return $this->render(
            $response,
            '@module/feature/delete.html.twig',
            [
                'label' => $this->feature,
                'id' => $id
            ]
        );
    }
}