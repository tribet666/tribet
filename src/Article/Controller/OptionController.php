<?php

declare(strict_types=1);

namespace App\Article\Controller;

use Paneric\Controller\AppController;
use App\Article\Service\OptionService;
use Paneric\Interfaces\Session\SessionInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Twig\Environment as Twig;

class OptionController extends AppController
{
    protected $optionService;

    public function __construct(Twig $twig, SessionInterface $session, OptionService $optionService)
    {
        parent:: __construct($twig, $session);

        $this->optionService = $optionService;
    }


    public function showAll(Response $response): Response
    {
        return $this->render(
            $response,
            '@module/option/show_all.html.twig',
            $this->optionService->getAllPaginated()
        );
    }

    public function show(Response $response, int $id): Response
    {
        return $this->render(
            $response,
            '@module/option/show.html.twig',
            ['option' => $this->optionService->getOneById($id)]
        );
    }

    public function add(Request $request, Response $response): Response
    {
        $result = $this->optionService->create($request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/article/options/show',
                200
            );
        }

        return $this->render(
            $response,
            '@module/option/add.html.twig',
            $result
        );
    }

    public function edit(Request $request, Response $response, int $id): Response
    {
        $result = $this->optionService->update($id, $request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/article/options/show',
                200
            );
        }

        return $this->render(
            $response,
            '@module/option/edit.html.twig',
            $result
        );
    }

    public function editMultiple(Request $request, Response $response): Response
    {
        $result = $this->optionService->updateMultiple($request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/article/options/edit',
                200
            );
        }

        return $this->render(
            $response,
            '@module/option/edit_multiple.html.twig',
            $result
        );
    }

    public function delete(Request $request, Response $response, int $id): Response
    {
        $result = $this->optionService->delete($request, $id);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/article/options/show',
                200
            );
        }

        return $this->render(
            $response,
            '@module/option/delete.html.twig',
            ['id' => $id]
        );
    }
}
