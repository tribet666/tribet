<?php

declare(strict_types=1);

namespace App\Article\Query;

use Paneric\PdoWrapper\Manager;
use App\Article\ADTO\SubcategoryADTO;
use PDO;


class SubcategoryQuery
{
    private $manager;

    protected $featuresQuerySelect = '
            SELECT pt.* 
            FROM articles_subcategorys_features rpt
            LEFT JOIN articles_features pt ON rpt.feature_id = pt.id
            WHERE rpt.subcategory_id = :id
        ';

    public function __construct(Manager $manager)
    {
        $this->manager = $manager;
    }

    public function findAllPaginated(int $limit, int $offset): array
    {
        $this->manager->setDTOClass(SubcategoryADTO::class);
        $this->manager->setFetchMode(PDO::FETCH_CLASS);

        $query = '
            SELECT
                bt.id,
                bt.ref,
                bt.number,
                bt.label_pl,
                bt.label_en,
                st1.ref AS category_ref
            FROM articles_subcategorys bt
            LEFT JOIN articles_categorys st1 on bt.category_id = st1.id
            ORDER BY st1.number ASC, bt.number ASC
            LIMIT :oft, :lmt
        ';

        $stmt = $this->manager->setStmt($query, ['oft' => $offset, 'lmt' => $limit]);

        return $stmt->fetchAll();
    }

    public function findOneById($subcategoryId): ?object
    {
        $this->manager->setDTOClass(SubcategoryADTO::class);
        $this->manager->setFetchMode(PDO::FETCH_CLASS);

        $query = '
            SELECT
                bt.id,
                bt.ref,
                bt.number,
                bt.label_pl,
                bt.label_en,
                bt.info_pl,
                bt.info_en,
                bt.description_pl,
                bt.description_en,                    
                st1.ref AS category_ref,
                st1.number AS category_number,                   
                st1.label_pl AS category_label_pl,
                st1.label_en AS category_label_en,
                st1.info_pl AS category_info_pl,
                st1.info_en AS category_info_en,
                st1.description_pl AS category_description_pl,
                st1.description_en AS category_description_en              
            FROM articles_subcategorys bt
            LEFT JOIN articles_categorys st1 on bt.category_id = st1.id
            WHERE bt.id = :subcategory_id
        ';

        $stmt = $this->manager->setStmt($query, ['subcategory_id' => $subcategoryId]);
        $subcategory = $stmt->fetch();

        if ($subcategory !== false) {
            return $subcategory;
        }

        return null;
    }

    public function findByCategoryId($categoryId): array
    {
        $this->manager->setDTOClass(SubcategoryADTO::class);
        $this->manager->setFetchMode(PDO::FETCH_CLASS);

        $query = '
            SELECT
                bt.id,
                bt.ref,
                bt.number,
                bt.label_pl,
                bt.label_en,
                bt.info_pl,
                bt.info_en,
                bt.description_pl,
                bt.description_en,                    
                st1.ref AS category_ref,
                st1.number AS category_number,                   
                st1.label_pl AS category_label_pl,
                st1.label_en AS category_label_en,
                st1.info_pl AS category_info_pl,
                st1.info_en AS category_info_en,
                st1.description_pl AS category_description_pl,
                st1.description_en AS category_description_en 
            FROM articles_subcategorys bt
            LEFT JOIN articles_categorys st1 on bt.category_id = st1.id
            WHERE st1.id = :category_id
            ORDER BY bt.number ASC
        ';

        $stmt = $this->manager->setStmt($query, ['category_id' => $categoryId]);

        return $stmt->fetchAll();
    }

    public function findByCategoryIdAsTiles($categoryId): array
    {
        $this->manager->setDTOClass(SubcategoryADTO::class);
        $this->manager->setFetchMode(PDO::FETCH_CLASS);

        $query = '
            SELECT
                bt.id,
                bt.ref,
                bt.number,
                bt.label_pl,
                bt.label_en,
                bt.info_pl,
                bt.info_en                 
            FROM articles_subcategorys bt
            LEFT JOIN articles_categorys st1 on bt.category_id = st1.id
            WHERE st1.id = :category_id  AND bt.number != \'\'
            ORDER BY bt.number ASC
        ';

        $stmt = $this->manager->setStmt($query, ['category_id' => $categoryId]);

        return $stmt->fetchAll();
    }


    public function updateFeatures(int $subcategoryId, array $featuresIds): ?array
    {
        return $this->updateRelated(
            $subcategoryId,
            $featuresIds,
            'articles_subcategorys_features',
            'subcategory_id',
            'feature_id'
        );
    }

    protected function updateRelated(
        int $leadingId,
        array $relatedIds,
        string $queryTable,
        string $leadingIdName,
        string $relatedIdName
    ): ?array {
        try {
            $this->manager->setTable($queryTable);

            $this->manager->beginTransaction();

            $this->manager->delete([$leadingIdName => $leadingId]);

            $this->manager->createMultiple(
                $this->setIdsFieldSets(
                    $leadingIdName,
                    $relatedIdName,
                    $leadingId,
                    $relatedIds
                )
            );

            $this->manager->commit();
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return null;
    }


    public function findFeatures(int $subcategoryId): array
    {
        return $this->findRelated(
            $subcategoryId,
            'articles_subcategorys_features',
            FeatureDTO::class,
            PDO::FETCH_CLASS,
            $this->featuresQuerySelect
        );
    }

    protected function findRelated(
        int $leadingId,
        string $queryTable,
        string $relatedClass,
        int $fetchMethod,
        string $querySelect
    ): array {
        $this->manager->setTable($queryTable);
        $this->manager->setDTOClass($relatedClass);
        $this->manager->setFetchMode($fetchMethod);

        $stmt = $this->manager->setStmt($querySelect, ['id' => $leadingId]);

        return $stmt->fetchAll();
    }


    public function removeFeatures(int $subcategoryId): ?array
    {
        return $this->removeRelated($subcategoryId, 'articles_subcategorys_features', 'subcategory_id');
    }

    protected function removeRelated(
        int $leadingId,
        string $queryTable,
        string $leadingIdName
    ): ?array {
        try {
            $this->manager->setTable($queryTable);

            $this->manager->delete([$leadingIdName => $leadingId]);
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return null;
    }


    protected function setIdsFieldSets(
        string $leadingId,
        string $relatedId,
        int $leadingValue,
        array $relatedValues
    ): array {
        $relatedValesNumber = count($relatedValues);

        $relatedValues = array_values($relatedValues);

        $idsSets = [];
        for ($i = 0; $i < $relatedValesNumber; $i++) {
            $idsSets[$i][$leadingId] = $leadingValue;
            $idsSets[$i][$relatedId] = $relatedValues[$i];
        }

        return $idsSets;
    }
}
