<?php

declare(strict_types=1);

namespace App\Article\Query;

use Paneric\PdoWrapper\Manager;
use App\Article\ADTO\OptionADTO;
use PDO;


class OptionQuery
{
    private $manager;

    public function __construct(Manager $manager)
    {
        $this->manager = $manager;
    }

    public function findAllPaginated(int $limit, int $offset): array
    {
        $this->manager->setDTOClass(OptionADTO::class);
        $this->manager->setFetchMode(PDO::FETCH_CLASS);

        $query = '
            SELECT
                bt.id,
                bt.ref,
                bt.number,
                bt.price_factor,
                bt.pl,
                bt.en,
                st1.ref AS feature_ref,
                st1.pl AS feature_pl,
                st1.en AS feature_en
            FROM articles_options bt
            LEFT JOIN articles_features st1 on bt.feature_id = st1.id
            ORDER BY feature_ref ASC, bt.number ASC
            LIMIT :oft, :lmt
        ';

        $stmt = $this->manager->setStmt($query, ['oft' => $offset, 'lmt' => $limit]);

        return $stmt->fetchAll();
    }

    public function findOneById($optionId): ?object
    {
        $this->manager->setDTOClass(OptionADTO::class);
        $this->manager->setFetchMode(PDO::FETCH_CLASS);

        $query = '
            SELECT
                bt.id,
                bt.ref,
                bt.pl,
                bt.en,
                st1.ref AS feature_ref,
                st1.pl AS feature_pl,
                st1.en AS feature_en
            FROM articles_options bt
            LEFT JOIN articles_features st1 on bt.feature_id = st1.id
            WHERE bt.id = :option_id
        ';

        $stmt = $this->manager->setStmt($query, ['option_id' => $optionId]);
        $option = $stmt->fetch();

        if ($option !== false) {
            return $option;
        }

        return null;
    }

    public function findAllByCategoryId(int $categoryId): array
    {
        $this->manager->setTable('articles_subcategorys_features');
        $this->manager->setDTOClass(OptionADTO::class);
        $this->manager->setFetchMode(PDO::FETCH_ASSOC);

        $query = '
            SELECT
                st2.id,
                st2.pl,
                st2.en,
                st3.id AS feature_id,
                st3.ref AS feature_ref,                   
                st3.pl AS feature_pl,
                st3.pl AS feature_en,
                st1.ref AS subcategory_ref,
                st4.ref AS category_ref
            FROM `articles_subcategorys_features` bt
            LEFT JOIN `articles_subcategorys` st1 on bt.subcategory_id = st1.id
            LEFT JOIN `articles_categorys` st4 on st1.category_id = st4.id
            RIGHT JOIN `articles_options` st2 on bt.feature_id = st2.feature_id
            LEFT JOIN `articles_features` st3 on st2.feature_id = st3.id
            WHERE st4.id = :category_id
            ORDER BY st3.number ASC, st2.number
        ';

        $stmt = $this->manager->setStmt($query, ['category_id' => $categoryId]);

        return $stmt->fetchAll();
    }

    public function findAllBySubcategoryId(int $subcategoryId): array
    {
        $this->manager->setTable('articles_subcategorys_features');
        $this->manager->setDTOClass(OptionADTO::class);
        $this->manager->setFetchMode(PDO::FETCH_ASSOC);

        $query = '
            SELECT
                st2.id,
                st2.pl,
                st2.en,
                st3.id AS feature_id,
                st3.ref AS feature_ref,                   
                st3.pl AS feature_pl,
                st3.pl AS feature_en,
                st1.ref AS subcategory_ref,
                st4.ref AS category_ref
            FROM `articles_subcategorys_features` bt
            LEFT JOIN `articles_subcategorys` st1 on bt.subcategory_id = st1.id
            LEFT JOIN `articles_categorys` st4 on st1.category_id = st4.id
            RIGHT JOIN `articles_options` st2 on bt.feature_id = st2.feature_id
            LEFT JOIN `articles_features` st3 on st2.feature_id = st3.id
            WHERE st1.id = :subcategory_id
            ORDER BY st3.number ASC, st2.number
        ';

        $stmt = $this->manager->setStmt($query, ['subcategory_id' => $subcategoryId]);

        return $stmt->fetchAll();
    }

    public function findAllByFeatureId(int $featureId): array
    {
        $this->manager->setTable('articles_subcategorys_features');
        $this->manager->setDTOClass(OptionADTO::class);
        $this->manager->setFetchMode(PDO::FETCH_ASSOC);

        $query = '
            SELECT
                st2.id,
                st2.pl,
                st2.en,
                st3.id AS feature_id,
                st3.ref AS feature_ref,                   
                st3.pl AS feature_pl,
                st3.pl AS feature_en,
                st1.ref AS subcategory_ref,
                st4.ref AS category_ref
            FROM `articles_subcategorys_features` bt
            LEFT JOIN `articles_subcategorys` st1 on bt.subcategory_id = st1.id
            LEFT JOIN `articles_categorys` st4 on st1.category_id = st4.id
            RIGHT JOIN `articles_options` st2 on bt.feature_id = st2.feature_id
            LEFT JOIN `articles_features` st3 on st2.feature_id = st3.id
            WHERE st3.id = :feature_id
            ORDER BY st3.number ASC, st2.number
        ';

        $stmt = $this->manager->setStmt($query, ['feature_id' => $featureId]);

        return $stmt->fetchAll();
    }
}
