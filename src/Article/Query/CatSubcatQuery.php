<?php

declare(strict_types=1);

namespace App\Article\Query;

use Paneric\PdoWrapper\Manager;
use App\Article\DTO\CatSubcatDTO;
use PDO;

class CatSubcatQuery
{
    private $manager;

    private $local;

    public function __construct(Manager $manager, string $local)
    {
        $this->manager = $manager;

        $this->local = $local;
    }

    public function findOneById(int $catSubCatId): array
    {
        $this->manager->setFetchMode(PDO::FETCH_CLASS);
        $this->manager->setDTOClass(CatSubcatDTO::class);

        $query = '
                SELECT 
                    bt.id,
                    st1.'.$this->local.' AS category,
                    st2.'.$this->local.' AS subcategory
                FROM articles_categories_subcategories bt
                LEFT JOIN articles_categories st1 on bt.category_id = st1.id
                LEFT JOIN articles_subcategories st2 on bt.subcategory_id = st2.id
                WHERE bt.id = '.$catSubCatId
            ;

        $stmt = $this->setStmt($query);

        return $stmt->fetchAll();
    }
}