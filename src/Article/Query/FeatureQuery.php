<?php

declare(strict_types=1);

namespace App\Article\Query;

use Paneric\PdoWrapper\Manager;
use App\Article\DTO\FeatureDTO;
use PDO;

class FeatureQuery
{
    private $manager;

    public function __construct(Manager $manager)
    {
        $this->manager = $manager;
    }

    public function findBySubcategoryRef(string $subcategoryRef): array
    {
        $this->manager->setTable('articles_subcategorys_features');
        $this->manager->setDTOClass(FeatureDTO::class);
        $this->manager->setFetchMode(PDO::FETCH_CLASS);

        $query = '
                SELECT 
                    st2.*
                FROM  articles_subcategorys_features AS bt 
                LEFT JOIN articles_subcategorys st1 ON bt.subcategory_id = st1.id 
                LEFT JOIN articles_features st2 ON bt.feature_id = st2.id 
                WHERE st1.ref = :ref
            ';

        $stmt = $this->manager->setStmt($query, ['ref' => $subcategoryRef]);

        return $stmt->fetchAll();
    }
}
