<?php

declare(strict_types=1);

namespace App\Article\ADTO;

use DateTimeImmutable;
use JsonSerializable;
use Paneric\Interfaces\Hydrator\HydratorInterface;

class QuantityADTO implements HydratorInterface, JsonSerializable
{
    private $id;
    private $quantityPriceFactor;

    private $feature1OptionId;
    private $feature1OptionRef;
    private $feature1OptionNumber;
    private $feature1OptionPriceFactor;
    private $feature1FeatureId;
    private $feature1OptionPl;
    private $feature1OptionEn;

    private $feature2OptionId;
    private $feature2OptionRef;
    private $feature2OptionNumber;
    private $feature2OptionPriceFactor;
    private $feature2FeatureId;
    private $feature2OptionPl;
    private $feature2OptionEn;

    private $createdAt;
    private $updatedAt;



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuantityPriceFactor(): ?string
    {
        return $this->quantityPriceFactor;
    }



    public function getFeature1OptionId(): ?int
    {
        return $this->feature1OptionId;
    }

    public function getFeature1OptionRef(): ?int
    {
        return $this->feature1OptionRef;
    }

    public function getFeature1OptionNumber(): ?int
    {
        return $this->feature1OptionNumber;
    }

    public function getFeature1OptionPriceFactor(): ?int
    {
        return $this->feature1OptionPriceFactor;
    }

    public function getFeature1FeatureId(): ?int
    {
        return $this->feature1FeatureId;
    }

    public function getFeature1OptionPl(): ?int
    {
        return $this->feature1OptionPl;
    }

    public function getFeature1OptionEn(): ?int
    {
        return $this->feature1OptionEn;
    }



    public function getFeature2OptionId(): ?int
    {
        return $this->feature2OptionId;
    }

    public function getFeature2OptionRef(): ?int
    {
        return $this->feature2OptionRef;
    }

    public function getFeature2OptionNumber(): ?int
    {
        return $this->feature2OptionNumber;
    }

    public function getFeature2OptionPriceFactor(): ?int
    {
        return $this->feature2OptionPriceFactor;
    }

    public function getFeature2FeatureId(): ?int
    {
        return $this->feature2FeatureId;
    }

    public function getFeature2OptionPl(): ?int
    {
        return $this->feature2OptionPl;
    }

    public function getFeature2OptionEn(): ?int
    {
        return $this->feature2OptionEn;
    }



    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }



    public function __set($name, $value)
    {
        $str ='';

        if (strpos($name, '_') !== false) {
            $str = str_replace(' ', '', ucwords(str_replace('_', ' ', $name)));
            $str[0] = lcfirst($str[0]);
        }

        if (
            $name === 'quantity_price_factor' ||

            $name === 'feature1_option_ref' ||
            $name === 'feature1_option_pl' ||
            $name === 'feature1_option_en' ||

            $name === 'feature2_option_ref' ||
            $name === 'feature2_option_pl' ||
            $name === 'feature2_option_en'
        ) {
            $this->$str = $value;
        }

        if (
            $name === 'id' ||

            $name === 'feature1_option_id' ||
            $name === 'feature1_feature_id' ||
            $name === 'feature1_option_number' ||

            $name === 'feature2_option_id' ||
            $name === 'feature2_feature_id' ||
            $name === 'feature2_option_number'
        ) {
            $this->$str = (int) $value;
        }

        if (
            $name === 'feature1_price_factor' ||
            $name === 'feature2_price_factor'
        ) {
            $this->$str = number_format((float) $value, 2, '.', '');
        }

        if ($value !== null && ($name === 'created_at' || $name === 'updated_at')) {
            $this->$str = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $value);
        }
    }



    public function hydrate(array $attributes): self
    {
        if (isset($attributes['id'])) {
            $this->id = (int) $attributes['id'];
        }

        if (isset($attributes['quantity_price_factor'])) {
            $this->quantityPriceFactor = $attributes['quantity_price_factor'];
        }



        if (isset($attributes['feature1_option_id'])) {
            $this->feature1OptionId = (int) $attributes['feature1_option_id'];
        }

        if (isset($attributes['feature1_option_ref'])) {
            $this->feature1OptionRef = $attributes['feature1_option_ref'];
        }

        if (isset($attributes['feature1_option_number'])) {
            $this->feature1OptionRef = (int) $attributes['feature1_option_number'];
        }

        if (isset($attributes['feature1_option_price_factor'])) {
            $this->feature1OptionPriceFactor = (float) $attributes['feature1_option_price_factor'];
        }

        if (isset($attributes['feature1_feature_id'])) {
            $this->feature1FeatureId = (int) $attributes['feature1_feature_id'];
        }

        if (isset($attributes['feature1_option_pl'])) {
            $this->feature1OptionPl = $attributes['feature1_option_pl'];
        }

        if (isset($attributes['feature1_option_en'])) {
            $this->feature1OptionEn = $attributes['feature1_option_en'];
        }



        if (isset($attributes['feature2_option_id'])) {
            $this->feature2OptionId = (int) $attributes['feature2_option_id'];
        }

        if (isset($attributes['feature2_option_ref'])) {
            $this->feature2OptionRef = $attributes['feature2_option_ref'];
        }

        if (isset($attributes['feature2_option_number'])) {
            $this->feature2OptionRef = (int) $attributes['feature2_option_number'];
        }

        if (isset($attributes['feature2_option_price_factor'])) {
            $this->feature2OptionPriceFactor = (float) $attributes['feature2_option_price_factor'];
        }

        if (isset($attributes['feature2_feature_id'])) {
            $this->feature2FeatureId = (int) $attributes['feature2_feature_id'];
        }

        if (isset($attributes['feature2_option_pl'])) {
            $this->feature2OptionPl = $attributes['feature2_option_pl'];
        }

        if (isset($attributes['feature2_option_en'])) {
            $this->feature2OptionEn = $attributes['feature2_option_en'];
        }



        return $this;
    }

    public function convert(): array
    {
        $attributes = [];



        if ($this->id !== null) {
            $attributes['id'] = $this->id;
        }

        if ($this->quantityPriceFactor !== null) {
            $attributes['quantity_price_factor'] = $this->quantityPriceFactor;
        }



        if ($this->feature1OptionId !== null) {
            $attributes['feature1_option_id'] = $this->feature1OptionId;
        }

        if ($this->feature1OptionRef !== null) {
            $attributes['feature1_option_ref'] = $this->feature1OptionRef;
        }

        if ($this->feature1OptionNumber !== null) {
            $attributes['feature1_option_number'] = $this->feature1OptionNumber;
        }

        if ($this->feature1OptionPriceFactor !== null) {
            $attributes['feature1_option_price_factor'] = $this->feature1OptionPriceFactor;
        }

        if ($this->feature1FeatureId !== null) {
            $attributes['feature1_feature_id'] = $this->feature1FeatureId;
        }

        if ($this->feature1OptionPl !== null) {
            $attributes['feature1_option_pl'] = $this->feature1OptionPl;
        }

        if ($this->feature1OptionEn !== null) {
            $attributes['feature1_option_en'] = $this->feature1OptionEn;
        }



        if ($this->feature2OptionId !== null) {
            $attributes['feature2_option_id'] = $this->feature2OptionId;
        }

        if ($this->feature2OptionRef !== null) {
            $attributes['feature2_option_ref'] = $this->feature2OptionRef;
        }

        if ($this->feature2OptionNumber !== null) {
            $attributes['feature2_option_number'] = $this->feature2OptionNumber;
        }

        if ($this->feature2OptionPriceFactor !== null) {
            $attributes['feature2_option_price_factor'] = $this->feature2OptionPriceFactor;
        }

        if ($this->feature2FeatureId !== null) {
            $attributes['feature2_feature_id'] = $this->feature2FeatureId;
        }

        if ($this->feature2OptionPl !== null) {
            $attributes['feature2_option_pl'] = $this->feature2OptionPl;
        }

        if ($this->feature2OptionEn !== null) {
            $attributes['feature2_option_en'] = $this->feature2OptionEn;
        }



        return $attributes;
    }

    public function jsonSerialize(): array
    {
        return get_object_vars($this);
    }
}
