<?php

declare(strict_types=1);

namespace App\Article\ADTO;

use DateTimeImmutable;
use JsonSerializable;
use Paneric\Interfaces\Hydrator\HydratorInterface;

class OptionADTO implements HydratorInterface, JsonSerializable
{
    private $id;
    private $ref;
    private $number;
    private $priceFactor;
    private $pl;
    private $en;

    private $featureId;
    private $featureRef;
    private $featurePl;
    private $featureEn;

    private $subcategoryId;
    private $subcategoryRef;
    private $subcategoryPl;
    private $subcategoryEn;

    private $categoryId;
    private $categoryRef;
    private $categoryPl;
    private $categoryEn;

    private $createdAt;
    private $updatedAt;


    public function getId(): ?int
    {
        return $this->id;
    }
    public function getRef(): ?string
    {
        return $this->ref;
    }
    public function getNumber(): ?int
    {
        return $this->number;
    }
    public function getPriceFactor(): ?float
    {
        return $this->priceFactor;
    }
    public function getPl(): ?string
    {
        return $this->pl;
    }
    public function getEn(): ?string
    {
        return $this->en;
    }


    public function getFeatureId(): ?int
    {
        return $this->featureId;
    }
    public function getFeatureRef(): ?string
    {
        return $this->featureRef;
    }
    public function getFeaturePl(): ?string
    {
        return $this->featurePl;
    }
    public function getFeatureEn(): ?string
    {
        return $this->featureEn;
    }

    public function getSubcategorId(): ?int
    {
        return $this->subcategoryId;
    }
    public function getSubcategoryRef(): ?string
    {
        return $this->subcategoryRef;
    }
    public function getSubcategoryPl(): ?string
    {
        return $this->subcategoryPl;
    }
    public function getSubcategoryEn(): ?string
    {
        return $this->subcategoryEn;
    }

    public function getCategorId(): ?int
    {
        return $this->categoryId;
    }
    public function getCategoryRef(): ?string
    {
        return $this->categoryRef;
    }
    public function getCategoryPl(): ?string
    {
        return $this->categoryPl;
    }
    public function getCategoryEn(): ?string
    {
        return $this->categoryEn;
    }


    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }
    public function getUpdatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }


    public function __set($name, $value)
    {
        $str ='';

        if (strpos($name, '_') !== false) {
            $str = str_replace(' ', '', ucwords(str_replace('_', ' ', $name)));
            $str[0] = lcfirst($str[0]);
        }

        if (
            $name === 'feature_ref' ||
            $name === 'feature_pl' ||
            $name === 'feature_en' ||
            $name === 'subcategory_ref' ||
            $name === 'subcategory_pl' ||
            $name === 'subcategory_en' ||
            $name === 'category_ref' ||
            $name === 'category_pl' ||
            $name === 'category_en'
        ) {
            $this->$str = $value;
        }

        if (
            $name === 'id' ||
            $name === 'feature_id' ||
            $name === 'subcategory_id' ||
            $name === 'category_id' ||
            $name === 'number'
        ) {
            $this->$str = (int) $value;
        }

        if ($name === 'price_factor') {
            $this->$str = number_format((float) $value, 2, '.', '');
        }

        if ($value !== null && ($name === 'created_at' || $name === 'updated_at')) {
            $this->$str = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $value);
        }
    }

    public function hydrate(array $attributes): self
    {
        if (isset($attributes['id'])) {
            $this->id = $attributes['id'];
        }

        if (isset($attributes['ref'])) {
            $this->ref = $attributes['ref'];
        }

        if (isset($attributes['number'])) {
            $this->number = (int) $attributes['number'];
        }

        if (isset($attributes['price_factor'])) {
            $this->priceFactor = number_format((float) $attributes['price_factor'], 2, '.', '');
        }

        if (isset($attributes['pl'])) {
            $this->pl = $attributes['pl'];
        }

        if (isset($attributes['en'])) {
            $this->en = $attributes['en'];
        }



        if (isset($attributes['feature_id'])) {
            $this->featureId = $attributes['feature_id'];
        }

        if (isset($attributes['feature_ref'])) {
            $this->featureRef = $attributes['feature_ref'];
        }

        if (isset($attributes['feature_pl'])) {
            $this->featurePl = $attributes['feature_pl'];
        }

        if (isset($attributes['feature_en'])) {
            $this->featureEn = $attributes['feature_en'];
        }



        if (isset($attributes['subcategory_id'])) {
            $this->subcategoryId = $attributes['subcategory_id'];
        }

        if (isset($attributes['subcategory_ref'])) {
            $this->subcategoryRef = $attributes['subcategory_ref'];
        }

        if (isset($attributes['subcategory_pl'])) {
            $this->subcategoryPl = $attributes['subcategory_pl'];
        }

        if (isset($attributes['subcategory_en'])) {
            $this->subcategoryEn = $attributes['subcategory_en'];
        }



        if (isset($attributes['category_id'])) {
            $this->categoryId = $attributes['category_id'];
        }

        if (isset($attributes['category_ref'])) {
            $this->categoryRef = $attributes['category_ref'];
        }

        if (isset($attributes['category_pl'])) {
            $this->categoryPl = $attributes['category_pl'];
        }

        if (isset($attributes['category_en'])) {
            $this->categoryEn = $attributes['category_en'];
        }



        return $this;
    }

    public function convert(): array
    {
        $attributes = [];

        if ($this->id !== null) {
            $attributes['id'] = $this->id;
        }

        if ($this->ref !== null) {
            $attributes['ref'] = $this->ref;
        }

        if ($this->number !== null) {
            $attributes['number'] = $this->number;
        }

        if ($this->priceFactor !== null) {
            $attributes['price_factor'] = $this->priceFactor;
        }

        if ($this->pl !== null) {
            $attributes['pl'] = $this->pl;
        }

        if ($this->en !== null) {
            $attributes['en'] = $this->en;
        }



        if ($this->featureId !== null) {
            $attributes['feature_id'] = $this->featureId;
        }

        if ($this->featureRef !== null) {
            $attributes['feature_ref'] = $this->featureRef;
        }

        if ($this->featurePl !== null) {
            $attributes['feature_pl'] = $this->featurePl;
        }

        if ($this->featureEn !== null) {
            $attributes['feature_en'] = $this->featureEn;
        }



        if ($this->subcategoryId !== null) {
            $attributes['subcategory_id'] = $this->subcategoryId;
        }

        if ($this->subcategoryRef !== null) {
            $attributes['subcategory_ref'] = $this->subcategoryRef;
        }

        if ($this->subcategoryPl !== null) {
            $attributes['subcategory_pl'] = $this->subcategoryPl;
        }

        if ($this->subcategoryEn !== null) {
            $attributes['subcategory_en'] = $this->subcategoryEn;
        }



        if ($this->categoryId !== null) {
            $attributes['category_id'] = $this->categoryId;
        }

        if ($this->categoryRef !== null) {
            $attributes['category_ref'] = $this->categoryRef;
        }

        if ($this->categoryPl !== null) {
            $attributes['category_pl'] = $this->categoryPl;
        }

        if ($this->categoryEn !== null) {
            $attributes['category_en'] = $this->categoryEn;
        }



        return $attributes;
    }

    public function jsonSerialize(): array
    {
        return get_object_vars($this);
    }
}
