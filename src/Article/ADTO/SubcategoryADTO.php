<?php

declare(strict_types=1);

namespace App\Article\ADTO;

use DateTimeImmutable;
use JsonSerializable;
use Paneric\Interfaces\Hydrator\HydratorInterface;

class SubcategoryADTO implements HydratorInterface, JsonSerializable
{
    private $id;



    private $ref;

    private $number;

    private $labelPl;

    private $labelEn;

    private $infoPl;

    private $infoEn;

    private $descriptionPl;

    private $descriptionEn;



    private $categoryRef;

    private $categoryNumber;

    private $categoryLabelPl;

    private $categoryLabelEn;

    private $categoryInfoPl;

    private $categoryInfoEn;

    private $categoryDescriptionPl;

    private $categoryDescriptionEn;



    private $createdAt;

    private $updatedAt;



    public function getId(): ?int
    {
        return $this->id;
    }



    public function getRef(): ?string
    {
        return $this->ref;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function getLabelPl(): ?string
    {
        return $this->labelPl;
    }

    public function getLabelEn(): ?string
    {
        return $this->labelEn;
    }

    public function getInfoPl(): ?string
    {
        return $this->infoPl;
    }

    public function getInfoEn(): ?string
    {
        return $this->infoEn;
    }

    public function getDescriptionPl(): ?string
    {
        return $this->descriptionPl;
    }

    public function getDescriptionEn(): ?string
    {
        return $this->descriptionEn;
    }



    public function getCategoryRef(): ?string
    {
        return $this->categoryRef;
    }

    public function getCategoryNumber(): ?int
    {
        return $this->categoryNumber;
    }

    public function getCategoryLabelPl(): ?string
    {
        return $this->categoryLabelPl;
    }

    public function getCategoryLabelEn(): ?string
    {
        return $this->categoryLabelEn;
    }

    public function getCategoryInfoPl(): ?string
    {
        return $this->categoryInfoPl;
    }

    public function getCategoryInfoEn(): ?string
    {
        return $this->categoryInfoEn;
    }

    public function getCategoryDescriptionPl(): ?string
    {
        return $this->categoryDescriptionPl;
    }

    public function getCategoryDescriptionEn(): ?string
    {
        return $this->categoryDescriptionEn;
    }



    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }



    public function __set($name, $value)
    {
        $str ='';

        if (strpos($name, '_') !== false) {
            $str = str_replace(' ', '', ucwords(str_replace('_', ' ', $name)));
            $str[0] = lcfirst($str[0]);
        }

        if (
            $name === 'label_pl' ||
            $name === 'label_en' ||
            $name === 'info_pl' ||
            $name === 'info_en' ||
            $name === 'description_pl' ||
            $name === 'description_en' ||

            $name === 'category_ref' ||
            $name === 'category_label_pl' ||
            $name === 'category_label_en' ||
            $name === 'category_info_pl' ||
            $name === 'category_info_en' ||
            $name === 'category_description_pl' ||
            $name === 'category_description_en'
        ) {
            $this->$str = $value;
        }

        if (
            $name === 'id' ||
            $name === 'number' ||
            $name === 'category_number'
        ) {
            $this->$str = (int) $value;
        }

        if ($value !== null && ($name === 'created_at' || $name === 'updated_at')) {
            $this->$str = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $value);
        }
    }

    public function hydrate(array $attributes): self
    {
        if (isset($attributes['id'])) {
            $this->id = $attributes['id'];
        }



        if (isset($attributes['ref'])) {
            $this->ref = $attributes['ref'];
        }

        if (isset($attributes['number'])) {
            $this->number = (int) $attributes['number'];
        }

        if (isset($attributes['label_pl'])) {
            $this->labelPl = $attributes['label_pl'];
        }

        if (isset($attributes['label_en'])) {
            $this->labelEn = $attributes['label_en'];
        }

        if (isset($attributes['info_pl'])) {
            $this->infoPl = $attributes['info_pl'];
        }

        if (isset($attributes['info_en'])) {
            $this->infoEn = $attributes['info_en'];
        }

        if (isset($attributes['description_pl'])) {
            $this->descriptionPl = $attributes['description_pl'];
        }

        if (isset($attributes['description_en'])) {
            $this->descriptionEn = $attributes['description_en'];
        }



        if (isset($attributes['category_ref'])) {
            $this->categoryRef = $attributes['category_ref'];
        }

        if (isset($attributes['category_number'])) {
            $this->categoryNumber = (int) $attributes['category_number'];
        }

        if (isset($attributes['category_label_pl'])) {
            $this->categoryLabelPl = $attributes['category_label_pl'];
        }

        if (isset($attributes['category_label_en'])) {
            $this->categoryLabelEn = $attributes['category_label_en'];
        }

        if (isset($attributes['category_info_pl'])) {
            $this->categoryInfoPl = $attributes['category_info_pl'];
        }

        if (isset($attributes['category_info_en'])) {
            $this->categoryInfoEn = $attributes['category_info_en'];
        }

        if (isset($attributes['category_description_pl'])) {
            $this->categoryDescriptionPl = $attributes['category_description_pl'];
        }

        if (isset($attributes['category_description_en'])) {
            $this->categoryDescriptionEn = $attributes['category_description_en'];
        }



        return $this;
    }

    public function convert(): array
    {
        $attributes = [];



        if ($this->id !== null) {
            $attributes['id'] = $this->id;
        }



        if ($this->ref !== null) {
            $attributes['ref'] = $this->ref;
        }

        if ($this->number !== null) {
            $attributes['number'] = $this->number;
        }

        if ($this->labelPl !== null) {
            $attributes['label_pl'] = $this->labelPl;
        }

        if ($this->labelEn !== null) {
            $attributes['label_en'] = $this->labelEn;
        }

        if ($this->infoPl !== null) {
            $attributes['info_pl'] = $this->infoPl;
        }

        if ($this->infoEn !== null) {
            $attributes['info_en'] = $this->infoEn;
        }

        if ($this->descriptionPl !== null) {
            $attributes['description_pl'] = $this->descriptionPl;
        }

        if ($this->descriptionEn !== null) {
            $attributes['description_en'] = $this->descriptionEn;
        }



        if ($this->categoryRef !== null) {
            $attributes['category_ref'] = $this->categoryRef;
        }

        if ($this->categoryNumber !== null) {
            $attributes['category_number'] = $this->categoryNumber;
        }

        if ($this->categoryLabelPl !== null) {
            $attributes['category_label_pl'] = $this->categoryLabelPl;
        }

        if ($this->categoryLabelEn !== null) {
            $attributes['category_label_en'] = $this->categoryLabelEn;
        }

        if ($this->categoryInfoPl !== null) {
            $attributes['category_info_pl'] = $this->categoryInfoPl;
        }

        if ($this->categoryInfoEn !== null) {
            $attributes['category_info_en'] = $this->categoryInfoEn;
        }

        if ($this->categoryDescriptionPl !== null) {
            $attributes['category_description_pl'] = $this->categoryDescriptionPl;
        }

        if ($this->categoryDescriptionEn !== null) {
            $attributes['category_description_en'] = $this->categoryDescriptionEn;
        }



        return $attributes;
    }

    public function jsonSerialize(): array
    {
        return get_object_vars($this);
    }
}