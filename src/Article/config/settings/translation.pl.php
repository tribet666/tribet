<?php

return [
    'translation_module' => [
        'message_title' => 'Tribet - wiadomość',

        'ref' => 'Referencja',
        'number' => 'n°',
        'category' => 'Kategoria',
        'subcategory' => 'Podkategoria',
        'feature' => 'Parametr',
        'info' => 'Opis',


        'cancel' => 'Anuluj',
        'add' => 'Dodaj',
        'edit' => 'Edytuj',
        'confirm' => 'Potwierdź',
        'modify' => 'Modyfikuj',
        'update' => 'Aktualizuj',
        'delete' => 'Usuń',
        'choose' => 'Wybierz...',
        'back' => 'Wróć',


        'content_articles_show_title' => 'Artykuły',
        'content_categorys_show_title' => 'Kategorie',
        'content_corners_show_title' => 'Narożniki',
        'content_features_show_title' => 'Parametry',
        'content_options_show_title' => 'Opcje parametrów',
        'content_subcategorys_show_title' => 'Podkategorie',
        'content_quantitys_show_title' => 'Nakłady',


        'form_article_add_title' => 'Dodaj nowy artykuł',
        'form_category_add_title' => 'Dodaj nową kategorię',
        'form_feature_add_title' => 'Dodaj parametr',
        'form_option_add_title' => 'Dodaj nowa opcję parametru',
        'form_subcategory_add_title' => 'Dodaj nową podkategorię',
        'form_quantitys_add_search_criteria_title' => 'Kryteria wyboru nakładów',
        'form_quantitys_add_title' => 'Dodaj nowe nakłady',


        'form_article_delete_title' => 'Usuń artykuł',
        'form_category_delete_title' => 'Usuń kategorię',
        'form_feature_delete_title' => 'Usuń parametr',
        'form_option_delete_title' => 'Usuń opcję parametru',
        'form_subcategory_delete_title' => 'Usuń podkategorię',
        'form_subcategory_features_delete_title' => 'Usuń relacje parametrow do podkategorii',

        'form_article_edit_title' => 'Modyfikuj artykuł',
        'form_category_edit_title' => 'Modyfikuj kategorię',
        'form_feature_edit_title' => 'Modyfikuj parametr',
        'form_option_edit_title' => 'Modyfikuj opcję parametru',
        'form_options_edit_search_criteria_title' => 'Kryteria wyboru opcji',
        'form_options_edit_title' => 'Modyfikuj opcje parametru',
        'form_subcategory_edit_title' => 'Modyfikuj podkategorię',
        'form_subcategory_features_edit_title' => 'Modyfikuj relacje parametrow do podkategorii',


        'article_delete_warning' => 'Zamierzasz usunąć artykuł. Wymagane potwierdzenie',
        'category_delete_warning' => 'Zamierzasz usunąć kategorię. Wymagane potwierdzenie',
        'feature_delete_warning' => 'Zamierzasz usunąć parametr. Wymagane potwierdzenie',
        'option_delete_warning' => 'Zamierzasz usunąć opcję parametru. Wymagane potwierdzenie',
        'subcategory_delete_warning' => 'Zamierzasz usunąć podkategorię. Wymagane potwierdzenie',
        'subcategory_features_delete_warning' => 'Zamierzasz usunąć relacje parametrow do podkategorii. Wymagane potwierdzenie',


        'article_ref_help' => 'Wprowadź krótką referencję artykułu',
        'category_ref_help' => 'Wprowadź krótką referencję kategorii',
        'feature_ref_help' => 'Wprowadź krótką referencję parametru',
        'option_ref_help' => 'Wprowadź krótką referencję opcji parametru',
        'subcategory_ref_help' => 'Wprowadź krótką referencję podkategorii',

        'category_number_help' => 'Wprowadź numer porządkowy kafla html',
        'subcategory_number_help' => 'Wprowadź numer porządkowy kafla html',

        'db_article_unique_error' => 'Wprowadzona rerefencja artykułu już istnieje.',
        'db_category_unique_error' => 'Wprowadzona rerefencja kategorii już istnieje',
        'db_feature_unique_error' => 'Wprowadzona rerefencja parametru już istnieje',
        'db_option_unique_error' => 'Wprowadzona rerefencja opcji parametru już istnieje',
        'db_subcategory_unique_error' => 'Wprowadzona rerefencja podkategorii już istnieje',

        'db_options_update_multiple_error' => 'Edycja opcji zakończyła sie niepowodzeniem',

        'article_info_help' => 'Wprowadź opis artykułu',

        'format_size_help' => 'Wybierz rozmiar formatu',

        'feature_help' => 'Wybierz parameter opcji',

        'version_pl_help' => 'Etykieta dla referencji w języku polskim',
        'version_en_help' => 'Etykieta dla referencji w języku angielskim'
    ],
];
