<?php

return [
    'translation_module' => [
        'message_title' => 'Tribet - message',


        'ref' => 'Reference',
        'number' => 'n°',
        'category' => 'Category',
        'subcategory' => 'Subcategory',
        'feature' => 'Feature',
        'info' => 'Description',


        'cancel' => 'Cancel',
        'add' => 'Add',
        'edit' => 'Edit',
        'confirm' => 'Confirm',
        'modify' => 'Modify',
        'update' => 'Update',
        'delete' => 'Delete',
        'choose' => 'Choose...',
        'back' => 'Back',


        'content_articles_show_title' => 'Articles',
        'content_categorys_show_title' => 'Categories',
        'content_features_show_title' => 'Features',
        'content_options_show_title' => 'Features options',
        'content_subcategorys_show_title' => 'Subcategories',
        'content_quantitys_show_title' => 'Quantities',


        'form_article_add_title' => 'Add new article',
        'form_category_add_title' => 'Add new category',
        'form_feature_add_title' => 'Add new feature',
        'form_option_add_title' => 'Add new feature option',
        'form_subcategory_add_title' => 'Add new subcategory',
        'form_quantitys_add_search_criteria_title' => 'Quantities search criteria',
        'form_quantitys_add_title' => 'Add new quantities',


        'form_article_delete_title' => 'Delete article',
        'form_category_delete_title' => 'Delete category',
        'form_feature_delete_title' => 'Delete feature',
        'form_option_delete_title' => 'Delete feature option',
        'form_subcategory_delete_title' => 'Delete subcategory',
        'form_subcategory_features_delete_title' => 'Delete features relations to subcategory',


        'form_article_edit_title' => 'Modify article',
        'form_category_edit_title' => 'Modify category',
        'form_feature_edit_title' => 'Modify feature',
        'form_option_edit_title' => 'Modify feature option',
        'form_options_edit_search_criteria_title' => 'Options search criteria',
        'form_options_edit_title' => 'Modify parameters\' options',
        'form_subcategory_edit_title' => 'Modify subcategory',
        'form_subcategory_features_edit_title' => 'Modify features relations to subcategory',


        'article_delete_warning' => 'Your are about to delete an article. Please confirm',
        'category_delete_warning' => 'Your are about to delete a category. Please confirm',
        'feature_delete_warning' => 'Your are about to delete a feature. Please confirm',
        'option_delete_warning' => 'Your are about to delete a feature option. Please confirm',
        'subcategory_delete_warning' => 'Your are about to delete a subcategory. Please confirm',
        'subcategory_features_delete_warning' => 'Your are about to delete features relations to subcategory. Please confirm',


        'article_ref_help' => 'Enter short article reference',
        'category_ref_help' => 'Enter short category reference',
        'feature_ref_help' => 'Enter short feature reference',
        'option_ref_help' => 'Enter short feature option reference',
        'subcategory_ref_help' => 'Enter short subcategory reference',

        'category_number_help' => 'Enter html tile order number',
        'subcategory_number_help' => 'Enter html tile order number',

        'db_article_unique_error' => 'Entered article reference is already in use',
        'db_category_unique_error' => 'Entered category reference is already in use',
        'db_feature_unique_error' => 'Entered feature reference is already in use',
        'db_option_unique_error' => 'Entered feature option reference is already in use',
        'db_subcategory_unique_error' => 'Entered subcategory reference is already in use',

        'db_options_update_multiple_error' => 'Multiple options update failed',

        'article_info_help' => 'Enter article description',

        'format_size_help' => 'Choose format size',

        'feature_help' => 'Choose options\'s parameter',

        'version_pl_help' => 'Label for reference in Polish',
        'version_en_help' => 'Label for reference in English'
    ],
];
