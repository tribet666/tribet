<?php

declare(strict_types=1);

use App\Article\DTO\ArticleDTO;
use App\Article\DTO\CategoryDTO;
use App\Article\DTO\OptionsSearchCriteriaDTO;
use App\Article\DTO\QuantityDTO;
use App\Article\DTO\SubcategoryDTO;
use App\Article\DTO\FeatureDTO;
use App\Article\DTO\OptionDTO;

return [
    'validation' => [

        'article.article.add' => [
            'methods' => ['POST'],
            ArticleDTO::class => [
                'rules' => [
                    'ref' => [
                        'required' => [],
                        'is_all_alpha_numeric' => [],
                    ],
                    'pl' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                    'en' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                    'info' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                ],
            ],
        ],
        'article.article.delete' => [
            'methods' => ['POST'],
            ArticleDTO::class => [
                'rules' => [],
            ],
        ],
        'article.article.edit' => [
            'methods' => ['POST'],
            ArticleDTO::class => [
                'rules' => [
                    'ref' => [
                        'required' => [],
                        'is_all_alpha_numeric' => [],
                    ],
                    'pl' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                    'en' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                    'info' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                ],
            ],
        ],



        'article.category.add' => [
            'methods' => ['POST'],
            CategoryDTO::class => [
                'rules' => [
                    'ref' => [
                        'required' => [],
                        'is_all_alpha_numeric' => [],
                    ],
                    'number' => [
                        'is_all_numeric' => [],
                    ],
                    'label_pl' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                    'label_en' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                    'info_pl' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                    'info_en' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                    'description_pl' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                    'description_en' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                ],
            ],
        ],
        'article.category.delete' => [
            'methods' => ['POST'],
            CategoryDTO::class => [
                'rules' => [],
            ],
        ],
        'article.category.edit' => [
            'methods' => ['POST'],
            CategoryDTO::class => [
                'rules' => [
                    'ref' => [
                        'required' => [],
                        'is_all_alpha_numeric' => [],
                    ],
                    'number' => [
                        'is_all_numeric' => [],
                    ],
                    'label_pl' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                    'label_en' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                    'info_pl' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                    'info_en' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                    'description_pl' => [
                        'is_text' => [],
                    ],
                    'description_en' => [
                        'is_text' => [],
                    ],
                ],
            ],
        ],



        'article.subcategory.add' => [
            'methods' => ['POST'],
            SubcategoryDTO::class => [
                'rules' => [
                    'category_id' => [
                        'required' => [],
                        'is_all_numeric' => [],
                    ],
                    'ref' => [
                        'required' => [],
                        'is_all_alpha_numeric' => [],
                    ],
                    'number' => [
                        'is_all_numeric' => [],
                    ],
                    'label_pl' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                    'label_en' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                    'info_pl' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                    'info_en' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                    'description_pl' => [
                        'is_text' => [],
                    ],
                    'description_en' => [
                        'is_text' => [],
                    ],
                ],
            ],
        ],
        'article.subcategory.delete' => [
            'methods' => ['POST'],
            SubcategoryDTO::class => [
                'rules' => [],
            ],
        ],
        'article.subcategory.edit' => [
            'methods' => ['POST'],
            SubcategoryDTO::class => [
                'category_id' => [
                    'required' => [],
                    'is_all_numeric' => [],
                ],
                'rules' => [
                    'ref' => [
                        'required' => [],
                        'is_all_alpha_numeric' => [],
                    ],
                    'number' => [
                        'is_all_numeric' => [],
                    ],
                    'label_pl' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                    'label_en' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                    'info_pl' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                    'info_en' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                    'description_pl' => [
                        'is_text' => [],
                    ],
                    'description_en' => [
                        'is_text' => [],
                    ],
                ],
            ],
        ],
        'article.subcategory.features.edit' => [
            'methods' => ['POST'],
            FeatureDTO::class => [
                'rules' => [
                    'id' => [
                        'is_integer' => [],
                    ],
                ],
            ],
        ],
        'article.subcategory.features.delete' => [
            'methods' => ['POST'],
            FeatureDTO::class => [
                'rules' => [],
            ],
        ],



        'article.feature.add' => [
            'methods' => ['POST'],
            FeatureDTO::class => [
                'rules' => [
                    'ref' => [
                        'required' => [],
                        'is_all_alpha_numeric' => [],
                    ],
                    'number' => [
                        'is_all_numeric' => [],
                    ],
                    'pl' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                    'en' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                ],
            ],
        ],
        'article.feature.delete' => [
            'methods' => ['POST'],
            FeatureDTO::class => [
                'rules' => [],
            ],
        ],
        'article.feature.edit' => [
            'methods' => ['POST'],
            FeatureDTO::class => [
                'rules' => [
                    'ref' => [
                        'required' => [],
                        'is_all_alpha_numeric' => [],
                    ],
                    'number' => [
                        'is_all_numeric' => [],
                    ],
                    'pl' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                    'en' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                ],
            ],
        ],



        'article.option.add' => [
            'methods' => ['POST'],
            OptionDTO::class => [
                'rules' => [
                    'feature_id' => [
                        'required' => [],
                        'is_all_numeric' => [],
                    ],
                    'ref' => [
                        'required' => [],
                        'is_all_alpha_numeric' => [],
                    ],
                    'number' => [
                        'is_all_numeric' => [],
                    ],
                    'price_factor' => [
                        'is_float' => [],
                    ],
                    'pl' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                    'en' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                ],
            ],
        ],
        'article.option.delete' => [
            'methods' => ['POST'],
            OptionDTO::class => [
                'rules' => [],
            ],
        ],
        'article.option.edit' => [
            'methods' => ['POST'],
            OptionDTO::class => [
                'rules' => [
                    'feature_id' => [
                        'required' => [],
                        'is_all_numeric' => [],
                    ],
                    'ref' => [
                        'required' => [],
                        'is_all_alpha_numeric' => [],
                    ],
                    'number' => [
                        'is_all_numeric' => [],
                    ],
                    'price_factor' => [
                        'is_float' => [],
                    ],
                    'pl' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                    'en' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                ],
            ],
        ],
        'article.options.edit' => [
            'methods' => ['POST'],
            OptionDTO::class => [
                'rules' => [
                    'id' => [
                        'required' => [],
                        'is_all_numeric' => [],
                    ],
                    'feature_id' => [
                        'required' => [],
                        'is_all_numeric' => [],
                    ],
                    'ref' => [
                        'required' => [],
                        'is_all_alpha_numeric' => [],
                    ],
                    'number' => [
                        'required' => [],
                        'is_all_numeric' => [],
                    ],
                    'price_factor' => [
                        'required' => [],
                        'is_float' => [],
                    ],
                    'pl' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                    'en' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                ],
            ],
            OptionsSearchCriteriaDTO::class => [
                'rules' => [
                    'category_id' => [
                        'required' => [],
                        'is_all_numeric' => [],
                    ],
                    'subcategory_id' => [
                        'required' => [],
                        'is_all_numeric' => [],
                    ],
                    'feature_id' => [
                        'required' => [],
                        'is_all_numeric' => [],
                    ],
                ],
            ],
        ],



        'article.options.search' => [
            'methods' => ['POST'],
            OptionsSearchCriteriaDTO::class => [
                'rules' => [
                    'select_ref' => [
                        'required' => [],
                        'is_one_of' => ['categorys', 'subcategorys', 'features'],
                    ],
                    'category_id' => [
                        'required' => [],
                        'is_all_numeric' => [],
                    ],
                    'subcategory_id' => [
                        'is_all_numeric' => [],
                    ],
                    'feature_id' => [
                        'is_all_numeric' => [],
                    ],
                ],
            ],
        ],
        'article.quantitys.search' => [
            'methods' => ['POST'],
            OptionsSearchCriteriaDTO::class => [
                'rules' => [
                    'select_ref' => [
                        'required' => [],
                        'is_one_of' => ['categorys', 'subcategorys', 'features1', 'features2', 'entrys'],
                    ],
                    'category_id' => [
                        'required' => [],
                        'is_all_numeric' => [],
                    ],
                    'subcategory_id' => [
                        'is_all_numeric' => [],
                    ],
                    'feature1_id' => [
                        'is_all_numeric' => [],
                    ],
                    'feature2_id' => [
                        'is_all_numeric' => [],
                    ],
                    'entrys_number' => [
                        'is_all_numeric' => [],
                    ],
                ],
            ],
        ],



        'article.quantity.add' => [
            'methods' => ['POST'],
            QuantityDTO::class => [
                'rules' => [
                    'id' => [
                        'required' => [],
                        'is_all_numeric' => [],
                    ],
                    'feature1_id' => [
                        'required' => [],
                        'is_all_numeric' => [],
                    ],
                    'feature2_id' => [
                        'required' => [],
                        'is_all_numeric' => [],
                    ],
                    'quantity_price_factor' => [
                        'is_text' => [],
                    ],
                ],
            ],
        ],
        'article.quantity.delete' => [
            'methods' => ['POST'],
            OptionDTO::class => [
                'rules' => [],
            ],
        ],
        'article.quantity.edit' => [
            'methods' => ['POST'],
            OptionDTO::class => [
                'rules' => [
                    'feature_id' => [
                        'required' => [],
                        'is_all_numeric' => [],
                    ],
                    'ref' => [
                        'required' => [],
                        'is_all_alpha_numeric' => [],
                    ],
                    'number' => [
                        'is_all_numeric' => [],
                    ],
                    'price_factor' => [
                        'is_float' => [],
                    ],
                    'pl' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                    'en' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                ],
            ],
        ],
        'article.quantitys.add' => [
            'methods' => ['POST'],
            QuantityDTO::class => [
                'rules' => [
                    'feature1_option_id' => [
                        'required' => [],
                        'is_all_numeric' => [],
                    ],
                    'feature2_option_id' => [
                        'required' => [],
                        'is_all_numeric' => [],
                    ],
                    'quantity_price_factor' => [
                        'required' => [],
                        'is_json_encoded' => [],
                    ],
                ],
            ],
            OptionsSearchCriteriaDTO::class => [
                'rules' => [
                    'category_id' => [
                        'required' => [],
                        'is_all_numeric' => [],
                    ],
                    'subcategory_id' => [
                        'required' => [],
                        'is_all_numeric' => [],
                    ],
                    'feature1_id' => [
                        'required' => [],
                        'is_all_numeric' => [],
                    ],
                    'feature1_id' => [
                        'required' => [],
                        'is_all_numeric' => [],
                    ],
                ],
            ],
        ],
    ],
];
