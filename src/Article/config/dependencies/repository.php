<?php

declare(strict_types=1);

use App\Article\Repository\ArticleRepository;
use App\Article\Repository\CategoryRepository;
use App\Article\Repository\FeatureRepository;
use App\Article\Repository\OptionRepository;
use App\Article\Repository\QuantityRepository;
use App\Article\Repository\SubcategoryRepository;

use App\Article\Repository\Interfaces\ArticleRepositoryInterface;
use App\Article\Repository\Interfaces\CategoryRepositoryInterface;
use App\Article\Repository\Interfaces\FeatureRepositoryInterface;
use App\Article\Repository\Interfaces\OptionRepositoryInterface;
use App\Article\Repository\Interfaces\QuantityRepositoryInterface;
use App\Article\Repository\Interfaces\SubcategoryRepositoryInterface;

use DI\Container;
use Paneric\PdoWrapper\Manager;

return [
    ArticleRepositoryInterface::class => static function (Container $container): ArticleRepository
    {
        return new ArticleRepository($container->get(Manager::class));
    },
    CategoryRepositoryInterface::class => static function (Container $container): CategoryRepository
    {
        return new CategoryRepository($container->get(Manager::class));
    },
    SubcategoryRepositoryInterface::class => static function (Container $container): SubcategoryRepository
    {
        return new SubcategoryRepository($container->get(Manager::class));
    },
    FeatureRepositoryInterface::class => static function (Container $container): FeatureRepository
    {
        return new FeatureRepository($container->get(Manager::class));
    },
    OptionRepositoryInterface::class => static function (Container $container): OptionRepository
    {
        return new OptionRepository($container->get(Manager::class));
    },
    QuantityRepositoryInterface::class => static function (Container $container): QuantityRepository
    {
        return new QuantityRepository($container->get(Manager::class));
    },
];
