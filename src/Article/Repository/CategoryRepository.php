<?php

declare(strict_types=1);

namespace App\Article\Repository;

use App\Article\DTO\RouteAttributesDTO;
use App\Article\Repository\Interfaces\CategoryRepositoryInterface;
use App\Article\DTO\CategoryDTO;
use Paneric\Interfaces\Hydrator\HydratorInterface;
use Paneric\PdoWrapper\Manager;
use PDO;

class CategoryRepository extends PDORepository implements CategoryRepositoryInterface
{
    public function __construct(Manager $manager)
    {
        parent::__construct($manager);

        $this->table = 'articles_categorys';
        $this->dtoClass = CategoryDTO::class;
        $this->fetchMode = PDO::FETCH_CLASS;
    }

    public function findRouteAttributesByRef(string $categoryRef): ?HydratorInterface
    {
        $this->manager->setTable('articles_categorys');
        $this->manager->setDTOClass(RouteAttributesDTO::CLASS);
        $this->manager->setFetchMode(PDO::FETCH_CLASS);

        $result = $this->manager->findBy(['ref' => $categoryRef]);

        if ($result !== false && !empty($result)) {
            return $result[0];
        }

        return null;
    }

    public function findAllAsTiles(): array
    {
        $this->manager->setTable('articles_categorys');
        $this->manager->setDTOClass(CategorDTO::CLASS);
        $this->manager->setFetchMode(PDO::FETCH_CLASS);

        $query = '
            SELECT *
            FROM articles_categorys bt
            WHERE bt.number != \'\'
            ORDER BY bt.number ASC
        ';

        $stmt = $this->manager->setStmt($query);

        return $stmt->fetchAll();
    }
}
