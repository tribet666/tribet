<?php

declare(strict_types=1);

namespace App\Article\Repository\Interfaces;

use Paneric\Interfaces\Hydrator\HydratorInterface;

interface CategoryRepositoryInterface
{
    public function findOneById(int $id): ?object;

    public function findAll();

    public function findRouteAttributesByRef(string $categoryRef): ?HydratorInterface;

    public function findAllAsTiles(): array;

    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null): array;

    public function findByEnhanced(array $criteria, string $operator = 'AND', int $id = null): ?array;

    public function create(HydratorInterface $hydrator): ?array;

    public function update(int $id, HydratorInterface $hydrator): ?array;

    public function remove(int $id): ?array;

    public function getRowsNumber(): int;
}
