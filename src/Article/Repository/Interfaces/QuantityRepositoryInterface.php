<?php

declare(strict_types=1);

namespace App\Article\Repository\Interfaces;

use Paneric\Interfaces\Hydrator\HydratorInterface;

interface QuantityRepositoryInterface
{
    public function findOneById(int $id): ?object;

    public function findAll();

    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null): array;

    public function findByEnhanced(array $criteria, string $operator = 'AND', int $id = null): ?array;

    public function create(HydratorInterface $hydrator): ?array;

    public function createMultiple(array $fieldsSets): ?string;

    public function update(int $id, HydratorInterface $hydrator): ?array;

    public function updateMultiple(array $fieldsSets): ?string;

    public function remove(int $id): ?array;

    public function getRowsNumber(): int;
}
