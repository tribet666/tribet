<?php

declare(strict_types=1);

namespace App\Article\Repository;

use App\Article\Repository\Interfaces\ArticleRepositoryInterface;
use App\Article\DTO\ArticleDTO;
use Paneric\PdoWrapper\Manager;
use PDO;

class ArticleRepository extends PDORepository implements ArticleRepositoryInterface
{
    public function __construct(Manager $manager)
    {
        parent::__construct($manager);

        $this->table = 'articles';
        $this->dtoClass = ArticleDTO::class;
        $this->fetchMode = PDO::FETCH_CLASS;
    }
}
