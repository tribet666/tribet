<?php

declare(strict_types=1);

namespace App\Article\Repository;

use App\Article\Repository\Interfaces\QuantityRepositoryInterface;
use App\Article\DTO\QuantityDTO;
use Paneric\PdoWrapper\Manager;
use PDO;

class QuantityRepository extends PDORepository implements QuantityRepositoryInterface
{
    public function __construct(Manager $manager)
    {
        parent::__construct($manager);

        $this->table = 'articles_quantitys';
        $this->dtoClass = QuantityDTO::class;
        $this->fetchMode = PDO::FETCH_CLASS;
    }
}
