<?php

declare(strict_types=1);

namespace App\Article\Repository;

use App\Article\Repository\Interfaces\FeatureRepositoryInterface;
use App\Article\DTO\FeatureDTO;
use Paneric\PdoWrapper\Manager;
use PDO;

class FeatureRepository extends PDORepository implements FeatureRepositoryInterface
{
    public function __construct(Manager $manager)
    {
        parent::__construct($manager);

        $this->table = 'articles_features';
        $this->dtoClass = FeatureDTO::class;
        $this->fetchMode = PDO::FETCH_CLASS;
    }
}
