<?php

declare(strict_types=1);

namespace App\Article\Repository;

use App\Article\DTO\RouteAttributesDTO;
use App\Article\Repository\Interfaces\SubcategoryRepositoryInterface;
use App\Article\DTO\SubcategoryDTO;
use Paneric\Interfaces\Hydrator\HydratorInterface;
use Paneric\PdoWrapper\Manager;
use PDO;

class SubcategoryRepository extends PDORepository implements SubcategoryRepositoryInterface
{
    public function __construct(Manager $manager)
    {
        parent::__construct($manager);

        $this->table = 'articles_subcategorys';
        $this->dtoClass = SubcategoryDTO::class;
        $this->fetchMode = PDO::FETCH_CLASS;
    }

    public function findRouteAttributesByRef(string $subcategoryRef): ?HydratorInterface
    {
        $this->manager->setTable('articles_subcategorys');
        $this->manager->setDTOClass(RouteAttributesDTO::CLASS);
        $this->manager->setFetchMode(PDO::FETCH_CLASS);

        $result = $this->manager->findBy(['ref' => $subcategoryRef]);

        if ($result !== false && !empty($result)) {
            return $result[0];
        }

        return null;
    }
}
