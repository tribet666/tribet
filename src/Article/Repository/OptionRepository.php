<?php

declare(strict_types=1);

namespace App\Article\Repository;

use App\Article\Repository\Interfaces\OptionRepositoryInterface;
use App\Article\DTO\OptionDTO;
use Paneric\PdoWrapper\Manager;
use PDO;

class OptionRepository extends PDORepository implements OptionRepositoryInterface
{
    public function __construct(Manager $manager)
    {
        parent::__construct($manager);

        $this->table = 'articles_options';
        $this->dtoClass = OptionDTO::class;
        $this->fetchMode = PDO::FETCH_CLASS;
    }
}
