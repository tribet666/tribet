<?php

declare(strict_types=1);

use App\Article\AJAX\Controller\OptionAJAXController;
use App\Article\AJAX\Controller\QuantityAJAXController;
use App\Article\Controller\ArticleController;
use App\Article\Controller\CategoryController;
use App\Article\Controller\QuantityController;
use App\Article\Controller\SubcategoryController;
use App\Article\Controller\FeatureController;
use App\Article\Controller\OptionController;

use DI\DependencyException;
use DI\NotFoundException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Paneric\Middleware\CSRFMiddleware;
use Paneric\Validation\ValidationMiddleware;
use Paneric\Middleware\PaginationMiddleware;

try {

//    $app->get('/article/articles/show[/{page}]', function (Request $request, Response $response) {
//        return $this->get(ArticleController::class)->showAll($response);
//    })->setName('article.articles.show')
//    ->addMiddleware($container->get(PaginationMiddleware::class));
//
//    $app->map(['GET', 'POST'], '/article/article/add', function (Request $request, Response $response) {
//        return $this->get(ArticleController::class)->add($request, $response);
//    })->setName('article.article.add')
//        ->addMiddleware($container->get(ValidationMiddleware::class))
//        ->addMiddleware($container->get(CSRFMiddleware::class));
//
//    $app->map(['GET', 'POST'], '/article/article/{id}/edit', function (Request $request, Response $response, array $args) {
//        return $this->get(ArticleController::class)->edit($request, $response, (int) $args['id']);
//    })->setName('article.article.edit')
//        ->addMiddleware($container->get(ValidationMiddleware::class))
//        ->addMiddleware($container->get(CSRFMiddleware::class));
//
//    $app->map(['GET', 'POST'], '/article/article/{id}/delete', function (Request $request, Response $response, array $args) {
//        return $this->get(ArticleController::class)->delete($request, $response, (int) $args['id']);
//    })->setName('article.article.delete')
//        ->addMiddleware($container->get(ValidationMiddleware::class))
//        ->addMiddleware($container->get(CSRFMiddleware::class));
//
//
//
    $app->get('/article/categorys/show[/{page}]', function (Request $request, Response $response) {
        return $this->get(CategoryController::class)->showAll($response);
    })->setName('article.categorys.show')
        ->addMiddleware($container->get(PaginationMiddleware::class));

    $app->map(['GET', 'POST'], '/article/category/add', function (Request $request, Response $response) {
        return $this->get(CategoryController::class)->add($request, $response);
    })->setName('article.category.add')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class));

    $app->map(['GET', 'POST'], '/article/category/{id}/edit', function (Request $request, Response $response, array $args) {
        return $this->get(CategoryController::class)->edit($request, $response, (int) $args['id']);
    })->setName('article.category.edit')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class));

    $app->map(['GET', 'POST'], '/article/category/{id}/delete', function (Request $request, Response $response, array $args) {
        return $this->get(CategoryController::class)->delete($request, $response, (int) $args['id']);
    })->setName('article.category.delete')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class));



    $app->get('/article/subcategorys/show[/{page}]', function (Request $request, Response $response) {
        return $this->get(SubcategoryController::class)->showAll($response);
    })->setName('article.subcategorys.show')
        ->addMiddleware($container->get(PaginationMiddleware::class));

    $app->map(['GET', 'POST'], '/article/subcategory/add', function (Request $request, Response $response) {
        return $this->get(SubcategoryController::class)->add($request, $response);
    })->setName('article.subcategory.add')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class));

    $app->map(['GET', 'POST'], '/article/subcategory/{id}/edit', function (Request $request, Response $response, array $args) {
        return $this->get(SubcategoryController::class)->edit($request, $response, (int) $args['id']);
    })->setName('article.subcategory.edit')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class));

    $app->map(['GET', 'POST'], '/article/subcategory/{id}/delete', function (Request $request, Response $response, array $args) {
        return $this->get(SubcategoryController::class)->delete($request, $response, (int) $args['id']);
    })->setName('article.subcategory.delete')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class));

    $app->map(['GET', 'POST'], '/article/subcategory/{id}/features/edit', function (Request $request, Response $response, array $args) {
        return $this->get(SubcategoryController::class)->editFeatures($request, $response, (int) $args['id']);
    })->setName('article.subcategory.features.edit')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class));

    $app->map(['GET', 'POST'], '/article/subcategory/{id}/features/delete', function (Request $request, Response $response, array $args) {
        return $this->get(SubcategoryController::class)->deleteFeatures($request, $response, (int) $args['id']);
    })->setName('article.subcategory.features.delete')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class));



    $app->get('/article/features/show[/{page}]', function (Request $request, Response $response) {
        return $this->get(FeatureController::class)->showAll($response);
    })->setName('article.features.show')
        ->addMiddleware($container->get(PaginationMiddleware::class));

    $app->map(['GET', 'POST'], '/article/feature/add', function (Request $request, Response $response) {
        return $this->get(FeatureController::class)->add($request, $response);
    })->setName('article.feature.add')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class));

    $app->map(['GET', 'POST'], '/article/feature/{id}/edit', function (Request $request, Response $response, array $args) {
        return $this->get(FeatureController::class)->edit($request, $response, (int) $args['id']);
    })->setName('article.feature.edit')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class));

    $app->map(['GET', 'POST'], '/article/feature/{id}/delete', function (Request $request, Response $response, array $args) {
        return $this->get(FeatureController::class)->delete($request, $response, (int) $args['id']);
    })->setName('article.feature.delete')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class));



    $app->get('/article/options/show[/{page}]', function (Request $request, Response $response) {
        return $this->get(OptionController::class)->showAll($response);
    })->setName('article.options.show')
        ->addMiddleware($container->get(PaginationMiddleware::class));

    $app->post('/article/options/search/{mode}', function (Request $request, Response $response, array $args) {
        return $this->get(OptionAJAXController::class)->search($request, $response, $args['mode']);
    })->setName('article.options.search')
        ->addMiddleware($container->get(ValidationMiddleware::class));

    $app->map(['GET', 'POST'], '/article/options/edit', function (Request $request, Response $response) {
        return $this->get(OptionController::class)->editMultiple($request, $response);
    })->setName('article.options.edit')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class));

    $app->map(['GET', 'POST'], '/article/option/add', function (Request $request, Response $response) {
        return $this->get(OptionController::class)->add($request, $response);
    })->setName('article.option.add')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class));

    $app->map(['GET', 'POST'], '/article/option/{id}/edit', function (Request $request, Response $response, array $args) {
        return $this->get(OptionController::class)->edit($request, $response, (int) $args['id']);
    })->setName('article.option.edit')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class));

    $app->map(['GET', 'POST'], '/article/option/{id}/delete', function (Request $request, Response $response, array $args) {
        return $this->get(OptionController::class)->delete($request, $response, (int) $args['id']);
    })->setName('article.option.delete')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class));



    $app->get('/article/quantitys/show[/{page}]', function (Request $request, Response $response) {
        return $this->get(QuantityController::class)->showAll($response);
    })->setName('article.quantitys.show')
        ->addMiddleware($container->get(PaginationMiddleware::class));

    $app->post('/article/quantitys/search/{mode}', function (Request $request, Response $response, array $args) {
        return $this->get(QuantityAJAXController::class)->search($request, $response, $args['mode']);
    })->setName('article.quantitys.search')
        ->addMiddleware($container->get(ValidationMiddleware::class));

    $app->map(['GET', 'POST'], '/article/quantitys/add', function (Request $request, Response $response) {
        return $this->get(QuantityController::class)->addMultiple($request, $response);
    })->setName('article.quantitys.add')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class));

    $app->map(['GET', 'POST'], '/article/quantitys/edit', function (Request $request, Response $response) {
        return $this->get(QuantityController::class)->editMultiple($request, $response);
    })->setName('article.quantitys.edit')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class));

    $app->map(['GET', 'POST'], '/article/quantity/add', function (Request $request, Response $response) {
        return $this->get(QuantityController::class)->add($request, $response);
    })->setName('article.quantity.add')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class));

    $app->map(['GET', 'POST'], '/article/quantity/{id}/edit', function (Request $request, Response $response, array $args) {
        return $this->get(QuantityController::class)->edit($request, $response, (int) $args['id']);
    })->setName('article.quantity.edit')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class));

    $app->map(['GET', 'POST'], '/article/quantity/{id}/delete', function (Request $request, Response $response, array $args) {
        return $this->get(QuantityController::class)->delete($request, $response, (int) $args['id']);
    })->setName('article.quantity.delete')
        ->addMiddleware($container->get(ValidationMiddleware::class))
        ->addMiddleware($container->get(CSRFMiddleware::class));




    require_once ROOT_FOLDER . 'src/Article/bootstrap/routes-cross.php';

} catch (DependencyException $e) {
    echo $e->getMessage();
} catch (NotFoundException $e) {
    echo $e->getMessage();
}
