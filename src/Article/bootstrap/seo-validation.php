<?php

declare(strict_types=1);

use App\Article\Service\FeatureService;
use App\Article\Service\SubcategoryService;
use DI\DependencyException;
use DI\NotFoundException;

try {
    $routeRef = $seoAdapter->getRouteRef();

    $subcategoryRouteAttributes = null;

    if ($routeRef !== null) {
        $subcategoryService = $container->get(SubcategoryService::class);
        $subcategoryRouteAttributes = $subcategoryService->getRouteAttributesByRef($routeRef);

        if ($subcategoryRouteAttributes !== null) {
            $featureService = $container->get(FeatureService::class);
            $subcategoryFeatures = $featureService->getBySubcategoryRef($routeRef);

            $container->set(
                'seo-validation',
                $seoAdapter->setArticleValidationSettings($subcategoryFeatures)
            );
        }
    }
} catch (DependencyException $e) {
    echo $e->getMessage();
} catch (NotFoundException $e) {
    echo $e->getMessage();
}
