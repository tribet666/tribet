<?php

declare(strict_types=1);

use App\Article\Controller\ArticleController;
use App\Article\Controller\CategoryController;
use App\Article\Service\CategoryService;
use DI\DependencyException;
use DI\NotFoundException;

try {
    $routeRef = $seoAdapter->getRouteRef();

    if ($routeRef !== null) {

        if ($subcategoryRouteAttributes !== null) {
            $app = $seoAdapter->setPOSTRoutes(
                $app,
                $container,
                ArticleController::class,
                'configure',
                $subcategoryRouteAttributes->convert()
            );
        }

        if ($subcategoryRouteAttributes === null) {

            $categoryService = $container->get(CategoryService::class);
            $categoryRouteAttributes = $categoryService->getRouteAttributesByRef($routeRef);

            if ($categoryRouteAttributes !== null) {
                $app = $seoAdapter->setGETRoutes(
                    $app,
                    CategoryController::class,
                    'showRefferedAsTiles',
                    $categoryRouteAttributes->convert()
                );
            }
        }
    }
} catch (DependencyException $e) {
    echo $e->getMessage();
} catch (NotFoundException $e) {
    echo $e->getMessage();
}
