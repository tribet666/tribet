<?php

declare(strict_types=1);

namespace App\Error\Controller;

use Paneric\Controller\AppController;
use Psr\Http\Message\ResponseInterface as Response;

class ErrorController extends AppController
{
    public function index(Response $response, array $payload): Response
    {
        return $this->render(
            $response,
            '@error/error.html.twig',
            $payload
        );
    }
}
