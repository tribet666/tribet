<?php

declare(strict_types=1);

namespace App\Dashboard\Service;

use Psr\Http\Message\ServerRequestInterface as Request;

class AdminService
{
    public function index(Request $request): void {}
}
