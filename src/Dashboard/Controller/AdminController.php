<?php

declare(strict_types=1);

namespace App\Dashboard\Controller;

use Paneric\Controller\AppController;
use App\Dashboard\Service\AdminService;
use Paneric\Interfaces\Session\SessionInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Twig\Environment as Twig;

class AdminController extends AppController
{
    private $adminService;

    public function __construct(Twig $twig, SessionInterface $session, AdminService $cmsService)
    {
        parent:: __construct($twig, $session);

        $this->adminService = $cmsService;
    }

    public function index(Request $request, Response $response): Response
    {
        $this->adminService->index($request);

        return $this->render(
            $response,
            '@module/index-admin.html.twig'
        );
    }
}
