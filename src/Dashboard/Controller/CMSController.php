<?php

declare(strict_types=1);

namespace App\Dashboard\Controller;

use Paneric\Controller\AppController;
use App\Dashboard\Service\CMSService;
use Paneric\Interfaces\Session\SessionInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Twig\Environment as Twig;

class CMSController extends AppController
{
    private $cmsService;

    public function __construct(Twig $twig, SessionInterface $session, CMSService $cmsService)
    {
        parent:: __construct($twig, $session);

        $this->cmsService = $cmsService;
    }

    public function index(Request $request, Response $response): Response
    {
        $this->cmsService->index($request);

        return $this->render(
            $response,
            '@module/index-cms.html.twig'
        );
    }
}
