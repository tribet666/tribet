<?php

declare(strict_types=1);

namespace App\Dashboard\Controller;

use Paneric\Controller\AppController;
use App\Dashboard\Service\DashboardService;
use Paneric\Interfaces\Session\SessionInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Twig\Environment as Twig;

class DashboardController extends AppController
{
    private $dashboardService;

    public function __construct(Twig $twig, SessionInterface $session, DashboardService $mainService)
    {
        parent:: __construct($twig, $session);

        $this->dashboardService = $mainService;
    }

    public function index(Request $request, Response $response): Response
    {
        $this->dashboardService->index($request);

        return $this->render(
            $response,
            '@module/index-dashboard.html.twig'
        );
    }
}
