<?php

declare(strict_types=1);

$validationModule = [
    'validation' => [
    ],
];

$validationAutheCommon = include ROOT_FOLDER . 'vendor/paneric/authentication/validation-common.php';

$validation['validation'] = array_merge(
    $validationModule['validation'],
    $validationAutheCommon['validation'],
);

return $validation;
