<?php

declare(strict_types=1);

use App\Dashboard\Service\DashboardService;
use App\Dashboard\Service\CMSService;
use App\Dashboard\Service\AdminService;
use DI\Container;

return [
    DashboardService::class => static function (Container $container): DashboardService
    {
        return new DashboardService();
    },
    CMSService::class => static function (Container $container): CMSService
    {
        return new CMSService();
    },
    AdminService::class => static function (Container $container): AdminService
    {
        return new AdminService();
    },
];
