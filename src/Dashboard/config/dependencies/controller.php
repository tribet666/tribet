<?php

declare(strict_types=1);

use App\Dashboard\Controller\DashboardController;
use App\Dashboard\Controller\CMSController;
use App\Dashboard\Controller\AdminController;
use App\Dashboard\Service\DashboardService;
use App\Dashboard\Service\CMSService;
use App\Dashboard\Service\AdminService;
use DI\Container;
use Paneric\Interfaces\Session\SessionInterface;
use Twig\Environment as Twig;

return [
    DashboardController::class => static function (Container $container): DashboardController
    {
        return new DashboardController(
            $container->get(Twig::class),
            $container->get(SessionInterface::class),
            $container->get(DashboardService::class)
        );
    },
    CMSController::class => static function (Container $container): CMSController
    {
        return new CMSController(
            $container->get(Twig::class),
            $container->get(SessionInterface::class),
            $container->get(CMSService::class)
        );
    },
    AdminController::class => static function (Container $container): AdminController
    {
        return new AdminController(
            $container->get(Twig::class),
            $container->get(SessionInterface::class),
            $container->get(AdminService::class)
        );
    },
];
