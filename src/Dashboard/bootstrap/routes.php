<?php

declare(strict_types=1);

require_once ROOT_FOLDER . 'vendor/paneric/authentication/routes.php';
require_once ROOT_FOLDER . 'vendor/paneric/authorization/routes.php';

require_once ROOT_FOLDER . 'src/Article/bootstrap/routes.php';