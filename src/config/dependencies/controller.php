<?php

declare(strict_types=1);

use App\Error\Controller\ErrorController;
use DI\Container;
use Paneric\Interfaces\Session\SessionInterface;
use Twig\Environment as Twig;

return [
    ErrorController::class => static function (Container $container): ErrorController
    {
        return new ErrorController(
            $container->get(Twig::class),
            $container->get(SessionInterface::class)
        );
    },
];
