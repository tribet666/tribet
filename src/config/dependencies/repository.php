<?php

declare(strict_types=1);

use App\Article\Repository\CategoryRepository;
use App\Article\Repository\FeatureRepository;
use App\Article\Repository\Interfaces\FeatureRepositoryInterface;
use App\Article\Repository\SubcategoryRepository;

use App\Article\Repository\Interfaces\CategoryRepositoryInterface;
use App\Article\Repository\Interfaces\SubcategoryRepositoryInterface;

use DI\Container;
use Paneric\PdoWrapper\Manager;

return [
    CategoryRepositoryInterface::class => static function (Container $container): CategoryRepository
    {
        return new CategoryRepository($container->get(Manager::class));
    },
    SubcategoryRepositoryInterface::class => static function (Container $container): SubcategoryRepository
    {
        return new SubcategoryRepository($container->get(Manager::class));
    },
    FeatureRepositoryInterface::class => static function (Container $container): FeatureRepository
    {
        return new FeatureRepository($container->get(Manager::class));
    },
];