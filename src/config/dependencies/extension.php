<?php

declare(strict_types=1);

use Paneric\Twig\Extension\CSRFExtension;
use DI\Container;
use Paneric\Interfaces\Guard\GuardInterface;
use Paneric\Interfaces\Session\SessionInterface;

return [
    CSRFExtension::class => static function (Container $container): CSRFExtension
    {
        return new CSRFExtension(
            $container->get(SessionInterface::class),
            $container->get(GuardInterface::class),
            $container->get('csrf')
        );
    },
];