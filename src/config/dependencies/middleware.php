<?php

declare(strict_types=1);

use Paneric\Interfaces\Session\SessionInterface;
use Paneric\Middleware\AuthenticationMiddleware;
use Paneric\Authorization\AuthorizationMiddleware;
use Paneric\Authorization\PDO\Repository\PrivilegeRepository;
use Paneric\Authorization\PDO\Repository\RoleRepository;
use Paneric\Middleware\CSRFMiddleware;
use DI\Container;
use Paneric\Interfaces\Guard\GuardInterface;
use Paneric\Middleware\PaginationMiddleware;
use Paneric\Validation\ValidationMiddleware;
use Paneric\Validation\Validator;
use Paneric\Validation\ValidatorBuilder;
use Paneric\Middleware\RouteMiddleware;
use Paneric\Session\SessionMiddleware;
use Paneric\Middleware\UriMiddleware;

return [
    RouteMiddleware::class => static function (Container $container): RouteMiddleware
    {
        return new RouteMiddleware($container);
    },

    UriMiddleware::class => static function (Container $container): UriMiddleware
    {
        return new UriMiddleware($container);
    },

    SessionMiddleware::class => static function (Container $container): SessionMiddleware
    {
        return new SessionMiddleware($container);
    },

    AuthenticationMiddleware::class => static function (Container $container): AuthenticationMiddleware
    {
        return new AuthenticationMiddleware($container->get(SessionInterface::class));
    },

    AuthorizationMiddleware::class => static function (Container $container): AuthorizationMiddleware
    {
        return new AuthorizationMiddleware(
            $container->get(RoleRepository::class),
            $container->get(PrivilegeRepository::class),
            $container->get(SessionInterface::class),
        );
    },

    Validator::class => static function (Container $container): Validator
    {
        $validatorBuilder = new ValidatorBuilder();

        return $validatorBuilder->build(
            (string) $container->get('local')
        );
    },

    ValidationMiddleware::class => static function (Container $container): ValidationMiddleware
    {
        $validation = (array) $container->get('validation');

        if ($container->has('seo-validation')) {
            $validation = array_merge($validation, (array) $container->get('seo-validation'));
        }

        return new ValidationMiddleware(
            $container->get(Validator::class),
            $validation
        );
    },

    CSRFMiddleware::class => static function (Container $container): CSRFMiddleware
    {
        $config = $container->get('csrf');

        return new CSRFMiddleware(
            $container->get(SessionInterface::class),
            $container->get(GuardInterface::class),
            $config
        );
    },
    PaginationMiddleware::class => static function (Container $container): PaginationMiddleware
    {
        return new PaginationMiddleware(
            $container
        );
    },
];
