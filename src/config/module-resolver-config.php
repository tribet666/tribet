<?php

return [
    'default_route_key' => 'main',
    'local_map' => ['en', 'pl'],
    'module_map' => [
        'error' => 'Error',
        'main' => 'Main',
        'authe' => 'Authentication',
        'autho' => 'Authorization',
        'dashboard' => 'Dashboard',
        'cms' => 'Dashboard',
        'admin' => 'Dashboard',
        'e-commerce' => 'ECommerce',
        'article' => 'Article',

        'witamy' => 'Article',
        'produkty' => 'Article',
    ],
];
