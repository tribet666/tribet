<?php

use App\Article\Repository\ArticleRepository;
use App\Article\Repository\CategoryRepository;
use App\Article\Repository\FeatureRepository;
use App\Article\Repository\OptionRepository;
use App\Article\Repository\QuantityRepository;
use App\Article\Repository\SubcategoryRepository;
use Paneric\Authentication\PDO\CredentialRepository;
use Paneric\Authorization\PDO\Repository\AccessRepository;
use Paneric\Authorization\PDO\Repository\FieldRepository;
use Paneric\Authorization\PDO\Repository\PrivilegeRepository;
use Paneric\Authorization\PDO\Repository\RoleRepository;

return [

    'main_route_name' => 'main.index',

    'pagination' => [
        'authe.credentials.show' => CredentialRepository::class,

        'autho.accesses.show' => AccessRepository::class,
        'autho.fields.show' => FieldRepository::class,
        'autho.privileges.show' => PrivilegeRepository::class,
        'autho.roles.show' => RoleRepository::class,

        'article.articles.show' => ArticleRepository::class,
        'article.categorys.show' => CategoryRepository::class,
        'article.features.show' => FeatureRepository::class,
        'article.options.show' => OptionRepository::class,
        'article.subcategorys.show' => SubcategoryRepository::class,
        'article.quantitys.show' => QuantityRepository::class,

        'page_rows_number' => 10,
        'links_number' => 2,

        'nav_tag_open' => '<nav><ul class="pagination">',
        'first_tag_open' => '<li class="page-item"><a class="page-link" href="',
        'first_tag_middle' => '">',
        'first_tag_close' => '</a></li>',
        'tag_open' => '<li class="page-item"><a class="page-link" href="',
        'tag_middle' => '">',
        'tag_close' => '</a></li>',
        'current_tag_open' => '<li class="page-item"><a class="page-link-current" href="',
        'current_tag_middle' => '">',
        'current_tag_close' => '<span class="sr-only">(current)</span></a></li>',
        'last_tag_open' => '<li class="page-item"><a class="page-link" href="',
        'last_tag_middle' => '">',
        'last_tag_close' => '</a></li>',
        'nav_tag_close' => '</ul></nav>',
    ],

    'csrf' => [
        'csrf_key_name' => 'csrf_key',
        'csrf_key_length' => 32,
        'csrf_hash_name' => 'csrf_hash',
    ],

    // TODO: DEV update session config:
    'session-wrapper' => [
        'table' => 'sessions',

        'cookie_lifetime' => '0',         // Expire on close (string value!!!)
        'cookie_access' => '/',         // SessionWrapper cookie readable in all folders.
        'domain' => '',                 // '' for localhost, '.exemple.com' for others
        'secure' => false,              // in case of https true
        'js_denied' => true,            // Make sure the session cookie is not accessible via javascript.

        'hash_function' => 'sha512',    // Hash algorithm to use for the session. (use hash_algos() to get a list of available hashes.)
        'hash_bits_per_character' => 6, // How many bits per character of the hash. The possible values are '4' (0-9, a-f), '5' (0-9, a-v), and '6' (0-9, a-z, A-Z, "-", ",").
        'use_only_cookies' => 1,        // Force the session to only use cookies, not URL variables.
        'gc_maxlifetime' => '86400',      // session max lifetime

        'cookie_name' => 'scm',         // session cookie name

        'encrypt' => true,
        'regenerate' => false,          // Adviced true, but somehow ajax searcher does not work with it.
    ],

    // TODO: DEV update pdo-wrapper config:
    'pdo-wrapper' => [
        'limit' => 10,
        'host' => 'localhost',
        'dbName' => 'tribet',
        'charset' => 'utf8',
        'user' => 'toor',
        'password' => 'toor',
        'options' => [
            PDO::ATTR_PERSISTENT         => true,
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_WARNING, //\PDO::ERRMODE_SILENT, \PDO::ERRMODE_WARNING, \PDO::ERRMODE_EXCEPTION
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_CLASS,
            PDO::ATTR_EMULATE_PREPARES   => false,
            PDO::MYSQL_ATTR_FOUND_ROWS   => false, // true -> matched, false -> affected
        ],
    ],

    // TODO: DEV update guard config:
    'guard' => [
        'file_path' => ROOT_FOLDER . 'temp.txt',
        'algo_password' => PASSWORD_BCRYPT,
        'options_algo_password' => [
            'cost' => 10,
        ],
        'algo_hash' => 'sha512',
        'unique_id_prefix' => '',
        'unique_id_more_entropy' => true,
    ],

    // TODO: DEV update twig config:
    'twig-builder-app' => [
        'templates_dirs' => [
            'error' => APP_FOLDER . 'Error/templates/',
            'module' => MODULE_FOLDER . 'templates/',
            'app' => APP_FOLDER . 'templates/',

        ],
        'options' => [
            'debug' => true, /* "prod" false */
            'charset' => 'UTF-8',
            'strict_variables' => false,
            'autoescape' => 'html',
            'cache' => false, /* "prod" ROOT_FOLDER.'/var/cache/twig'*/
            'auto_reload' => null,
            'optimizations' => -1,
        ],
    ],

    // TODO: DEV update recaptcha config:
    'recaptcha-middleware' => [
        'site_key' => '6LceLdsUAAAAALkAvJtzFRk7YR1QZwJjPN5CoxoT',
        'url_api_script' => '<script src="https://www.google.com/recaptcha/api.js?render=%s"></script>',
        'url_site_verify' => 'https://www.google.com/recaptcha/api/siteverify?secret=%s&response=%s',
        'file_path' => ROOT_FOLDER . 'temp2.txt',
        'score' => 0.1,
    ],

    // TODO: COMMON update visitor role_id:
    'credential-service' =>[
        'role_id' => 4,
        'csrf_hash_field_name' => 'csrf_hash',
    ],

    // TODO: DEV update mailer config:
    'mailer' => [
        'smtp_debug' => 0,
        'host' => 'smtp.mailtrap.io',
        'smtp_auth' => true,
        'username' => '27f054dd15af4f',
        'password' => '15974543b4f6d6',
        'smtp_secure' => 'tls',
        'port' => 2525,
        'html' => true,
        'set_from_address' => '5f10887ab7-9afe04@inbox.mailtrap.io',
        'set_from_name' => 'Mailer',
    ],

    // TODO: DEV update mailer-service config:
    'mailer-service' => [
        'random_string_length' => 128,

        'activation_url' => 'http://127.0.0.1:8000/authe',
        'activation_time_delay' => 24,

        'reset_url' => 'http://127.0.0.1:8000/authe',
        'reset_time_delay' => 24,

        'mail_content' => [
            'activation_email_subject' => [
                'en' => 'Account activation automatic message, please do not respond.',
                'pl' => 'Aktywacja konta - wiadomość automatyczna, proszę nie odpowiadać.',
            ],
            'activation_email_message' => [
                'en' => 'Please click the following link within %sh to activate your account: %s/%s/%s/activate .',
                'pl' => 'Kliknij następujący link w ciągu %sh, aby aktywować konto: %s/%s/%s/activate .',
            ],
            'reset_email_subject' => [
                'en' => 'Password reset automatic message.',
                'pl' => 'Reset hasła - wiadomość automatyczna, proszę nie odpowiadać.',
            ],
            'reset_email_message' => [
                'en' => 'Please click the following link within %sh to reset your account password: %s/%s/%s/reset .',
                'pl' => 'Kliknij następujący link w ciągu %sh, aby zresetować hasło: %s/%s/%s/reset .',
            ],
        ],
    ],

];
