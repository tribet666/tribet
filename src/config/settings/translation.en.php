<?php

return [
    'translation_app' => [
        'service_name' => 'Tribet',
        'main' => 'Main page',

        'error' => 'Error',
        'Not found.' => 'Page not found.',

        'log_in' => 'Log in',
        'restore' => 'Restore',
        'log_out' => 'Log out',
        'subscribe' => 'Subscribe',
        'unsubscribe' => 'Unsubscribe',
        'all_rights' => '© 2020 Tribet. All rights reserved.',

        'dashboard' => 'Dashboard',
        'cms' => 'CMS',
        'admin' => 'Admin',
        'credentials' => 'Credentials',
        'e-commerce' => 'e-Commerce',

        'accesses' => 'Accesses',
        'fields' => 'Fields',
        'privileges' => 'Privileges',
        'roles' => 'Roles',

        'articles' => 'Articles',
        'categorys' => 'Categories',
        'features' => 'Features',
        'options' => 'Options',
        'subcategorys' => 'Subcategories',
        'entrys_number' => 'Entries number',
        'quantitys' => 'Quantities',
        'option' => 'Option',
        'price_factor' => 'Price/Factor',
        'quantity_price_factor' => 'Quantity - Price/Factor',
    ],
];
