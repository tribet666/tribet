<?php

return [
    'translation_app' => [
        'service_name' => 'Tribet',
        'main' => 'Strona główna',

        'error' => 'Błąd',
        'Not found.' => 'Nie znaleziono strony.',

        'log_in' => 'Zaloguj się',
        'restore' => 'Odzyskaj',
        'log_out' => 'Wyloguj się',
        'subscribe' => 'Załóż konto',
        'unsubscribe' => 'Usuń konto',
        'all_rights' => '© 2020 Tribet. Wszystkie prawa zastrzeżone.',

        'dashboard' => 'Dashboard',
        'cms' => 'CMS',
        'admin' => 'Admin',
        'credentials' => 'Loginy',
        'e-commerce' => 'e-Commerce',

        'accesses' => 'Dostępy',
        'fields' => 'Pola',
        'privileges' => 'Przywileje',
        'roles' => 'Role',

        'articles' => 'Artykuły',
        'categorys' => 'Kategorie',
        'features' => 'Parametry',
        'options' => 'Opcje',
        'subcategorys' => 'Podkategorie',
        'entrys_number' => 'Liczba wpisów',
        'quantitys' => 'Nakłady',
        'option' => 'Opcja',
        'price_factor' => 'Kwota/Współcz.',
        'quantity_price_factor' => 'Nakład - Kwota/Współcz.',
    ],
];
