<?php

declare(strict_types=1);

namespace App\SEO;

use App\Article\DTO\ArticleDTO;
use Paneric\Middleware\CSRFMiddleware;
use Paneric\Validation\ValidationMiddleware;
use Slim\App;
use PSR\Container\ContainerInterface as Container;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class SEOAdapter
{
    private $requestUri;

    private $routeRef;

    public function __construct(string $requestUri, array $moduleMarkers)
    {
        $this->setRequestUri($requestUri, $moduleMarkers);
    }

    private function setRequestUri(string $requestUri, array $moduleMarkers): void
    {
        foreach ($moduleMarkers as $marker) {
            if (strpos($requestUri, $marker) !== false) {

                $this->requestUri = $requestUri;

                $this->routeRef = str_replace('/', '-', ltrim($requestUri, '//'));

                return;
            }
        }
    }

    public function getRouteRef(): ?string
    {
        return $this->routeRef;
    }

    public function setGETRoutes(
        App $app,
        string $controllerClassName,
        string $method,
        array $routeAttributes = null
    ): App {

        $app->get($this->requestUri, function (
            Request $request,
            Response $response
        ) use (
            $controllerClassName,
            $routeAttributes,
            $method
        ) {
            return $this->get($controllerClassName)->{$method}($response, $routeAttributes['id']);
        })->setName($this->routeRef);

        return $app;
    }

    public function setPOSTRoutes(
        App $app,
        Container $container,
        string $controllerClassName,
        string $method,
        array $routeAttributes
    ): App {
        $app->map(['GET', 'POST'], $this->requestUri, function (
            Request $request,
            Response $response
        ) use (
            $controllerClassName,
            $routeAttributes,
            $method
        ) {
            return $this->get($controllerClassName)->{$method}($request, $response, $routeAttributes['id']);
        })->setName($this->routeRef)
            ->addMiddleware($container->get(ValidationMiddleware::class))
            ->addMiddleware($container->get(CSRFMiddleware::class));

        return $app;
    }

    public function setArticleValidationSettings(array $features): array
    {
        if (!empty($features)) {
            $rules['features'] = [
                'required' => [],
                'is_all_numeric' => [],
            ];
        }

        $rules['quantity'] = [
            'required' => [],
            'is_all_numeric' => [],
        ];

        return [
            $this->routeRef => [
                'methods' => ['POST'],
                ArticleDTO::class => [
                    'rules' => $rules,
                ],
            ],
        ];
    }
}
