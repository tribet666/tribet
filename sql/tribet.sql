-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Czas generowania: 19 Maj 2020, 17:02
-- Wersja serwera: 8.0.20-0ubuntu0.19.10.1
-- Wersja PHP: 7.3.11-0ubuntu0.19.10.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `tribet`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `accesses`
--

CREATE TABLE `accesses` (
  `id` int NOT NULL,
  `level` int NOT NULL,
  `ref` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `info` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)',
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `accesses`
--

INSERT INTO `accesses` (`id`, `level`, `ref`, `info`, `created_at`) VALUES
(1, 0, 'NIL', 'a', '2020-05-05 18:58:18'),
(2, 1, 'CEO', 'a', '2020-05-05 19:24:19'),
(3, 2, 'Internal Audit', 'a', '2020-05-05 19:28:11'),
(4, 31, 'Marketing Director', 'a', '2020-05-05 19:28:30'),
(5, 32, 'Legal Director', 'a', '2020-05-05 19:29:14'),
(6, 33, 'Development Director', 'a', '2020-05-05 19:29:48'),
(7, 34, 'Operations Director', 'a', '2020-05-05 19:30:29'),
(8, 35, 'Financial Director', 'a', '2020-05-05 19:31:46');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `addresses`
--

CREATE TABLE `addresses` (
  `id` int UNSIGNED NOT NULL,
  `id_company` int UNSIGNED NOT NULL,
  `designation` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `country` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `region` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `province` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `zip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `city` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `street` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `number` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `box` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `articles`
--

CREATE TABLE `articles` (
  `id` int UNSIGNED NOT NULL,
  `ref` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `category_id` int UNSIGNED NOT NULL,
  `subcategory_id` int UNSIGNED NOT NULL,
  `corner_id` int UNSIGNED NOT NULL,
  `fixing_id` int UNSIGNED NOT NULL,
  `format_id` int UNSIGNED NOT NULL,
  `elastic_id` int UNSIGNED NOT NULL,
  `handle_id` int UNSIGNED NOT NULL,
  `insert_id` int UNSIGNED NOT NULL,
  `overprint_id` int UNSIGNED NOT NULL,
  `paper_id` int UNSIGNED NOT NULL,
  `page_id` int UNSIGNED NOT NULL,
  `refinement_id` int UNSIGNED NOT NULL,
  `ridge_id` int UNSIGNED NOT NULL,
  `sample_id` int UNSIGNED NOT NULL,
  `info` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `articles_categorys`
--

CREATE TABLE `articles_categorys` (
  `id` int UNSIGNED NOT NULL,
  `ref` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `number` int UNSIGNED NOT NULL,
  `label_pl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `label_en` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `info_pl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'info_pl',
  `info_en` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'info_en',
  `description_pl` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description_en` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `articles_categorys`
--

INSERT INTO `articles_categorys` (`id`, `ref`, `number`, `label_pl`, `label_en`, `info_pl`, `info_en`, `description_pl`, `description_en`, `created_at`) VALUES
(2, 'produkty-torby-papierowe', 1, 'torby_papierowe_pl', 'torby_papierowe_en', 'info_pl', 'info_en', '', '', '2020-05-08 11:23:39'),
(3, 'produkty-teczki', 3, 'teczki_pl', 'teczki_en', 'info_pl', 'info_en', '', '', '2020-05-08 11:23:56'),
(4, 'produkty-katalogi', 4, 'katalogi_pl', 'katalogi_en', 'info_pl', 'info_en', '', '', '2020-05-08 11:24:19'),
(5, 'produkty-ulotki', 5, 'ulotki_pl', 'ulotki_en', 'info_pl', 'info_en', '', '', '2020-05-08 11:25:12'),
(6, 'produkty-notesy', 6, 'notesy_pl', 'notesy_en', 'info_pl', 'info_en', '', '', '2020-05-08 11:25:18'),
(7, 'produkty-wizytowki', 7, 'wizytowki_pl', 'wizytowki_en', 'info_pl', 'info_en', '', '', '2020-05-08 11:25:40'),
(8, 'produkty-kalendarze', 8, 'kalendarze_pl', 'kalendarze_en', 'info_plpl', 'info_enen', 'description_pl', 'description_en', '2020-05-08 11:25:58'),
(9, 'produkty-torby-ekologiczne', 2, 'torby_ekologiczne_pl', 'torby_ekologiczne_en', 'info_pl', 'info_en', '', '', '2020-05-12 17:50:21');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `articles_features`
--

CREATE TABLE `articles_features` (
  `id` int UNSIGNED NOT NULL,
  `ref` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `pl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `en` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `articles_features`
--

INSERT INTO `articles_features` (`id`, `ref`, `pl`, `en`, `created_at`) VALUES
(1, 'narozniki', 'narozniki_pl', 'narozniki_en', '2020-05-08 19:15:54'),
(2, 'gumki', 'gumki_pl', 'gumki_en', '2020-05-15 14:25:50'),
(3, 'mocowania', 'mocowania_pl', 'mocowania_en', '2020-05-15 14:29:53'),
(4, 'wymiary', 'wymiary_pl', 'wymiary_en', '2020-05-15 14:29:53'),
(5, 'uchwyty', 'uchwyty_pl', 'uchwyty_en', '2020-05-15 14:31:17'),
(6, 'wklady', 'wklady_pl', 'wklady_en', '2020-05-15 14:31:17'),
(7, 'nadruki', 'nadruki_pl', 'nadruki_en', '2020-05-15 14:32:07'),
(8, 'strony', 'strony_pl', 'strony_en', '2020-05-15 14:32:07'),
(9, 'papiery', 'papiery_pl', 'papiery_en', '2020-05-15 14:33:05'),
(10, 'uszlachetnienia', 'uszlachetnienia_pl', 'uszlachetnienia_en', '2020-05-15 14:33:05'),
(11, 'grzbiety', 'grzbiety_pl', 'grzbiety_en', '2020-05-15 14:33:55'),
(12, 'wzory', 'wzory_pl', 'wzory_en', '2020-05-15 14:33:55');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `articles_options`
--

CREATE TABLE `articles_options` (
  `id` int UNSIGNED NOT NULL,
  `ref` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `feature_id` int UNSIGNED NOT NULL,
  `pl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `en` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `articles_options`
--

INSERT INTO `articles_options` (`id`, `ref`, `feature_id`, `pl`, `en`, `created_at`) VALUES
(1, 'prostokatne', 1, 'prostokatne_pl', 'prostokatne_en', '2020-05-15 14:16:27'),
(2, 'zaokraglone', 1, 'zaokraglone_pl', 'zaokraglone_en', '2020-05-15 14:16:27'),
(4, 'nie', 2, 'nie_pl', 'nie_en', '2020-05-15 14:16:51'),
(5, 'biala', 2, 'biala_pl', 'biala_en', '2020-05-15 14:16:51'),
(6, 'czarna', 2, 'czarna_pl', 'czarna_pl', '2020-05-15 14:16:51'),
(7, 'zielona', 2, 'zielona_pl', 'zielona_en', '2020-05-15 14:16:51'),
(8, 'czerwona', 2, 'czerwona_pl', 'czerwona_en', '2020-05-15 14:16:51'),
(9, 'niebieska', 2, 'niebieska_pl', 'niebieska_en', '2020-05-15 14:16:51'),
(11, 'zszywka_standard', 3, 'zszywka_standard_pl', 'zszywka_standard_en', '2020-05-15 14:39:00'),
(12, 'zszywka_oczko', 3, 'zszywka_oczko_pl', 'zszywka_oczko_en', '2020-05-15 14:39:00'),
(13, 'klej_PUR', 3, 'klej_PUR_pl', 'klej_PUR_pl', '2020-05-15 14:39:00'),
(14, '16x7x22', 4, '16x7x22_pl', '16x7x22_en', '2020-05-15 14:39:46'),
(15, '16x7x24', 4, '16x7x24_pl', '16x7x24_en', '2020-05-15 14:39:46'),
(16, '24x9x15', 4, '24x9x15_pl', '24x9x15_en', '2020-05-15 14:39:46'),
(17, '18x10x22', 4, '18x10x22_pl', '18x10x22_en', '2020-05-15 14:39:46'),
(18, '18x10x25', 4, '18x10x25_pl', '18x10x25_en', '2020-05-15 14:39:46'),
(19, '24x9x24', 4, '24x9x24_pl', '24x9x24_en', '2020-05-15 14:39:46'),
(20, '18x16x36', 4, '18x16x36_pl', '18x16x36_en', '2020-05-15 14:39:46'),
(21, '24x9x30', 4, '24x9x30_pl', '24x9x30_en', '2020-05-15 14:39:46'),
(22, '24x9x32', 4, '24x9x32_pl', '24x9x32_en', '2020-05-15 14:39:46'),
(23, '24x9x36', 4, '24x9x36_pl', '24x9x36_en', '2020-05-15 14:39:46'),
(24, '24x10x32', 4, '24x10x32_pl', '24x10x32_en', '2020-05-15 14:39:46'),
(25, '24x10x35', 4, '24x10x35_pl', '24x10x35_en', '2020-05-15 14:39:46'),
(26, '24x10x37', 4, '24x10x37_pl', '24x10x37_en', '2020-05-15 14:39:46'),
(27, '35x10x24', 4, '35x10x24_pl', '35x10x24_en', '2020-05-15 14:39:46'),
(28, '31x15x22', 4, '31x15x22_pl', '31x15x22_pl', '2020-05-15 14:39:46'),
(29, '32x12x40', 4, '32x12x40_pl', '32x12x40_en', '2020-05-15 14:39:46'),
(30, '32x12x42', 4, '32x12x42_pl', '32x12x42_en', '2020-05-15 14:39:46'),
(31, '42x12x32', 4, '42x12x32_pl', '42x12x32_en', '2020-05-15 14:39:46'),
(32, '35x10x35', 4, '35x10x35_pl', '35x10x35_en', '2020-05-15 14:39:46'),
(33, '35x12x30', 4, '35x12x30_pl', '35x12x30_en', '2020-05-15 14:39:46'),
(34, '48x12x38', 4, '48x12x38_pl', '48x12x38_en', '2020-05-15 14:39:46'),
(35, '48x18x35', 4, '48x18x35_pl', '48x18x35_en', '2020-05-15 14:39:46'),
(36, '30x27x50', 4, '30x27x50_pl', '30x27x50_en', '2020-05-15 14:39:46'),
(37, '11x10x37', 4, '11x10x37_pl', '11x10x37_en', '2020-05-15 14:39:46'),
(38, '13x11x27', 4, '13x11x27_pl', '13x11x27_en', '2020-05-15 14:39:46'),
(39, '13x11x34', 4, '13x11x34_pl', '13x11x34_en', '2020-05-15 14:39:46'),
(40, '18x8x22', 4, '18x8x22_pl', '18x8x22_en', '2020-05-15 14:39:46'),
(41, '25x15x32', 4, '25x15x32_pl', '25x15x32_en', '2020-05-15 14:39:46'),
(42, '30x17x34', 4, '30x17x34_pl', '30x17x34_en', '2020-05-15 14:39:46'),
(43, '34x20x33', 4, '34x20x33_pl', '34x20x33_en', '2020-05-15 14:39:46'),
(44, '40x18x39', 4, '40x18x39_pl', '40x18x39_en', '2020-05-15 14:39:46'),
(45, '50x18x39', 4, '50x18x39_pl', '50x18x39_en', '2020-05-15 14:39:46'),
(46, '16x8x39', 4, '16x8x39_pl', '16x8x39_en', '2020-05-15 14:39:46'),
(47, '148x210', 4, '148x210_pl', '148x210_en', '2020-05-15 14:39:46'),
(48, '210x148', 4, '210x148_pl', '210x148_en', '2020-05-15 14:39:46'),
(49, '210x297', 4, '210x297_pl', '210x297_en', '2020-05-15 14:39:46'),
(50, '297x210', 4, '297x210_pl', '297x210_en', '2020-05-15 14:39:46'),
(51, '99x210', 4, '99x210_pl', '99x210_en', '2020-05-15 14:39:46'),
(52, '120x120', 4, '120x120_pl', '120x120_en', '2020-05-15 14:39:46'),
(53, '210x210', 4, '210x210_pl', '210x210_en', '2020-05-15 14:39:46'),
(54, '105x148', 4, '105x148_pl', '105x148_en', '2020-05-15 14:39:46'),
(55, '297x420', 4, '297x420_pl', '297x420_en', '2020-05-15 14:39:46'),
(56, '85x55', 4, '85x55_pl', '85x55_en', '2020-05-15 14:39:46'),
(57, '90x50', 4, '90x50_pl', '90x50_en', '2020-05-15 14:39:46'),
(58, '24x9x34', 4, '24x9x34_pl', '24x9x34_en', '2020-05-15 14:39:46'),
(77, 'sznurek', 5, 'sznurek_pl', 'sznurek_en', '2020-05-15 14:41:14'),
(78, 'wstazka', 5, 'wstazka_pl', 'wstazka_en', '2020-05-15 14:41:14'),
(79, 'wciecie', 5, 'wciecie_pl', 'wciecie_en', '2020-05-15 14:41:14'),
(80, 'papier_skrecony_brazowy', 5, 'papier_skrecony_brazowy_pl', 'papier_skrecony_brazowy_en', '2020-05-15 14:41:14'),
(86, 'nie_insert', 6, 'nie_insert_pl', 'nie_insert_en', '2020-05-15 14:48:07'),
(87, 'tak_insert', 6, 'tak_insert_pl', 'tak_insert_en', '2020-05-15 14:48:07'),
(89, 'pelny_kolor_CMYK', 7, 'pelny_kolor_CMYK_pl', 'pelny_kolor_CMYK_en', '2020-05-15 14:49:03'),
(90, 'pelny_kolor_CMYK_BIALY', 7, 'pelny_kolor_CMYK_BIALY_pl', 'pelny_kolor_CMYK_BIALY_en', '2020-05-15 14:49:03'),
(91, '1_kolor_PANTONE', 7, '1_kolor_PANTONE_pl', '1_kolor_PANTONE_en', '2020-05-15 14:49:03'),
(92, '2_kolory_PANTONE', 7, '2_kolory_PANTONE_pl', '2_kolory_PANTONE_en', '2020-05-15 14:49:03'),
(93, '1_kolor_BIALY', 7, '1_kolor_BIALY_pl', '1_kolor_BIALY_en', '2020-05-15 14:49:03'),
(94, '1_kolor_jednostronnie', 7, '1_kolor_jednostronnie_pl', '1_kolor_jednostronnie_en', '2020-05-15 14:49:03'),
(95, '2_kolory_jednostronnie', 7, '2_kolory_jednostronnie_pl', '2_kolory_jednostronnie_en', '2020-05-15 14:49:03'),
(96, '1_kolor_dwustronnie', 7, '1_kolor_dwustronnie_pl', '1_kolor_dwustronnie_en', '2020-05-15 14:49:03'),
(97, '2_kolory_dwustronnie', 7, '2_kolory_dwustronnie_pl', '2_kolory_dwustronnie_en', '2020-05-15 14:49:03'),
(98, 'pelny_kolor_CMYK_jednostronnie', 7, 'pelny_kolor_CMYK jednostronnie_pl', 'pelny_kolor_CMYK jednostronnie_en', '2020-05-15 14:49:03'),
(99, 'pelny_kolor_CMYK_dwustronnie', 7, 'pelny_kolor_CMYK dwustronnie_pl', 'pelny_kolor_CMYK dwustronnie_en', '2020-05-15 14:49:03'),
(100, 'okladka_4_4_wnetrze_4_4', 7, 'okladka_4_4_wnetrze 4_4_pl', 'okladka_4_4_wnetrze 4_4_en', '2020-05-15 14:49:03'),
(101, 'okladka_4_4_wnetrze_1_1_czarny', 7, 'okladka_4_4_wnetrze_1_1_czarny_pl', 'okladka_4_4_wnetrze_1 1_czarny_en', '2020-05-15 14:49:03'),
(104, '4+4', 8, '4+4', '4+4', '2020-05-15 14:50:52'),
(105, '4+8', 8, '4+8', '4+8', '2020-05-15 14:50:52'),
(106, '4+12', 8, '4+12', '4+12', '2020-05-15 14:50:52'),
(107, '4+16', 8, '4+16', '4+16', '2020-05-15 14:50:52'),
(108, '4+20', 8, '4+20', '4+20', '2020-05-15 14:50:52'),
(109, '4+24', 8, '4+24', '4+24', '2020-05-15 14:50:52'),
(110, '4+28', 8, '4+28', '4+28', '2020-05-15 14:50:52'),
(111, '4+32', 8, '4+32', '4+32', '2020-05-15 14:50:52'),
(112, '4+36', 8, '4+36', '4+36', '2020-05-15 14:50:52'),
(113, '4+40', 8, '4+40', '4+40', '2020-05-15 14:50:52'),
(114, '4+44', 8, '4+44', '4+44', '2020-05-15 14:50:52'),
(115, '4+48', 8, '4+48', '4+48', '2020-05-15 14:50:52'),
(119, 'kreda_mat_170', 9, 'kreda_mat_170_pl', 'kreda_mat_170_en', '2020-05-15 14:52:12'),
(120, 'kreda_mat_250', 9, 'kreda_mat_250_pl', 'kreda_mat_250_en', '2020-05-15 14:52:12'),
(121, 'kraft_brazowy_gladki_170', 9, 'kraft_brazowy_gladki_170_pl', 'kraft_brazowy_gladki_170_en', '2020-05-15 14:52:12'),
(122, 'kraft_bialy_gladki_140', 9, 'kraft_bialy_gladki_140_pl', 'kraft_bialy_gladki_140_en', '2020-05-15 14:52:12'),
(123, 'karton_220', 9, 'karton_220_pl', 'karton_220_en', '2020-05-15 14:52:12'),
(124, 'offset_190', 9, 'offset_190_pl', 'offset_190_en', '2020-05-15 14:52:12'),
(125, 'kraft_brazowy_100', 9, 'kraft_brazowy_100_pl', 'kraft_brazowy_100_en', '2020-05-15 14:52:12'),
(126, 'kraft_bialy_100', 9, 'kraft_bialy_100_pl', 'kraft_bialy_100_en', '2020-05-15 14:52:12'),
(127, 'kreda_mat_350', 9, 'kreda_mat_350_pl', 'kreda_mat_350_en', '2020-05-15 14:52:12'),
(128, 'kraft_brazowy_gladki_300', 9, 'kraft_brazowy_gladki_300_pl', 'kraft_brazowy_gladki_300_en', '2020-05-15 14:52:12'),
(129, 'offset_90', 9, 'offset_90_pl', 'offset_90_en', '2020-05-15 14:52:12'),
(130, 'kreda_blysk_130', 9, 'kreda_blysk_130_pl', 'kreda_blysk_130_en', '2020-05-15 14:52:12'),
(131, 'kreda_mat_130', 9, 'kreda_mat_130_pl', 'kreda_mat_130_en', '2020-05-15 14:52:12'),
(132, 'kreda_blysk_170', 9, 'kreda_blysk_170_pl', 'kreda_blysk_170_en', '2020-05-15 14:52:12'),
(133, 'kreda_blysk_250', 9, 'kreda_blysk_250_pl', 'kreda_blysk_250_en', '2020-05-15 14:52:12'),
(134, 'kreda_blysk_okladka_250_offset_wnetrze_90', 9, 'kreda_blysk_okladka 250_offset_wnetrze_90_pl', 'kreda_blysk_okladka 250_offset_wnetrze_90_en', '2020-05-15 14:52:12'),
(135, 'kreda_mat_okladka_250_offset_wnetrze_90', 9, 'kreda_mat_okladka 250_offset_wnetrze_90_pl', 'kreda_mat_okladka 250_offset_wnetrze_90_en', '2020-05-15 14:52:12'),
(136, 'kreda_błysk_okladka_250_wnetrze_130', 9, 'kreda_błysk_okladka 250_wnetrze_130_pl', 'kreda_błysk_okladka 250_wnetrze_130_en', '2020-05-15 14:52:12'),
(137, 'kreda_mat_okladka_250_wnetrze_130', 9, 'kreda_mat_okladka 250_wnetrze_130_pl', 'kreda_mat_okladka 250_wnetrze_130_en', '2020-05-15 14:52:12'),
(138, 'kreda_blysk_okladka_250_wnetrze_170', 9, 'kreda_blysk_okladka 250_wnetrze_170_pl', 'kreda_blysk_okladka 250_wnetrze_170_en', '2020-05-15 14:52:12'),
(139, 'kreda_mat_okladka_250_wnetrze_170', 9, 'kreda_mat_okladka 250_wnetrze_170_pl', 'kreda_mat_okladka 250_wnetrze_170_en', '2020-05-15 14:52:12'),
(140, 'kreda_blysk_okladka_250_wnetrze_130', 9, 'kreda_blysk_okladka 250_wnetrze_130_pl', 'kreda_blysk_okladka 250_wnetrze_130_en', '2020-05-15 14:52:12'),
(141, 'papier_gladki_290_szarobrazowy', 9, 'papier_gladki_290 szarobrazowy_pl', 'papier_gladki_290 szarobrazowy_en', '2020-05-15 14:52:12'),
(142, 'papier_gladki_granatowy_290', 9, 'papier_gladki_granatowy 290_pl', 'papier_gladki_granatowy 290_en', '2020-05-15 14:52:12'),
(143, 'papier_gladki_czerwony_290', 9, 'papier_gladki_czerwony 290_pl', 'papier_gladki_czerwony 290_en', '2020-05-15 14:52:12'),
(144, 'papier_gladki_czarny_290', 9, 'papier_gladki_czarny 290_pl', 'papier_gladki_czarny 290_en', '2020-05-15 14:52:12'),
(145, 'papier_gladki_szarobrazowy_290', 9, 'papier_gladki_szarobrazowy 290_pl', 'papier_gladki_szarobrazowy 290_en', '2020-05-15 14:52:12'),
(146, 'papier_syntetyczny_bialy_490', 9, 'papier_syntetyczny bialy_490_pl', 'papier_syntetyczny bialy_490_en', '2020-05-15 14:52:12'),
(147, 'kraft_gladki_bezowy_250', 9, 'kraft_gladki_bezowy_250_pl', 'kraft_gladki_bezowy_250_en', '2020-05-15 14:52:12'),
(150, 'folia_blysk', 10, 'folia_blysk_pl', 'folia_blysk_en', '2020-05-15 14:54:40'),
(151, 'folia_mat_hot-stamping_dwustronnie', 10, 'folia_mat_hot-stamping_dwustronnie_pl', 'folia_mat_hot-stamping_dwustronnie_en', '2020-05-15 14:54:40'),
(152, 'folia_mat', 10, 'folia_mat_pl', 'folia_mat_en', '2020-05-15 14:54:40'),
(153, 'folia_soft-touch', 10, 'folia_soft-touch_pl', 'folia_soft-touch_en', '2020-05-15 14:54:40'),
(154, 'folia_mat_lakier_UV', 10, 'folia_mat_lakier_UV_pl', 'folia_mat_lakier_UV_en', '2020-05-15 14:54:40'),
(155, 'brak', 10, 'brak_pl', 'brak_en', '2020-05-15 14:54:40'),
(156, 'folia_mat_lakier_UV_wybiorczy', 10, 'folia_mat_lakier_UV_wybiorczy_pl', 'folia_mat_lakier_UV_wybiorczy_en', '2020-05-15 14:54:40'),
(157, 'folia_mat_hot-stamping', 10, 'folia_mat_hot-stamping_pl', 'folia_mat_hot-stamping_en', '2020-05-15 14:54:40'),
(158, 'lakier_UV_wybiorczy', 10, 'lakier_UV_wybiorczy_pl', 'lakier_UV_wybiorczy_en', '2020-05-15 14:54:40'),
(159, 'folia_soft-touch_lakier_UV_wybiorczy', 10, 'folia_soft-touch_lakier_UV_wybiorczy_pl', 'folia_soft-touch_lakier_UV_wybiorczy_pl', '2020-05-15 14:54:40'),
(160, 'folia_mat_1_0', 10, 'folia_mat_1_0_pl', 'folia_mat_1_0_en', '2020-05-15 14:54:40'),
(161, 'folia_mat_1_1', 10, 'folia_mat_1_1_pl', 'folia_mat_1_1_pl', '2020-05-15 14:54:40'),
(162, 'folia_mat_1_1_lakier UV_wybiorczy', 10, 'folia_mat_1_1_lakier UV_wybiorczy_pl', 'folia_mat_1_1_lakier UV_wybiorczy_pl', '2020-05-15 14:54:40'),
(163, 'folia_blysk_1_0', 10, 'folia_blysk_1_0_pl', 'folia_blysk_1_0_en', '2020-05-15 14:54:40'),
(164, 'folia_blysk_1_1', 10, 'folia_blysk_1_1_pl', 'folia_blysk_1_1_en', '2020-05-15 14:54:40'),
(165, 'folia_soft-touch_1_0', 10, 'folia_soft-touch_1_0_pl', 'folia_soft-touch_1_0_en', '2020-05-15 14:54:40'),
(166, 'folia_soft-touch_1_1', 10, 'folia_soft-touch_1_1_pl', 'folia_soft-touch_1_1_en', '2020-05-15 14:54:40'),
(182, 'brak_ridges', 11, 'brak_ridges_pl', 'brak_ridges_en', '2020-05-15 14:57:18'),
(183, '5mm', 11, '5mm', '5mm', '2020-05-15 14:57:18'),
(184, '10mm', 11, '10mm', '10mm', '2020-05-15 14:57:18'),
(185, '1', 12, '1_pl', '1_en', '2020-05-15 14:57:57'),
(186, '2', 12, '2_pl', '2_en', '2020-05-15 14:57:57');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `articles_subcategorys`
--

CREATE TABLE `articles_subcategorys` (
  `id` int UNSIGNED NOT NULL,
  `ref` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `number` int UNSIGNED NOT NULL,
  `category_id` int UNSIGNED NOT NULL DEFAULT '0',
  `label_pl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `label_en` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `info_pl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'info_pl',
  `info_en` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'info_en',
  `description_pl` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description_en` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `articles_subcategorys`
--

INSERT INTO `articles_subcategorys` (`id`, `ref`, `number`, `category_id`, `label_pl`, `label_en`, `info_pl`, `info_en`, `description_pl`, `description_en`, `created_at`) VALUES
(6, 'produkty-torby-papierowe-laminowane', 1, 2, 'laminowane_pl', 'laminowane_en', 'info_pl', 'info_en', '', '', '2020-05-08 11:31:21'),
(7, 'produkty-torby-papierowe-kraftowe', 2, 2, 'kraftowe_pl', 'kraftowe_en', 'info_pl', 'info_en', '', '', '2020-05-08 11:31:33'),
(8, 'produkty-torby-papierowe-kartonowe', 3, 2, 'kartonowe_pl', 'kartonowe_en', 'info_pl', 'info_en', '', '', '2020-05-08 11:31:45'),
(9, 'produkty-torby-ekologiczne-brazowe', 1, 9, 'brazowe_pl', 'brazowe_en', 'info_pl', 'info_en', '', '', '2020-05-08 11:32:21'),
(10, 'produkty-torby-ekologiczne-biale', 2, 9, 'biale_pl', 'biale_pl', 'info_pl', 'info_en', '', '', '2020-05-08 11:32:27'),
(11, 'produkty-torby-ekologiczne-kolorowe', 3, 9, 'kolorowe_cmyk_pl', 'kolorowe_cmyk_en', 'info_pl', 'info_en', '', '', '2020-05-08 11:32:59'),
(12, 'produkty-teczki-jednobigowe', 1, 3, 'jednobigowe_pl', 'jednobigowe_en', 'info_pl', 'info_en', '', '', '2020-05-08 11:36:29'),
(13, 'produkty-teczki-jednobigowe-z-wizytowka', 2, 3, 'jednobigowe_wizytowka_pl', 'jednobigowe_wizytowka_en', 'info_pl', 'info_en', '', '', '2020-05-08 11:36:56'),
(14, 'produkty-teczki-dwubigowe', 3, 3, 'dwubigowe_pl', 'dwubigowe_en', 'info_pl', 'info_en', '', '', '2020-05-08 11:37:08'),
(15, 'produkty-teczki-dwubigowe-z-wizytowka', 4, 3, 'dwubigowe_wizytowka_pl', 'dwubigowe_wizytowka_pl', 'info_pl', 'info_en', '', '', '2020-05-08 11:37:23'),
(16, 'produkty-teczki-dwubigowe-z-gumka', 5, 3, 'dwubigowe_gumka_pl', 'dwubigowe_gumka_pl', 'info_pl', 'info_en', '', '', '2020-05-08 11:37:51'),
(17, 'produkty-teczki-dwubigowe-z-wizytowka-i-gumka', 6, 3, 'dwubigowe_wizytowka gumka_pl', 'dwubigowe_wizytowka gumka_en', 'info_pl', 'info_en', '', '', '2020-05-08 11:38:18'),
(18, 'produkty-katalogi-szyte', 1, 4, 'szyte_pl', 'szyte_en', 'info_pl', 'info_en', '', '', '2020-05-08 11:39:26'),
(19, 'produkty-katalogi-klejone', 2, 4, 'klejone_pl', 'klejone_en', 'info_pl', 'info_en', '', '', '2020-05-08 11:39:34'),
(20, 'produkty-katalogi-spiralowane', 3, 4, 'spiralowane_pl', 'spiralowane_en', 'info_pl', 'info_en', '', '', '2020-05-08 11:39:44'),
(21, 'produkty-ulotki-jednoczesciowe', 1, 5, 'jednoczesciowe_pl', 'jednoczesciowe_en', 'info_pl', 'info_en', '', '', '2020-05-08 11:40:51'),
(22, 'produkty-ulotki-dwuczesciowe', 2, 5, 'dwuczesciowe_pl', 'dwuczesciowe_en', 'info_pl', 'info_en', '', '', '2020-05-08 11:41:00'),
(23, 'produkty-ulotki-trzyczesciowe', 3, 5, 'trzyczesciowe_pl', 'trzyczesciowe_en', 'info_pl', 'info_en', '', '', '2020-05-08 11:41:08'),
(25, 'produkty-kalendarze-trojdzielne', 1, 8, 'trojdzielne_pl', 'trojdzielne_en', 'info_pl', 'info_en', '', '', '2020-05-08 11:55:08'),
(26, 'produkty-kalendarze-trojdzielne-spiralowane', 2, 8, 'trojdzielne_spiralowane_pl', 'trojdzielne_spiralowane_en', 'info_pl', 'info_en', '', '', '2020-05-08 11:55:34'),
(27, 'produkty-kalendarze-dwudzielne', 3, 8, 'dwudzielne_pl', 'dwudzielne_en', 'info_pl', 'info_en', '', '', '2020-05-08 11:55:52'),
(28, 'produkty-kalendarze-jednodzielne', 4, 8, 'jednodzielne_pl', 'jednodzielne_en', 'info_pl', 'info_en', '', '', '2020-05-08 11:56:09'),
(29, 'produkty-kalendarze-plaskie-z-okienkiem', 5, 8, 'plaskie_okienkiem_pl', 'plaskie_okienkiem_en', 'info_pl', 'info_en', '', '', '2020-05-08 11:56:30'),
(30, 'produkty-kalendarze-biurkowe-stojace', 6, 8, 'biurkowe_stojace_pl', 'biurkowe_stojace_en', 'info_pl', 'info_en', '', '', '2020-05-08 11:57:06'),
(31, 'produkty-kalendarze-biurkowe-stojace-spiralowane', 7, 8, 'biurkowe_stojace_spiralowane_pl', 'biurkowe_stojace_spiralowane_en', 'info_pl', 'info_en', '', '', '2020-05-08 11:57:25'),
(32, 'produkty-kalendarze-biuwarowe', 8, 8, 'biuwarowe_pl', 'biuwarowe_en', 'info_pl', 'info_en', '', '', '2020-05-08 11:57:51'),
(33, 'produkty-kalendarze-piornikowe', 9, 8, 'piornikowe_pl', 'piornikowe_en', 'info_pl', 'info_en', '', '', '2020-05-08 11:58:08'),
(34, 'produkty-kalendarze-listkowe', 10, 8, 'listkowe_pl', 'listkowe_en', 'info_pl', 'info_en', '', '', '2020-05-08 11:58:24'),
(35, 'produkty-kalendarze-scienne-listwowane', 11, 8, 'scienne_listwowane_pl', 'scienne_listwowane_en', 'info_pl', 'info_en', '', '', '2020-05-08 11:58:52'),
(36, 'produkty-kalendarze-scienne-spiralowane', 12, 8, 'scienne_spiralowane_pl', 'scienne_spiralowane_en', 'info_pl', 'info_en', '', '', '2020-05-08 11:59:23'),
(37, 'produkty-kalendarze-scienne-planery', 13, 8, 'scienne_planery_pl', 'scienne_planery_en', 'info_pl', 'info_en', '', '', '2020-05-08 11:59:48'),
(38, 'produkty-kalendarze-ksiazkowe', 14, 8, 'ksiazkowe_pl', 'ksiazkowe_en', 'info_pl', 'info_en', '', '', '2020-05-08 12:00:07'),
(42, 'produkty-notesy-klejone-z-okladka', 2, 6, 'klejone_n_okladka_pl', 'klejone_n_okladka_en', 'info_pl', 'info_en', '', '', '2020-05-13 02:22:00'),
(44, 'produkty-notesy-klejone', 1, 6, 'klejone_n_pl', 'klejone_n_en', 'info_pl', 'info_en', '', '', '2020-05-13 02:33:12'),
(45, 'produkty-notesy-spiralowane', 3, 6, 'spiralowane_n_pl', 'spiralowane_n_en', 'info_pl', 'info_en', '', '', '2020-05-13 02:33:12'),
(46, 'produkty-wizytowki-standardowe', 1, 7, 'standardowe_pl', 'standardowe_en', 'info_pl', 'info_en', '', '', '2020-05-16 12:05:57'),
(47, 'produkty-wizytowki-skladane', 2, 7, 'skladane_pl', 'skladane_en', 'info_pl', 'info_en', '', '', '2020-05-16 12:05:57');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `articles_subcategorys_features`
--

CREATE TABLE `articles_subcategorys_features` (
  `id` int UNSIGNED NOT NULL,
  `subcategory_id` int UNSIGNED NOT NULL,
  `feature_id` int UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `articles_subcategorys_features`
--

INSERT INTO `articles_subcategorys_features` (`id`, `subcategory_id`, `feature_id`, `created_at`) VALUES
(1, 6, 4, '2020-05-16 16:15:18'),
(2, 6, 5, '2020-05-16 16:15:18'),
(3, 6, 7, '2020-05-16 16:15:18'),
(4, 6, 9, '2020-05-16 16:15:18'),
(5, 6, 10, '2020-05-16 16:15:18'),
(6, 7, 4, '2020-05-16 16:16:30'),
(7, 7, 5, '2020-05-16 16:16:30'),
(8, 7, 7, '2020-05-16 16:16:30'),
(9, 7, 9, '2020-05-16 16:16:30'),
(10, 7, 10, '2020-05-16 16:16:30'),
(11, 8, 4, '2020-05-16 16:17:41'),
(12, 8, 5, '2020-05-16 16:17:41'),
(13, 8, 7, '2020-05-16 16:17:41'),
(14, 8, 9, '2020-05-16 16:17:41'),
(15, 8, 10, '2020-05-16 16:17:41'),
(16, 9, 4, '2020-05-16 16:18:57'),
(17, 9, 5, '2020-05-16 16:18:57'),
(18, 9, 7, '2020-05-16 16:18:57'),
(19, 9, 9, '2020-05-16 16:18:57'),
(20, 10, 4, '2020-05-16 16:20:00'),
(21, 10, 5, '2020-05-16 16:20:00'),
(22, 10, 7, '2020-05-16 16:20:00'),
(23, 10, 9, '2020-05-16 16:20:00'),
(24, 11, 4, '2020-05-16 16:21:16'),
(25, 11, 5, '2020-05-16 16:21:16'),
(26, 11, 7, '2020-05-16 16:21:16'),
(27, 11, 9, '2020-05-16 16:21:16'),
(28, 12, 2, '2020-05-16 16:24:45'),
(29, 12, 6, '2020-05-16 16:24:45'),
(30, 12, 7, '2020-05-16 16:24:45'),
(31, 12, 9, '2020-05-16 16:24:45'),
(32, 12, 10, '2020-05-16 16:24:45'),
(33, 12, 11, '2020-05-16 16:24:45'),
(34, 13, 2, '2020-05-16 16:26:08'),
(35, 13, 6, '2020-05-16 16:26:08'),
(36, 13, 7, '2020-05-16 16:26:08'),
(37, 13, 9, '2020-05-16 16:26:08'),
(38, 13, 10, '2020-05-16 16:26:08'),
(39, 13, 11, '2020-05-16 16:26:08'),
(40, 14, 2, '2020-05-16 16:27:47'),
(41, 14, 6, '2020-05-16 16:27:47'),
(42, 14, 7, '2020-05-16 16:27:47'),
(43, 14, 9, '2020-05-16 16:27:47'),
(44, 14, 10, '2020-05-16 16:27:47'),
(45, 14, 11, '2020-05-16 16:27:47'),
(46, 15, 2, '2020-05-16 16:29:41'),
(47, 15, 6, '2020-05-16 16:29:41'),
(48, 15, 7, '2020-05-16 16:29:41'),
(49, 15, 9, '2020-05-16 16:29:41'),
(50, 15, 10, '2020-05-16 16:29:41'),
(51, 15, 11, '2020-05-16 16:29:41'),
(52, 16, 2, '2020-05-16 16:31:08'),
(53, 16, 6, '2020-05-16 16:31:08'),
(54, 16, 7, '2020-05-16 16:31:08'),
(55, 16, 9, '2020-05-16 16:31:08'),
(56, 16, 10, '2020-05-16 16:31:08'),
(57, 16, 11, '2020-05-16 16:31:08'),
(58, 17, 2, '2020-05-16 16:32:52'),
(59, 17, 6, '2020-05-16 16:32:52'),
(60, 17, 7, '2020-05-16 16:32:52'),
(61, 17, 9, '2020-05-16 16:32:52'),
(62, 17, 10, '2020-05-16 16:32:52'),
(63, 17, 11, '2020-05-16 16:32:52'),
(64, 18, 3, '2020-05-16 16:37:21'),
(65, 18, 4, '2020-05-16 16:37:21'),
(66, 18, 7, '2020-05-16 16:37:21'),
(67, 18, 8, '2020-05-16 16:37:21'),
(68, 18, 9, '2020-05-16 16:37:21'),
(69, 18, 10, '2020-05-16 16:37:21'),
(70, 19, 3, '2020-05-16 16:38:30'),
(71, 19, 4, '2020-05-16 16:38:30'),
(72, 19, 7, '2020-05-16 16:38:30'),
(73, 19, 8, '2020-05-16 16:38:30'),
(74, 19, 9, '2020-05-16 16:38:30'),
(75, 19, 10, '2020-05-16 16:38:30'),
(76, 20, 3, '2020-05-16 16:39:57'),
(77, 20, 4, '2020-05-16 16:39:57'),
(78, 20, 7, '2020-05-16 16:39:57'),
(79, 20, 8, '2020-05-16 16:39:57'),
(80, 20, 9, '2020-05-16 16:39:57'),
(81, 20, 10, '2020-05-16 16:39:57'),
(82, 21, 4, '2020-05-16 16:42:01'),
(83, 21, 7, '2020-05-16 16:42:01'),
(84, 21, 9, '2020-05-16 16:42:01'),
(85, 21, 10, '2020-05-16 16:42:01'),
(86, 22, 4, '2020-05-16 16:43:02'),
(87, 22, 7, '2020-05-16 16:43:02'),
(88, 22, 9, '2020-05-16 16:43:02'),
(89, 22, 10, '2020-05-16 16:43:02'),
(90, 23, 4, '2020-05-16 16:43:37'),
(91, 23, 7, '2020-05-16 16:43:37'),
(92, 23, 9, '2020-05-16 16:43:37'),
(93, 23, 10, '2020-05-16 16:43:37'),
(94, 46, 1, '2020-05-16 16:45:04'),
(95, 46, 4, '2020-05-16 16:45:04'),
(96, 46, 7, '2020-05-16 16:45:04'),
(97, 46, 9, '2020-05-16 16:45:04'),
(98, 46, 10, '2020-05-16 16:45:04'),
(99, 46, 12, '2020-05-16 16:45:04'),
(100, 47, 1, '2020-05-16 16:46:06'),
(101, 47, 4, '2020-05-16 16:46:06'),
(102, 47, 7, '2020-05-16 16:46:06'),
(103, 47, 9, '2020-05-16 16:46:06'),
(104, 47, 10, '2020-05-16 16:46:06'),
(105, 47, 12, '2020-05-16 16:46:06');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `carts`
--

CREATE TABLE `carts` (
  `id` int UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `companies`
--

CREATE TABLE `companies` (
  `id` int UNSIGNED NOT NULL,
  `ref` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `register` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `vat` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `contacts`
--

CREATE TABLE `contacts` (
  `id` int UNSIGNED NOT NULL,
  `id_credential` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `id_company` int UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `surname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `gsm` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `department` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `section` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `position` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `credentials`
--

CREATE TABLE `credentials` (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `role_id` int NOT NULL,
  `ref` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `terms` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `activation_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)',
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `credentials`
--

INSERT INTO `credentials` (`id`, `role_id`, `ref`, `password_hash`, `terms`, `is_active`, `activation_id`, `reset_id`, `created_at`) VALUES
('5eac675a9a02d1.26262525', 1, 'superadmin@superadmin.com', '$2y$10$cvBHXvudnfI2SbTBFITB0uYqU6qLXN.zMLPOvA/APLQMLn45DnSzu', 'OK', 1, 'JxsTtyOA4IFD03/ARsZNVeJMrnnLUIe3x1KDPDQ2GO1VjZyR34QAr6+bCn71+4O7lLlEWb0w2haN9eIb5CflovVqV6uN0zAzi2fkGwXpKyFfpaNKUTu/zmrZH1E3+U1q', NULL, '2020-05-01 20:15:54'),
('5eac6aac0ea024.89330589', 2, 'adminuser@adminuser.com', '$2y$10$8J/HHCcB/O37d.bp.0qFleqYk6HCYgr7po2Ky6fiz0OZkKVSNopw2', 'OK', 1, 'XDLWgmM7SVWhcICd3qz/1jQcUzYk3VBe7kN5e5sq8S6ulOjNwuHlJGeoE7llmjQwYbc8S0VtEnyA3ohR8mDk4SdFV+iUp2mwbB4hUVI0X3fFGiRR2yE0xJkKLvfJnY5E', NULL, '2020-05-01 20:30:04'),
('5eb09f3a344a26.24196510', 4, 'simpleuser@simpleuser01.com', '$2y$10$rRTw9Eq1OLRSxmORZSdSyOpUR1oen/7dRHIhGgWRS0BZx/4jA9bRi', 'OK', 1, NULL, NULL, '2020-05-05 01:03:22');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `fields`
--

CREATE TABLE `fields` (
  `id` int NOT NULL,
  `ref` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)',
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `fields`
--

INSERT INTO `fields` (`id`, `ref`, `created_at`) VALUES
(1, '*', '2020-05-05 18:59:08'),
(2, 'ref', '2020-05-05 19:34:00');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `invoices`
--

CREATE TABLE `invoices` (
  `id` int UNSIGNED NOT NULL,
  `ref` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `id_company` int UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `notebooks`
--

CREATE TABLE `notebooks` (
  `id` int UNSIGNED NOT NULL,
  `ref` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `subcategory_id` int UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `orders`
--

CREATE TABLE `orders` (
  `id` int UNSIGNED NOT NULL,
  `ref` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `payments`
--

CREATE TABLE `payments` (
  `id` int UNSIGNED NOT NULL,
  `ref` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `id_company` int UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `privileges`
--

CREATE TABLE `privileges` (
  `id` int NOT NULL,
  `access_id` int DEFAULT '1',
  `ref` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `route` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `info` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)',
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `privileges`
--

INSERT INTO `privileges` (`id`, `access_id`, `ref`, `route`, `info`, `created_at`) VALUES
(1, 1, 'authe.credentials.show', 'authe.credentials.show', 'a', '2020-05-05 19:05:20'),
(2, 1, 'authe.credential.add', 'authe.credential.add', 'a', '2020-05-05 19:05:20'),
(3, 1, 'authe.credential.edit', 'authe.credential.edit', 'a', '2020-05-05 21:10:09'),
(4, 1, 'authe.credential.delete', 'authe.credential.delete', 'a', '2020-05-05 21:16:16'),
(5, 1, 'authe.credential.update', 'authe.credential.update', 'a', '2020-05-05 21:16:30'),
(6, 1, 'authe.credential.unsubscribe', 'authe.credential.unsubscribe', 'a', '2020-05-05 21:16:46'),
(7, 1, 'authe.credential.logout', 'authe.credential.logout', 'a', '2020-05-05 21:17:21'),
(8, 1, 'autho.accesses.show', 'autho.accesses.show', 'a', '2020-05-05 21:17:34'),
(9, 1, 'autho.access.show', 'autho.access.show', 'a', '2020-05-05 21:17:53'),
(10, 1, 'autho.access.add', 'autho.access.add', 'a', '2020-05-05 21:18:07'),
(11, 1, 'autho.access.edit', 'autho.access.edit', 'a', '2020-05-05 21:18:26'),
(12, 1, 'autho.access.delete', 'autho.access.delete', 'a', '2020-05-05 21:18:41'),
(13, 1, 'autho.access.privileges.show', 'autho.access.privileges.show', 'a', '2020-05-05 21:18:55'),
(14, 1, 'autho.access.privileges.edit', 'autho.access.privileges.edit', 'a', '2020-05-05 21:19:09'),
(15, 1, 'autho.access.privileges.delete', 'autho.access.privileges.delete', 'a', '2020-05-05 21:19:22'),
(16, 1, 'autho.fields.show', 'autho.fields.show', 'a', '2020-05-05 21:19:36'),
(17, 1, 'autho.field.show', 'autho.field.show', 'a', '2020-05-05 21:19:50'),
(18, 1, 'autho.field.add', 'autho.field.add', 'a', '2020-05-05 21:20:02'),
(19, 1, 'autho.field.edit', 'autho.field.edit', 'a', '2020-05-05 21:20:22'),
(20, 1, 'autho.field.delete', 'autho.field.delete', 'a', '2020-05-05 21:20:37'),
(21, 1, 'autho.field.privileges.edit', 'autho.field.privileges.edit', 'a', '2020-05-05 21:20:53'),
(22, 1, 'autho.field.privileges.delete', 'autho.field.privileges.delete', 'a', '2020-05-05 21:21:10'),
(23, 1, 'autho.privileges.show', 'autho.privileges.show', 'a', '2020-05-05 21:21:26'),
(24, 1, 'autho.privilege.show', 'autho.privilege.show', 'a', '2020-05-05 21:23:55'),
(25, 1, 'autho.privilege.add', 'autho.privilege.add', 'a', '2020-05-05 21:24:16'),
(26, 1, 'autho.privilege.edit', 'autho.privilege.edit', 'a', '2020-05-05 21:25:25'),
(27, 1, 'autho.privilege.delete', 'autho.privilege.delete', 'a', '2020-05-05 21:25:25'),
(28, 1, 'autho.privilege.fields.edit', 'autho.privilege.fields.edit', 'a', '2020-05-05 21:25:55'),
(29, 1, 'autho.privilege.roles.edit', 'autho.privilege.roles.edit', 'a', '2020-05-05 21:26:22'),
(30, 1, 'autho.privilege.fields.delete', 'autho.privilege.fields.delete', 'a', '2020-05-05 21:26:57'),
(31, 1, 'autho.privilege.roles.delete', 'autho.privilege.roles.delete', 'a', '2020-05-05 21:26:57'),
(32, 1, 'autho.roles.show', 'autho.roles.show', 'a', '2020-05-05 21:29:09'),
(33, 1, 'autho.role.show', 'autho.role.show', 'a', '2020-05-05 21:29:09'),
(34, 1, 'autho.role.add', 'autho.role.add', 'a', '2020-05-05 21:29:09'),
(35, 1, 'autho.role.edit', 'autho.role.edit', 'a', '2020-05-05 21:29:09'),
(36, 1, 'autho.role.delete', 'autho.role.delete', 'a', '2020-05-05 21:29:09'),
(37, 1, 'autho.role.privileges.edit', 'autho.role.privileges.edit', 'a', '2020-05-05 21:29:09'),
(38, 1, 'autho.role.privileges.delete', 'autho.role.privileges.delete', 'a', '2020-05-05 21:29:09'),
(43, 1, 'article.articles.show', 'article.articles.show', 'a', '2020-05-12 12:27:51'),
(44, 1, 'article.article.add', 'article.article.add', 'a', '2020-05-12 12:28:17'),
(45, 1, 'article.article.edit', 'article.article.edit', 'a', '2020-05-12 12:28:34'),
(46, 1, 'article.article.delete', 'article.article.delete', 'a', '2020-05-12 12:28:56'),
(47, 1, 'article.categories.show', 'article.categories.show', 'a', '2020-05-12 12:29:14'),
(48, 1, 'article.category.add', 'article.category.add', 'a', '2020-05-12 12:29:30'),
(49, 1, 'article.category.edit', 'article.category.edit', 'a', '2020-05-12 12:29:47'),
(50, 1, 'article.category.delete', 'article.category.delete', 'a', '2020-05-12 12:30:05'),
(51, 1, 'article.fixings.show', 'article.fixings.show', 'a', '2020-05-12 12:30:46'),
(52, 1, 'article.fixing.add', 'article.fixing.add', 'a', '2020-05-12 12:31:03'),
(53, 1, 'article.fixing.edit', 'article.fixing.edit', 'a', '2020-05-12 12:31:20'),
(54, 1, 'article.fixing.delete', 'article.fixing.delete', 'a', '2020-05-12 12:31:36'),
(55, 1, 'article.formats.show', 'article.formats.show', 'a', '2020-05-12 12:32:12'),
(56, 1, 'article.format.add', 'article.format.add', 'a', '2020-05-12 12:32:27'),
(57, 1, 'article.format.edit', 'article.format.edit', 'a', '2020-05-12 12:32:47'),
(58, 1, 'article.format.delete', 'article.format.delete', 'a', '2020-05-12 12:33:03'),
(59, 1, 'article.handles.show', 'article.handles.show', 'a', '2020-05-12 12:33:23'),
(60, 1, 'article.handle.add', 'article.handle.add', 'a', '2020-05-12 12:33:44'),
(61, 1, 'article.handle.edit', 'article.handle.edit', 'a', '2020-05-12 12:33:58'),
(62, 1, 'article.handle.delete', 'article.handle.delete', 'a', '2020-05-12 12:34:16'),
(63, 1, 'article.overprints.show', 'article.overprints.show', 'a', '2020-05-12 12:34:44'),
(64, 1, 'article.overprint.add', 'article.overprint.add', 'a', '2020-05-12 12:34:59'),
(65, 1, 'article.overprint.edit', 'article.overprint.edit', 'a', '2020-05-12 12:35:19'),
(66, 1, 'article.overprint.delete', 'article.overprint.delete', 'a', '2020-05-12 12:35:33'),
(67, 1, 'article.papers.show', 'article.papers.show', 'a', '2020-05-12 12:35:52'),
(68, 1, 'article.paper.add', 'article.paper.add', 'a', '2020-05-12 12:36:17'),
(69, 1, 'article.paper.edit', 'article.paper.edit', 'a', '2020-05-12 12:36:32'),
(70, 1, 'article.paper.delete', 'article.paper.delete', 'a', '2020-05-12 12:36:49'),
(71, 1, 'article.refinements.show', 'article.refinements.show', 'a', '2020-05-12 12:37:08'),
(72, 1, 'article.refinement.add', 'article.refinement.add', 'a', '2020-05-12 12:37:28'),
(73, 1, 'article.refinement.edit', 'article.refinement.edit', 'a', '2020-05-12 12:37:41'),
(74, 1, 'article.refinement.delete', 'article.refinement.delete', 'a', '2020-05-12 12:37:57'),
(75, 1, 'article.subcategories.show', 'article.subcategories.show', 'a', '2020-05-12 12:38:16'),
(76, 1, 'article.subcategory.add', 'article.subcategory.add', 'a', '2020-05-12 12:38:32'),
(77, 1, 'article.subcategory.edit', 'article.subcategory.edit', 'a', '2020-05-12 12:38:47'),
(78, 1, 'article.subcategory.delete', 'article.subcategory.delete', 'a', '2020-05-12 12:39:12');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `privileges_fields`
--

CREATE TABLE `privileges_fields` (
  `privilege_id` int NOT NULL,
  `field_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `roles`
--

CREATE TABLE `roles` (
  `id` int NOT NULL,
  `ref` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `info` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)',
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `roles`
--

INSERT INTO `roles` (`id`, `ref`, `info`, `created_at`) VALUES
(1, 'Super admin', 'a', '2020-05-01 23:59:46'),
(2, 'Admin', 'a', '2020-05-01 00:00:10'),
(3, 'Visitor', 'a', '2020-05-01 00:00:26'),
(4, 'User', 'a', '2020-05-01 00:00:35');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `roles_privileges`
--

CREATE TABLE `roles_privileges` (
  `role_id` int NOT NULL,
  `privilege_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `roles_privileges`
--

INSERT INTO `roles_privileges` (`role_id`, `privilege_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 14),
(1, 15),
(1, 16),
(1, 17),
(1, 18),
(1, 19),
(1, 20),
(1, 21),
(1, 22),
(1, 23),
(1, 24),
(1, 25),
(1, 26),
(1, 27),
(1, 28),
(1, 29),
(1, 30),
(1, 31),
(1, 32),
(1, 33),
(1, 34),
(1, 35),
(1, 36),
(1, 37),
(1, 38),
(2, 2),
(2, 3),
(2, 4),
(2, 5),
(2, 6),
(2, 7),
(2, 43),
(2, 44),
(2, 45),
(2, 46),
(2, 47),
(2, 48),
(2, 49),
(2, 50),
(2, 51),
(2, 52),
(2, 53),
(2, 54),
(2, 55),
(2, 56),
(2, 57),
(2, 58),
(2, 59),
(2, 60),
(2, 61),
(2, 62),
(2, 63),
(2, 64),
(2, 65),
(2, 66),
(2, 67),
(2, 68),
(2, 69),
(2, 70),
(2, 71),
(2, 72),
(2, 73),
(2, 74),
(2, 75),
(2, 76),
(2, 77),
(2, 78),
(4, 6),
(4, 7);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `sessions`
--

CREATE TABLE `sessions` (
  `id` int NOT NULL,
  `session_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_agent` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `data` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)',
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `sessions`
--

INSERT INTO `sessions` (`id`, `session_id`, `ip_address`, `user_agent`, `data`, `created_at`) VALUES
(31, 'bpou8ndbi1a9nkscueaj2sbp0b', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) snap Chromium/81.0.4044.138 Chrome/81.0.4044.138 Safari/537.36', 'def50200c313e370146c412c257a7c07250db7588a9e2a66e5edb4c05560f28bafb507230630d952a4c25ffb9b952ea5f7300f091b57c17379b55d21b70fa810d146a3d3912c1cb343d8560e64debd587d56f155538135732936624456b1447c551ea208d402608e18816174dc728e6eceae7d3f233a65b5bc634d0e574b0e4d48405892dc8fe7bb0d9a638ea9b3bc933005a8c2865a59e90caf7c4d016ee3959e36dc5dcd798e9dc628babcd71fd01fd8ec6211911250ea6ace1f34a2c7dd7099f59ba5ae32b08116d3d966c507c29d8297e2d87ed4e79910717acd4c66', '2020-05-11 14:33:45'),
(32, 'dkjgk26vp7b6h3fh4rn03oj9ba', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) snap Chromium/81.0.4044.138 Chrome/81.0.4044.138 Safari/537.36', 'def502000493880315f6a3c68d1c09ed888293efb9507e6e5c654397f40ba3399b85c0d37764afcb89c595da8ea2f4eeb2481cc41fa17d0a74c75c33dcd96913fb0e32af548cdf4f087dc94d0ab904dbe1f1069150360d8be2f3d4c0103a964ae870eb2709a382daed9e5d6760f61f', '2020-05-11 14:37:17'),
(33, 'vqpfq17qjrktcvkoiu2dif10vm', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) snap Chromium/81.0.4044.138 Chrome/81.0.4044.138 Safari/537.36', 'def50200f500a8e231ac6aa04706df4837ead5a49e1cf327e5658a387da7169dababfd01c58ade0e6b7b3de488bbdea31063836a6ab27de942594fb446fdd41b0aa7dbfbcdd2ca5ac84c7b2667270f35565e2ab71f2c1e14097e20c87bf6f6784a50f7a986b7c8d590735fbb857087', '2020-05-11 14:37:59'),
(34, 'qv2f94ig07to2vtr9ttm36mvm2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) snap Chromium/81.0.4044.138 Chrome/81.0.4044.138 Safari/537.36', 'def50200fa7a56e0dc8e7c1cb7003958ee6189bf54398f15da82721f19b877992f93d013f5265eeac9a6e1046bdfa62ffb02a2549829b6c0476b43cf5c311e594c4c89dbb44c5edf29e984e2f584fd9f008a17c0797ad36f0d8e00bfb79538e4635e2771d9d6387a04373882ebd87f', '2020-05-11 14:39:10'),
(35, 'bn14p85m2uvd8gfld0upntfk94', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) snap Chromium/81.0.4044.138 Chrome/81.0.4044.138 Safari/537.36', 'def502000235f2f39cd859ddeb9012f9405ce4f7ad4958590a8bb6f75a6e6f2814c655d4a34980716622738b348901a88c85bb17e5cced437d933717482bece0794307d167526a7715228cdda2fe0c5d8d090b9d38b588b1f6b18e546262c3a9c31259d84c762a3c6d50ac558dfc35ecd4f9ee4306f56506ac6a5097f106e0765a213f3e8ea09ab3e6038e2a20a5a511879cc60f21a8140ba8051f41719c0f75fc79ca928480f39c7cc178d37cf3e01795a8264f8c39f4abbedd05e22cbc099dea5dd8a552e6998955a3fb1c614d1d546a992ec8f240ba0faf542c247eb7d93f7d21d4715a0484ce0745c6dc10a4b8140becc3fb6f6a55dff3a5c7cbc980e61779979966e78dd956adfbe6dd6c6d3b6158a0feb3b22eeb42e9765fa2f53d3dccd1fc73c027a27b332853d263a08ff1861d57ae631ccd8e62e98256076df592a4be46a863852e313034fd24d6e783a2d81985cbca43ab8773aa46244849314ab865ae1d3aef3c10a8dd96ad81d56d', '2020-05-11 14:52:20'),
(36, 'gf6rb96422keu7oah4crj4fbii', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) snap Chromium/81.0.4044.138 Chrome/81.0.4044.138 Safari/537.36', 'def502003e2988e969e49135891e17f046a2cb629f3aabc509c4283a236e90c5b2d8e521958dd4273aece0783222cd9fc2836ecf8e734f6af777850f0502193b9784edc9229b5f6ea81ba5cb9917a821005125ea808f599e0a19f8c23b487f480f409209459e2dc0b648df012f2ace0259a2f339cd978509b056a865242d06209bc0be6329ed01d4bcfd112c63a0d7a89ebbed32ee3eccbc1d7078c463c0f809086148103bc2002960fecac9a5b57ddc38a380e6b26b9b4883c9990279618648e4320e407667acd6f1ef7bbcdd93d25d3cb83b76079a4990359c2f063d426487f30fc09cd99951e76fb51f1122d09a9040aef3bfca5882e8d641f38d30a344687b667aeb7592b2621e897fe7fc0b1118f7552b50ed03ffc17ee9d8cdf0fc2d9538278b46f19ba0aaa8d8ed1022c6816d02bed5c6d514d44c43f16e0fbedc33428ca58d3477a00097be21df8cfb740853d7a4c32e07006591b31580b4d52ff473d1571644d1eaaf268f13b2b0ee73a97969cca2fee323ddafc805fd3f8fca7e5f1bc6e0a71c4664c6909c1000749ae9dacb5cc22f74ca1c4d23002bfa679f85be0bd92d37968e2eb79c0d2891f65e6fc6316333d0d804d8751aee6d78726f1c9e84af3b975b533a4376ba720637429fb49ef918cc4f0c29cce9f15ddaa5cc8ab6fd7cbfbd71cf3868fa3bdeb7b882dc0e3a482fb05f95747dbbc2c49d5a0a7c5b08142b27c4468c499e4ae264983ef41c0a2fa5248293d42db8fc2fd90658c4aef7af7ce9beac63d0fc2a3f2ffd1096dbd6184f4be9959cc8a5fc21413d3457ca954bb9c3f1cb0324495cb1ceb6b02035e8c5cf956cfc8e9463620fd9e069791ce7bb2352694d4439bab5eea69cb325803570ab7bba5266c95b5cc7e677bd66a9c7dc4e0484fb4964a86577f5ee15998dda308c8697041f1e0e0689d5f5e59e7c3276c6369ad436cec77c6a965a3428bf2b4537f9bea8d2eecd986990bc4ff844541016e6bb4bba6fd06903125c9f13c6195a37f9428485fc82cf279c9563a52750b040327d49eb5ab74e778d7cc20c69d9c15e203c53cedd392344771975c81a989f3eac8e1b14688e44f28aef88fd3b39f88f6f010b6a1d8d3a95b95c9d74c14e5199cb7f10103fef7f9d4bd0e472215b23b36c1466461c4b848d69ad9b12a61066236186b7760b7a0b8107e3c1cbbb0f9259a1ac5891b2fa9b6119443e108be9bf1f9a6b772e0dc9bb7fdded0bf255b8fbd4cf548ff8b60e60540e94d479b0119cb6d130cd67f28367fe0ba79a0708bbdfdc2a743b0776814170ab3d752c9bc0b6fdbdafe62e2692544d5586708a4d516371224ee45b2279de1a47b2d9931c4dd6ea84ccc57cfcf7d4d89bfb31909ca9495fc29ef0af98969c26ecf8a8c5d8388156a6cdf54dbecd0f7f88df614371754ed539d519d096b111881fd3b0e9e5f8f70657688b3c5c14577bf4ee7f2bc181aaffed0dbab9a8beae990e10eac98c12a1ecb8345ec55c8ba23f29d0580583bb542cdc925db00dc7e93c09c9695507a707e562bb19547f00269191e705e197a89d3255322e27126ede480c450ac2a6b540f11782456ee5a6cb18000af838141b921c6a2541d0f1dae8f11b379f6c4647253a3ff8490bb8f5550d8b2a14af9fa5192edfe98c2687ab9b52902d2377e1825e675b2b406afc51d3a33865246c6da5049c0d6d289d010972322a68ee96f24a19e0d469b1725c27a140552ffc9e45e96b780529419a88309997aa6670d652e0078796847cc0f750670294a486b4393fdb8db9907113930792d449d3cb6972fa2658f2cf53bd71f6ba4e9ce375e1fc1b54c3f3d151caaa8d899664e52aa967e7bc683671bd943a87ea7f80ded0702b15a673c8d2a0c38fda94b8571aee83228906c4a18a', '2020-05-11 14:56:22'),
(37, 'tr860ii8mqm2hb10jqugj1q0fe', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) snap Chromium/81.0.4044.138 Chrome/81.0.4044.138 Safari/537.36', 'def50200dc16706a9b6f3cc9c26e12b456a505ec31030006cdc7e207b6114465cb6ddce4f675ffe252f813703dd071f085e572748feea563707ebff93d0a7491a1deb45ece02737edd87980eccdd5ab77f6ea76455bca8ae1a7a38f03b1321bcd72a339c85408d7d40f65142da20396ec68bf80ed89b9b19762703077532bd46209cc10f7de4ec2fc0a082839c817c2c9c7009274e0033fe39ab4e769bebe233edf84fdef9c3e6592070f9e38e83ef7e8e2f994db92ec449b514fe6194ca18ac710f3c2b2f6159431f13aa7534c3ab4fc24db33a6abc0c9fff1f4066948c1f2c74e315ea9b052c5cd5ec7c104a01c39129b18863a7b2b614e9305bfce0adecd3b1506d902d8907f54e938cd377b3c7a316734591095134807fed95f12221c5', '2020-05-12 23:08:31'),
(38, 'lp73sju5lnp0c21p8q2jn2v9tc', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) snap Chromium/81.0.4044.138 Chrome/81.0.4044.138 Safari/537.36', 'def50200f6f3a1c7894fc0d9459fa8e358e5bf28136215f93706dda7346298f46444a962d1ad0ee608867a07024473983b9e5cfa34a388de65b672690381e121516055e226a1ebff8e6d2b6c4d7e6e2735d78b1c807794eca6f80e3b550c6ae53a1f062d9f1c725400db8894e8d12e06724a01776169a8f60f44eef30dece3396f7fb54752e5ba87000bf47069d696dee1a3019d6d39f477dd4aa2a5ca9da9f64c1589e06802e62ee2799d37e21e975fe4bc8f29167d4de36e5416f8910d2639b5e5b5b3a6fe3e6e3a44062269e7a33abd1194fbc5b8c8a6def937e798a5b5531586663602f5d10643702bafd72d4657900424a8b9d2be2034f142c775e9b7d96a8ae3fb0d84e952f88b5bac7dcd2bf2bbecb4e8dc50c70abb846f99ddb9a6d7e5ac1746fabbf7f6333c8b8a65d7fd7123197feb779217e725d51f01e58f9ddfff6d4b1dca410a9a6024069e19ac02fcd4a5eaf48a59e0e00e497daf92f67bca07fe1b0947c432efebeb86dbc9215bd4d260c04c7b8f36ce1e14174b938b0c2277157afe94c9f60837962d57da3c6702f4840f77474333a5b1ce1edb47dc47c7549fe7f95cf89d67b535fd4e7160512c8252058bd2307abe6af68e47b1af9a72e6aafab2b3fd1497f5aa04056e0d2091a03fc257c3f34a24608fb8e160d51fb9a13773951bd86956491d20c02683be7096a41582bac22f5192a467c6a717ea1a1ff3c07695f314cf65fc072c4d820185aad4bb2dac03f90e9d6571fcc3e20739fbb4a84140320ba5ba95594cf59b912b94474a8e4df16d31f20862e31ca599d937ade97b6fdec91e8e066c41fb42af19b1ab538054c878e4e9242cb6510e7b6169dcef50dddb88ef58a27b510c40565bef58521956d72de82aad7dcf7601fc2691d0fde8a55b4c2766d794ebc986e4446fd005661cc7d9361bff9e3c8a277bd0d47594ccad5f561af1384354f8541a1c46c0bf09161133f0735b3aaa5b79dc62ed161bad7ea784f9a6fee682416e97825adee441f9257471af910177e1cadf1984fcf0eea5c77662ce7f03e9cf52ed28a6a620b0f1485ba0514e020443966949b4641f4ac568f5', '2020-05-13 12:21:52'),
(39, 'brbejla0qsaa292n3k464gmn15', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) snap Chromium/81.0.4044.138 Chrome/81.0.4044.138 Safari/537.36', 'def50200cbd925102ee6dff67252f06485223fb65cd2e8ad21fdeae1cc552e11b60d6eeffda4f33ba6774c5cf46faec5b95494e12ef6087f116e78d24f218848bf86af573b9219305abf289da751f198fe46ba182858aee572006e0639b66cc138e8bff7aaad09db040c8da74fec48d3079c777ef257865f60858cc85f69cd4dfd64ce6b2de949f376c9dc14189c367591084f1c29824784baf2c3d18995199d6f71e1abc4e22eeb5406436aee1413f58f2262d8277fd0917110d1f63e2b9c483f39496b33d22b8ace10d2ce3f024939a845d31de9945f7052f7857ccf5bb9c8de34d5b5aa856664a96cb0dd4062f14d47b34616e3065fbda4ce26b79d9cb7d69a58dcde48afb12d9fbe6e0e070fc87cbcc5692a9b3f8e8bc3f6c9b1e9383fac3a8e44e30070e4c8da59a42b9193f942ede38527fba6eee4167cf6cb8b6c2379bf9dc6127f24ad4b039a18d089b3c47a79ff2af6adfe0547405b5d71a9d0a80d8353208b3948a8ae3f74d390fd0bf3ad55a76f29bef774c5383d8322d516200ba53cbb5a1755a91ea2b6f049c555c8a5506c9913a534e064d5ae0887c05d02f98bfffc75505c0eaae216480845ecb67869d0c04a1604e94132a0be6687885db7d073f4747b8f323c7b4487f68d1126ed9f3cbea97ae8005538cec55cb988c408d82df0a8d3e291e937a005b6bdbba715c26ec486ffe82d09f3eb7b865667f4794148b96df8b5575b86f92d3cecb98117c8a1eca96c098fc819a5fbab36e6e3530543d6f444f8b322bdca3ac148ed9a23fb7cf8d197f1b17ebcb65e39d2cbcc8cf83949a6c4a2bf210784425e64a38b881be562211c1d5f755f646e5cbf955fbd482063e4a687d9bf6c8ee3b72b28e0a6132a3ab3f2c8431e02688c0bf741e77b4c3c31279d3bfe1477291d22cdf83b5defc333f67391dfcb7b8788fefc6fc9080a1b9027798bf152e9db2775351c9738ca421f3be167414c7de39fd5c9550d2f39e0a3315285d1d29e044493ea927dbdb2f10b1e418136c56b4de2da2a55ee5d56ae909bb13cfe02b89b1913c08d5e8bc69808542ab8a8726a742407a191b4f4ed097610e5fc8c72bed9aee1b78602dbacdb6abf8945b7a78a1488c7775debf6806152f3ea5e6222d4da2dea9155fb691afd170f160ec9368f583dcfe3140df73ff99af594ba3c407709444924f42d5ac5538efe38748adc936965a0ad2565ca3746816225041c2bcd3924a2de8ac76d44d02a23a1633fb822f210cc24126ce7b0f41bf133765ef0d9512baf8bc03a5a92190b2f3b4e7b19b4f6f6b37c3e834f565595c2662dd095e487792783412a4d17a4a21e3130ff62c0492a39ac001d9a20ee4008e4671e91ade7fc633c4775c42c8f5f4d102abdc2e4b1d083ab0a3445d6958ad1c6fc1ab25737729d93fb5601c79b4262c7100924156b50e7d7655bad5abf88ddf2ba5fd427d6e4806652f965586718fdb50bc15f0c8227b0a7f7c37f5191d638a5dcfce8c64bfadff707eaa59a938598b0f11c4b1f5a9f09671cfeb2630abadd0bcf7472cb717b356ccc2499c700a63e599230e835550dc371f9a49c4d679568e29be646ac4974d49f53809b79c1ca304a893a0e39645ec1cdaba1ea517b2c3bb6f2bf8b612078905a313793278f2250b148234005050f227fdaa6951fea4d3949cf5ce7bc315843266e5cdc5dc3eed7f238d57e331fc73c54244bd4c03c2745e461790d6f99248f1327481fb571e869f4dffece42cbd92c5551de4c3f0133a8211838adc7288241c4e65372651feb1cde6458ec6ae80230bd71a54b7818576cc9a79d7c19d43a3d316dbdc529f8ce6c13d7b4b55ddc30ea6518cd76b89f7224b4ea246886b0aa8e2a3984dd973e1c35e5d6a375f5c1c2b7ddd3224fe9837beca983f951c4a13f8089632df83bf9f804518cc2e4b872a0b0263dc0f283b65e0b59f94e61bba3591cec11eed7fc3138d39f7a101933218b5e8886b036b01231cdcf50c7c3536c32a1a99a804a3f609b41e998aa0c9e13999974740da73226cc0505f6a76a24a3c504', '2020-05-15 12:35:13');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `shipments`
--

CREATE TABLE `shipments` (
  `id` int UNSIGNED NOT NULL,
  `ref` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `id_company` int UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `transactions`
--

CREATE TABLE `transactions` (
  `id` int UNSIGNED NOT NULL,
  `transaction_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `id_article` int UNSIGNED NOT NULL,
  `id_contact` int UNSIGNED NOT NULL,
  `id_cart` int UNSIGNED NOT NULL,
  `id_order` int UNSIGNED NOT NULL,
  `id_shippment` int UNSIGNED NOT NULL,
  `id_payment` int UNSIGNED NOT NULL,
  `id_invoice` int UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `transactions_types`
--

CREATE TABLE `transactions_types` (
  `id` int UNSIGNED NOT NULL,
  `ref` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `accesses`
--
ALTER TABLE `accesses`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `level_idx` (`level`),
  ADD UNIQUE KEY `ref_idx` (`ref`),
  ADD KEY `created_at_idx` (`created_at`),
  ADD KEY `updated_at_idx` (`updated_at`);

--
-- Indeksy dla tabeli `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_created_at` (`created_at`),
  ADD KEY `index_updated_at` (`updated_at`),
  ADD KEY `index_country` (`country`),
  ADD KEY `index_region` (`region`),
  ADD KEY `index_province` (`province`),
  ADD KEY `index_zip` (`zip`),
  ADD KEY `index_city` (`city`),
  ADD KEY `index_street` (`street`),
  ADD KEY `index_number` (`number`),
  ADD KEY `index_box` (`box`),
  ADD KEY `index_designation` (`designation`),
  ADD KEY `index_id_company` (`id_company`) USING BTREE;

--
-- Indeksy dla tabeli `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_ref` (`ref`),
  ADD KEY `index_created_at` (`created_at`),
  ADD KEY `index_updated_at` (`updated_at`),
  ADD KEY `index_insert_id` (`insert_id`) USING BTREE,
  ADD KEY `index_subcategory_id` (`subcategory_id`) USING BTREE,
  ADD KEY `index_category_id` (`category_id`) USING BTREE,
  ADD KEY `index_format_id` (`format_id`) USING BTREE,
  ADD KEY `index_overprint_id` (`overprint_id`) USING BTREE,
  ADD KEY `index_refinement_id` (`refinement_id`) USING BTREE,
  ADD KEY `index_handle_id` (`handle_id`) USING BTREE,
  ADD KEY `index_fixing_id` (`fixing_id`) USING BTREE,
  ADD KEY `index_elastic_id` (`elastic_id`) USING BTREE,
  ADD KEY `index_page_id` (`page_id`) USING BTREE,
  ADD KEY `index_corner_id` (`corner_id`) USING BTREE,
  ADD KEY `index_sample_id` (`sample_id`) USING BTREE,
  ADD KEY `index_ridge_id` (`ridge_id`) USING BTREE,
  ADD KEY `index_paper_id` (`paper_id`) USING BTREE;

--
-- Indeksy dla tabeli `articles_categorys`
--
ALTER TABLE `articles_categorys`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_ref` (`ref`),
  ADD UNIQUE KEY `unique_number` (`number`) USING BTREE,
  ADD KEY `index_created_at` (`created_at`),
  ADD KEY `index_updated_at` (`updated_at`),
  ADD KEY `index_info_pl` (`info_pl`),
  ADD KEY `index_info_en` (`info_en`),
  ADD KEY `index_label_pl` (`label_pl`) USING BTREE,
  ADD KEY `index_label_en` (`label_en`) USING BTREE;

--
-- Indeksy dla tabeli `articles_features`
--
ALTER TABLE `articles_features`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_ref` (`ref`) USING BTREE,
  ADD KEY `index_created_at` (`created_at`),
  ADD KEY `index_updated_at` (`updated_at`),
  ADD KEY `index_pl` (`pl`),
  ADD KEY `index_en` (`en`);

--
-- Indeksy dla tabeli `articles_options`
--
ALTER TABLE `articles_options`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_ref` (`ref`) USING BTREE,
  ADD KEY `index_created_at` (`created_at`),
  ADD KEY `index_updated_at` (`updated_at`),
  ADD KEY `index_pl` (`pl`),
  ADD KEY `index_en` (`en`),
  ADD KEY `feature_id` (`feature_id`);

--
-- Indeksy dla tabeli `articles_subcategorys`
--
ALTER TABLE `articles_subcategorys`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_ref` (`ref`) USING BTREE,
  ADD KEY `index_created_at` (`created_at`),
  ADD KEY `index_updated_at` (`updated_at`),
  ADD KEY `index_category_id` (`category_id`),
  ADD KEY `index_number` (`number`),
  ADD KEY `index_label_pl` (`label_pl`) USING BTREE,
  ADD KEY `index_label_en` (`label_en`) USING BTREE,
  ADD KEY `index_info_pl` (`info_pl`),
  ADD KEY `index_info_en` (`info_en`);

--
-- Indeksy dla tabeli `articles_subcategorys_features`
--
ALTER TABLE `articles_subcategorys_features`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_created_at` (`created_at`),
  ADD KEY `index_updated_at` (`updated_at`),
  ADD KEY `index_feature_id` (`feature_id`) USING BTREE,
  ADD KEY `index_subcategory_id` (`subcategory_id`) USING BTREE;

--
-- Indeksy dla tabeli `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_created_at` (`created_at`),
  ADD KEY `index_updated_at` (`updated_at`);

--
-- Indeksy dla tabeli `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_ref` (`ref`) USING BTREE,
  ADD UNIQUE KEY `unique_register` (`register`) USING BTREE,
  ADD UNIQUE KEY `unique_vat` (`vat`) USING BTREE,
  ADD KEY `index_created_at` (`created_at`),
  ADD KEY `index_updated_at` (`updated_at`),
  ADD KEY `index_name` (`name`) USING BTREE,
  ADD KEY `index_type` (`type`) USING BTREE;

--
-- Indeksy dla tabeli `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_gsm` (`gsm`),
  ADD UNIQUE KEY `unique_phone` (`phone`),
  ADD KEY `index_created_at` (`created_at`),
  ADD KEY `index_updated_at` (`updated_at`),
  ADD KEY `index_id_company` (`id_company`),
  ADD KEY `index_id_credential` (`id_credential`) USING BTREE,
  ADD KEY `index_name` (`name`),
  ADD KEY `index_surname` (`surname`),
  ADD KEY `index_department` (`department`),
  ADD KEY `index_section` (`section`),
  ADD KEY `index_position` (`position`);

--
-- Indeksy dla tabeli `credentials`
--
ALTER TABLE `credentials`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ref_idx` (`ref`),
  ADD UNIQUE KEY `password_hash_idx` (`password_hash`),
  ADD KEY `is_active_idx` (`is_active`),
  ADD KEY `activation_id_idx` (`activation_id`),
  ADD KEY `reset_id_idx` (`reset_id`),
  ADD KEY `created_at_idx` (`created_at`),
  ADD KEY `updated_at_idx` (`updated_at`);

--
-- Indeksy dla tabeli `fields`
--
ALTER TABLE `fields`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ref_idx` (`ref`),
  ADD KEY `created_at_idx` (`created_at`),
  ADD KEY `updated_at_idx` (`updated_at`);

--
-- Indeksy dla tabeli `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_ref` (`ref`),
  ADD KEY `index_created_at` (`created_at`),
  ADD KEY `index_updated_at` (`updated_at`),
  ADD KEY `index_id_company` (`id_company`);

--
-- Indeksy dla tabeli `notebooks`
--
ALTER TABLE `notebooks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_subcategory_id` (`subcategory_id`),
  ADD KEY `index_created_at` (`created_at`),
  ADD KEY `updated_at` (`updated_at`);

--
-- Indeksy dla tabeli `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_ref` (`ref`),
  ADD KEY `index_created_at` (`created_at`),
  ADD KEY `index_updated_at` (`updated_at`);

--
-- Indeksy dla tabeli `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_ref` (`ref`),
  ADD KEY `index_created_at` (`created_at`),
  ADD KEY `index_updated_at` (`updated_at`),
  ADD KEY `index_id_company` (`id_company`);

--
-- Indeksy dla tabeli `privileges`
--
ALTER TABLE `privileges`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ref_idx` (`ref`),
  ADD KEY `IDX_6855F9124FEA67CF` (`access_id`),
  ADD KEY `route_idx` (`route`),
  ADD KEY `created_at_idx` (`created_at`),
  ADD KEY `updated_at_idx` (`updated_at`);

--
-- Indeksy dla tabeli `privileges_fields`
--
ALTER TABLE `privileges_fields`
  ADD PRIMARY KEY (`privilege_id`,`field_id`),
  ADD KEY `IDX_4B6B5E6A32FB8AEA` (`privilege_id`),
  ADD KEY `IDX_4B6B5E6A443707B0` (`field_id`);

--
-- Indeksy dla tabeli `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ref_idx` (`ref`),
  ADD KEY `created_at_idx` (`created_at`),
  ADD KEY `updated_at_idx` (`updated_at`);

--
-- Indeksy dla tabeli `roles_privileges`
--
ALTER TABLE `roles_privileges`
  ADD PRIMARY KEY (`role_id`,`privilege_id`),
  ADD KEY `IDX_2472C79AD60322AC` (`role_id`),
  ADD KEY `IDX_2472C79A32FB8AEA` (`privilege_id`);

--
-- Indeksy dla tabeli `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_9A609D13613FECDF` (`session_id`);

--
-- Indeksy dla tabeli `shipments`
--
ALTER TABLE `shipments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_ref` (`ref`),
  ADD KEY `index_created_at` (`created_at`),
  ADD KEY `index_updated_at` (`updated_at`),
  ADD KEY `index_id_company` (`id_company`);

--
-- Indeksy dla tabeli `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_created_at` (`created_at`),
  ADD KEY `index_updated_at` (`updated_at`),
  ADD KEY `index_id_delivery` (`id_article`),
  ADD KEY `index_id_contact` (`id_contact`),
  ADD KEY `index_id_cart` (`id_cart`),
  ADD KEY `index_id_order` (`id_order`),
  ADD KEY `index_id_payment` (`id_payment`),
  ADD KEY `index_id_invoice` (`id_invoice`),
  ADD KEY `index_id_shippment` (`id_shippment`),
  ADD KEY `index_transaction_type` (`transaction_type`) USING BTREE;

--
-- Indeksy dla tabeli `transactions_types`
--
ALTER TABLE `transactions_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_ref` (`ref`),
  ADD KEY `index_created_at` (`created_at`),
  ADD KEY `index_updated_at` (`updated_at`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `accesses`
--
ALTER TABLE `accesses`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT dla tabeli `addresses`
--
ALTER TABLE `addresses`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `articles_categorys`
--
ALTER TABLE `articles_categorys`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT dla tabeli `articles_features`
--
ALTER TABLE `articles_features`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT dla tabeli `articles_options`
--
ALTER TABLE `articles_options`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=189;

--
-- AUTO_INCREMENT dla tabeli `articles_subcategorys`
--
ALTER TABLE `articles_subcategorys`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT dla tabeli `articles_subcategorys_features`
--
ALTER TABLE `articles_subcategorys_features`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=138;

--
-- AUTO_INCREMENT dla tabeli `carts`
--
ALTER TABLE `carts`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `fields`
--
ALTER TABLE `fields`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT dla tabeli `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `notebooks`
--
ALTER TABLE `notebooks`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `privileges`
--
ALTER TABLE `privileges`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT dla tabeli `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT dla tabeli `sessions`
--
ALTER TABLE `sessions`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT dla tabeli `shipments`
--
ALTER TABLE `shipments`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `transactions_types`
--
ALTER TABLE `transactions_types`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `addresses`
--
ALTER TABLE `addresses`
  ADD CONSTRAINT `addresses_ibfk_1` FOREIGN KEY (`id_company`) REFERENCES `companies` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ograniczenia dla tabeli `articles_options`
--
ALTER TABLE `articles_options`
  ADD CONSTRAINT `articles_options_ibfk_1` FOREIGN KEY (`feature_id`) REFERENCES `articles_features` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ograniczenia dla tabeli `articles_subcategorys`
--
ALTER TABLE `articles_subcategorys`
  ADD CONSTRAINT `articles_subcategorys_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `articles_categorys` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ograniczenia dla tabeli `articles_subcategorys_features`
--
ALTER TABLE `articles_subcategorys_features`
  ADD CONSTRAINT `articles_subcategorys_features_ibfk_1` FOREIGN KEY (`feature_id`) REFERENCES `articles_features` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  ADD CONSTRAINT `articles_subcategorys_features_ibfk_2` FOREIGN KEY (`subcategory_id`) REFERENCES `articles_subcategorys` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

--
-- Ograniczenia dla tabeli `contacts`
--
ALTER TABLE `contacts`
  ADD CONSTRAINT `contacts_ibfk_1` FOREIGN KEY (`id_company`) REFERENCES `companies` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ograniczenia dla tabeli `notebooks`
--
ALTER TABLE `notebooks`
  ADD CONSTRAINT `notebooks_ibfk_1` FOREIGN KEY (`subcategory_id`) REFERENCES `articles_subcategorys` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ograniczenia dla tabeli `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `payments_ibfk_1` FOREIGN KEY (`id_company`) REFERENCES `companies` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ograniczenia dla tabeli `privileges`
--
ALTER TABLE `privileges`
  ADD CONSTRAINT `privileges_ibfk_1` FOREIGN KEY (`access_id`) REFERENCES `accesses` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ograniczenia dla tabeli `privileges_fields`
--
ALTER TABLE `privileges_fields`
  ADD CONSTRAINT `FK_4B6B5E6A32FB8AEA` FOREIGN KEY (`privilege_id`) REFERENCES `privileges` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_4B6B5E6A443707B0` FOREIGN KEY (`field_id`) REFERENCES `fields` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `roles_privileges`
--
ALTER TABLE `roles_privileges`
  ADD CONSTRAINT `FK_2472C79A32FB8AEA` FOREIGN KEY (`privilege_id`) REFERENCES `privileges` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_2472C79AD60322AC` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `shipments`
--
ALTER TABLE `shipments`
  ADD CONSTRAINT `shipments_ibfk_1` FOREIGN KEY (`id_company`) REFERENCES `companies` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ograniczenia dla tabeli `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `transactions_ibfk_1` FOREIGN KEY (`id_article`) REFERENCES `articles` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `transactions_ibfk_2` FOREIGN KEY (`id_cart`) REFERENCES `carts` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `transactions_ibfk_3` FOREIGN KEY (`id_contact`) REFERENCES `contacts` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `transactions_ibfk_4` FOREIGN KEY (`id_invoice`) REFERENCES `invoices` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `transactions_ibfk_5` FOREIGN KEY (`id_order`) REFERENCES `orders` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `transactions_ibfk_6` FOREIGN KEY (`id_payment`) REFERENCES `payments` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `transactions_ibfk_7` FOREIGN KEY (`id_shippment`) REFERENCES `shipments` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
