setSearchSelectData = function(element, category, subcategory) {
    let data = "";
    data = data.concat("select_ref=", element.getAttribute("id"));

    let categoryId = "";
    if (typeof category.options[category.selectedIndex] !== "undefined") {
        categoryId = category.options[category.selectedIndex].value;
    }
    data = data.concat("&category_id=", categoryId);

    let subcategoryId = "";
    if (typeof subcategory.options[subcategory.selectedIndex] !== "undefined") {
        subcategoryId = subcategory.options[subcategory.selectedIndex].value;
    }

    return data.concat("&subcategory_id=", subcategoryId);
}

setSearchSelectDataForOptions = function(element, category, subcategory, feature) {
    let data = setSearchSelectData(element, category, subcategory)

    let featureId = "";
    if (typeof feature.options[feature.selectedIndex] !== "undefined") {
        featureId = feature.options[feature.selectedIndex].value;
    }

    return data.concat("&feature_id=",featureId);
}

setSearchSelectDataForQuantities = function(element, category, subcategory, feature1, feature2, entrys) {
    let data = setSearchSelectData(element, category, subcategory)

    let feature1Id = "";
    if (typeof feature1.options[feature1.selectedIndex] !== "undefined") {
        feature1Id = feature1.options[feature1.selectedIndex].value;
    }
    data = data.concat("&feature1_id=", feature1Id);

    let feature2Id = "";
    if (typeof feature2.options[feature2.selectedIndex] !== "undefined") {
        feature2Id = feature2.options[feature2.selectedIndex].value;
    }
    data = data.concat("&feature2_id=", feature2Id);

    let entrysNumber = "";
    if (typeof entrys.options[entrys.selectedIndex] !== "undefined") {
        entrysNumber = entrys.options[entrys.selectedIndex].value;
    }

    return data.concat("&entrys_number=", entrysNumber);
}

sendData = function(data, url) {
    const XHR = new XMLHttpRequest();

    XHR.addEventListener("load", function(event) {
        let content = document.getElementById("ajax-content");
        content.parentNode.removeChild(content);

        document.getElementById('ajax-container').innerHTML = event.target.responseText;
    });

    XHR.addEventListener("error", function(event) {
        console.log('Oops! Something went wrong.');
    });

    XHR.open("POST", url);
    XHR.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    XHR.send(data);
}

setSelects = function(element) {
    let category = document.getElementById("categorys");
    let subcategory = document.getElementById("subcategorys");

    if (
        element.classList.contains('search-select-options-edit') ||
        element.classList.contains('search-select-options-add')
    ) {
        let feature = document.getElementById("features");

        let data = setSearchSelectDataForOptions(element, category, subcategory, feature);

        let url = element.classList.contains('search-select-options-edit') ? articleOptionsSearchEditUrl : articleOptionsSearchAddUrl;

        sendData(data, url);
    }

    if (
        element.classList.contains('search-select-quantitys-edit') ||
        element.classList.contains('search-select-quantitys-add')
    ) {
        let feature1 = document.getElementById("features1");
        let feature2 = document.getElementById("features2");
        let entrys = document.getElementById("entrys");

        let data = setSearchSelectDataForQuantities(element, category, subcategory, feature1, feature2, entrys);
console.log(data);
        let url = element.classList.contains('search-select-quantitys-edit') ? articleQuantitysSearchEditUrl : articleQuantitysSearchAddUrl;

        sendData(data, url);
    }
}

document.addEventListener('change',function(e){
    if(
        e.target &&
        (
            e.target.id === "categorys" ||
            e.target.id === "subcategorys" ||
            e.target.id === "features" ||
            e.target.id === "features1" ||
            e.target.id === "features2" ||
            e.target.id === "entrys"
        )
    ) {
        setSelects(e.target);
    }
});
