<?php

declare(strict_types=1);

define('ENV', 'dev');

define('ROOT_FOLDER', dirname(__DIR__) . '/');
define('APP_FOLDER', ROOT_FOLDER . 'src/');

require ROOT_FOLDER . 'vendor/autoload.php';

use App\SEO\SEOAdapter;
use DI\ContainerBuilder;
use Paneric\Local\Local;
use Paneric\ModuleResolver\DefinitionsCollector;
use Paneric\ModuleResolver\ModuleResolver;
use Slim\Factory\AppFactory;

try {
    $seoAdapter = new SEOAdapter($_SERVER['REQUEST_URI'], ['produkty']);

    $moduleResolverConfig = require APP_FOLDER . 'config/module-resolver-config.php';
    $moduleResolver = new ModuleResolver();
    $moduleFolderName = $moduleResolver->setModuleFolderName(
        $_SERVER['REQUEST_URI'],
        $moduleResolverConfig
    );

    define('MODULE_FOLDER', APP_FOLDER . $moduleFolderName);

    $localValue = $moduleResolver->getLocal();
    $localConfig = require APP_FOLDER . 'config/local-config.php';
    $local = new Local();
    $localValue = $local->setValue($localConfig[ENV], $moduleResolverConfig['local_map'], $_COOKIE, $localValue);

    $definitionsCollector = new DefinitionsCollector();
    $definitions = $definitionsCollector->setDefinitions(
        APP_FOLDER,
        MODULE_FOLDER,
        'config',
        $localValue,
        ENV
    );

    $builder = new ContainerBuilder();
    $builder->useAutowiring(true);
    $builder->useAnnotations(true);
    $builder->addDefinitions($definitions);

    $container = $builder->build();

    AppFactory::setContainer($container);
    $app = AppFactory::create();

    $container->set('local', $localValue);
    $container->set('base_path', ROOT_FOLDER);
    $container->set('route_parser_interface', $app->getRouteCollector()->getRouteParser());

    include MODULE_FOLDER . 'bootstrap/seo-validation.php';

    include MODULE_FOLDER . 'bootstrap/middleware.php';
    include APP_FOLDER . 'bootstrap/middleware.php';

    include APP_FOLDER . 'bootstrap/routes.php';
    require MODULE_FOLDER . 'bootstrap/routes.php';
    include MODULE_FOLDER . 'bootstrap/seo-routes.php';

    $app->run();

} catch (Exception $e) {
    echo $e->getMessage();
}
